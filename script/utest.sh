#!/bin/bash

###############################
#
#  Testing script
#
###############################

MODULE_PATH=$1
if [ "$MODULE_PATH" == "" ]; then
    echo "Missing PATH command line argument"
    exit -1
fi

BUILD=debug
if [ ! "$2" == "" ]; then
    BUILD=$2
fi

WORK_DIR="${MODULE_PATH}/${BUILD}/work"
EXEC="${MODULE_PATH}/${BUILD}/utest"

rm -rf $WORK_DIR
mkdir $WORK_DIR
cd $WORK_DIR

if [ -f $PROJECT_HOME/test/valgrind.on ]; then
    valgrind --leak-check=full $EXEC
else
    $EXEC
fi


