#!/bin/bash

echo "Running unit tests for all modules..."
cd $PROJECT_HOME/src
make test 2>&1 | egrep "UNIT|PASSED|FAILED|ERROR SUMMARY"

