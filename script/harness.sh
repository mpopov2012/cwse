#!/bin/bash

#################################
#
#  Test Harness for ClockWorkDB
#
#################################

function prepareHarnessScripts() {
    local SOURCE_DIR=$1
    local WORK_DIR=$2

    rm -rf $WORK_DIR
    mkdir $WORK_DIR

    echo "echo Entering $WORK_DIR" > $WORK_DIR/test.sh
    echo "cd $WORK_DIR" > $WORK_DIR/test.sh
    echo "echo ======== Testing $3 ========= >> $WORK_DIR/result.output" >> $WORK_DIR/test.sh
    chmod +x $WORK_DIR/test.sh

    if [ $WORK_DIR == $CDB_TEST_WORK ]; then
        # Remove older test report. Just in case...
        echo "rm -rf $CDB_TEST/$TEST_NOW.result"  >> $WORK_DIR/test.sh
        echo "" >> $WORK_DIR/test.sh
    fi

    # Copy data files to the working directory
    local dataFileSelector=""
    DATA_FILES=`find $SOURCE_DIR -mindepth 1 -maxdepth 1 -name *.data -type f`
    for dataFileSelector in $DATA_FILES; do
        local file=$(basename $dataFileSelector)
        cp  $SOURCE_DIR/$file  $WORK_DIR/$file
    done

    local fileSelector=""
    FILES=`find $SOURCE_DIR -mindepth 1 -maxdepth 1 -name *.test -type f`
    for fileSelector in $FILES; do
        local file=$(basename $fileSelector)
        local disabledCheck="ok"

        if [ -f $SOURCE_DIR/disabled ]; then
            disabledFound=`grep $file $SOURCE_DIR/disabled | wc -l`
            if [ $disabledFound != "0" ]; then
                disabledCheck="skip"
            fi
        fi

        if [ "$CDB_CHECK" != "" ]; then
            if [ "$CDB_CHECK.test" != $file ]; then
                disabledCheck="skip"
            fi
        fi

        $CDB_HOME/bin-dbg/tester  $SOURCE_DIR/$file > $WORK_DIR/$file.sh
        #cp  $SOURCE_DIR/$file  $WORK_DIR/$file.sh
        if [ $? -ne 0 ]; then
            echo "Failed to compile file $SOURCE_DIR/$file"
            exit -1
        fi

        chmod +x $WORK_DIR/$file.sh

        if [ "$WIN32" != "" ]; then
            dost2unix $WORK_DIR/$file.sh
        fi

        if [ $disabledCheck == "ok" ]; then
            echo "echo " "Running $file" >> $WORK_DIR/test.sh
            echo "$WORK_DIR/$file.sh  >> $WORK_DIR/result.output" >> $WORK_DIR/test.sh
        else
            echo "# echo " "Running $file" >> $WORK_DIR/test.sh
            echo "# $WORK_DIR/$file.sh  >> $WORK_DIR/result.output" >> $WORK_DIR/test.sh
        fi
    done

    echo "" >> $WORK_DIR/test.sh
    echo "cat $WORK_DIR/result.output >> $CDB_TEST/$TEST_NOW.result" >> $WORK_DIR/test.sh

    echo "" >> $WORK_DIR/test.sh
    echo "PASSED_COUNT=\`grep PASSED $WORK_DIR/result.output | wc -l\`" >> $WORK_DIR/test.sh
    echo "FAILED_COUNT=\`grep FAILED $WORK_DIR/result.output | wc -l\`" >> $WORK_DIR/test.sh
    echo "echo ------------- >> $WORK_DIR/result.output"                >> $WORK_DIR/test.sh
    echo "echo --- \$PASSED_COUNT   PASSED >> $WORK_DIR/result.output"  >> $WORK_DIR/test.sh
    echo "echo --- \$FAILED_COUNT   FAILED >> $WORK_DIR/result.output"  >> $WORK_DIR/test.sh
    echo "" >> $WORK_DIR/test.sh

    local dirSelector=""
    DIRS=`find $SOURCE_DIR -mindepth 1 -maxdepth 1 -type d`
    for dirSelector in $DIRS; do
        local dir=$(basename $dirSelector)
        prepareHarnessScripts $SOURCE_DIR/$dir $WORK_DIR/$dir $dir
        echo "$WORK_DIR/$dir/test.sh" >> $WORK_DIR/test.sh
    done

    if [ $WORK_DIR == $CDB_TEST_WORK ]; then
        echo "" >> $WORK_DIR/test.sh
        echo "PASSED_COUNT=\`grep PASSED $CDB_TEST/$TEST_NOW.result | wc -l\`" >> $WORK_DIR/test.sh
        echo "FAILED_COUNT=\`grep FAILED $CDB_TEST/$TEST_NOW.result | wc -l\`" >> $WORK_DIR/test.sh
        echo "echo ------------- >> $CDB_TEST/$TEST_NOW.result"                >> $WORK_DIR/test.sh
        echo "echo --- \$PASSED_COUNT   PASSED >> $CDB_TEST/$TEST_NOW.result"  >> $WORK_DIR/test.sh
        echo "echo --- \$FAILED_COUNT   FAILED >> $CDB_TEST/$TEST_NOW.result"  >> $WORK_DIR/test.sh
        echo "" >> $WORK_DIR/test.sh
        echo "echo ------------- "                >> $WORK_DIR/test.sh
        echo "echo --- \$PASSED_COUNT   PASSED "  >> $WORK_DIR/test.sh
        echo "echo --- \$FAILED_COUNT   FAILED "  >> $WORK_DIR/test.sh
    fi
}


###########################
# Set variables with paths
###########################
if [ "$CDB_TEST" == "" ]; then
    if [ "$CDB_HOME" == "" ]; then
        echo "Environment is not set! Neither CDB_TEST nor CDB_HOME env vars"
        exit -1
    fi
    export CDB_TEST=$CDB_HOME/test/work
fi

if [ ! -d $CDB_TEST ]; then
    mkdir $CDB_TEST
fi

if [ "$1" != "" ]; then
    export CDB_TEST_SOURCE=$1
else
    export CDB_TEST_SOURCE=$CDB_HOME/test/library
fi

DATE=`date +%F`
TIME=`date +%s`
#export TEST_NOW=${DATE}_${TIME}
export TEST_NOW=${DATE}

export CDB_TEST_WORK=$CDB_TEST/$TEST_NOW

#echo "#####################################"
#echo "CDB_TEST=$CDB_TEST"
#echo "CDB_TEST_SOURCE=$CDB_TEST_SOURCE"
#echo "TEST_NOW=$TEST_NOW"
#echo "CDB_TEST_WORK=$CDB_TEST_WORK"
#echo "#####################################"

############################
# Prepare test scritps
############################

prepareHarnessScripts $CDB_TEST_SOURCE  $CDB_TEST_WORK

############################
# Execute test scritps
############################

if [ ! -f "$CDB_TEST_WORK/test.sh" ]; then
    echo "Missing test script $CDB_TEST_WORK/test.sh"
    exit -1
fi

$CDB_TEST_WORK/test.sh

