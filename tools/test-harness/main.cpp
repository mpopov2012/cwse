/**********************************************
   File:   main.cpp

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifdef WIN32
// Turning off warning generated for Boost split compilation
#pragma warning( disable : 4996 )
#endif

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <stdlib.h>

#define DEFAULT_OUTPUT  "test.out"
#define THROW(s)  throw std::runtime_error(s)

using namespace std;

struct Position
{
    string m_fileName;
    int m_line;
};
list< Position > positions;
ostream& operator<<(ostream& os, const Position& pos)
{
    os << pos.m_fileName << ":" << pos.m_line << "  ";
    return os;
}

bool g_trace = false;
bool g_out = false;
bool g_in = false;
string g_currentCheck = "";
string g_currentOut = DEFAULT_OUTPUT;

enum Operation { OP_NOP, OP_SET, OP_INCLUDE, OP_SCRIPT, OP_COMMAND, 
                 OP_OUTPUT, OP_INPUT, OP_SCRIPTED_INPUT, OP_CHECK };

void usage();
void prepareScript(const string& fileName);
void trimLine(Operation& oper, string& str);
void closeInput();
void processFile(const char* fileName);
void processSet(string& str);
void processScript(string& str);
void processCommand(string& str);
void processOutput(string& str);
void processInput(string& str);
void processScriptedInput(string& str);
void processCheck(string& str);
string buildFileName(const string& currentFileName, const string& includeFileName);

int main(int argc, char* argv[])
{
    if (argc < 2) usage();
    try {
        prepareScript(argv[1]);
        processFile(argv[1]);
    }
    catch(std::exception& e) {
        cerr << e.what() << endl;
        return -1;
    }
    return 0;
}

void usage()
{
    cerr << "Usage:" << endl;
    cerr << "  cdbth <file_name>" << endl;
    cerr << "    file_name is a name of an input file" << endl;
    cerr << "    output is written to stdout" << endl;
    cerr << "Example:" << endl;
    cerr << "  cdbth Dataspaces.case > Dataspaces.sh" << endl;
    cerr << endl;
    exit(-1);
}

void prepareScript(const string& fileName)
{
    size_t pos;
    string name = fileName;

    pos = name.find_last_of('/');
    if (pos != string::npos) name = name.substr(pos+1);

    pos = name.find_last_of(".");
    if (pos != string::npos) name = name.substr(0, pos);

    if ((name.length() == 0) || (name == ".") || (name == "..")) THROW("Invalid test case name");

    cout << "#!/bin/bash" << endl;
    cout << endl;
    cout << "function report() {" << endl;
    cout << "    if [ \"$1\" == \"FAILED\" ]; then" << endl;
    cout << "        echo \" > FAILED    \" $2" << endl;
    cout << "    else" << endl;
    cout << "        echo \"   PASSED    \" $2" << endl;
    cout << "    fi" << endl;
    cout << "}" << endl;
    cout << endl;
    cout << "export NAME=" << name << endl;
    cout << endl;
    cout << "rm -rf " << name << endl;
    cout << "mkdir " << name << endl;
    cout << "if [ ! -d " << name << " ]; then" << endl;
    cout << "  echo \">>>> Failed to create working directory\" " << name << endl;
    cout << "  exit -1" << endl;
    cout << "fi" << endl;
    cout << "cd " << name << endl;
    cout << endl;
}

void processFile(const char* fileName)
{
    ifstream f(fileName);
    if (!f.good()) THROW(string("Failed to open file - ") + fileName);

    Operation op;
    string str;

    Position pos = { fileName, 0 };
    positions.push_front(pos);

    while (!f.eof()) {
        positions.front().m_line++;

        str = "";
        getline(f, str);
        if (str.empty()) continue;
        trimLine(op, str);

        switch (op) {
            case OP_NOP: break;
            case OP_SET: processSet(str); break;
            case OP_INCLUDE: {
                bool saveTraceState = g_trace;
                string newfileName = buildFileName(fileName, str);
                processFile(newfileName.c_str());
                g_trace = saveTraceState;
                break;
            }
            case OP_SCRIPT: processScript(str); break;
            case OP_COMMAND: processCommand(str); break;
            case OP_OUTPUT: processOutput(str); break;
            case OP_INPUT: processInput(str); break;
            case OP_SCRIPTED_INPUT: processScriptedInput(str); break;
            case OP_CHECK: processCheck(str); break;
        }
    }

    positions.pop_front();
}

void trimLine(Operation& oper, string& str)
{
    boost::algorithm::trim(str);

    if (str.length() == 0) {
        oper = OP_NOP;
        return;
    }

    switch(str[0]) {
        default:  oper = OP_OUTPUT; return;
        case '#': oper = OP_NOP; return;
        case '@': oper = OP_SET; break;
        case ':': oper = OP_SCRIPT; break;
        case '%': oper = OP_INCLUDE; break;
        case '>': oper = OP_COMMAND; break;
        case '$': oper = OP_INPUT; break;
        case '*': oper = OP_SCRIPTED_INPUT; break;
        case '=': oper = OP_CHECK; break;
    }

    str[0] = ' ';
    boost::algorithm::trim(str);
}

void processSet(string& str)
{
    vector< string > strs;
    boost::split(strs, str, boost::is_any_of("="));
    if (strs.size() != 2) {
        cerr << positions.front() << "Expected name=value pair" << endl;
        THROW("Failed to parse SET (@) line");
    }

    if (boost::iequals(strs[0], "name")) {
        g_currentCheck = strs[1];
        g_currentOut = g_currentCheck + ".out";
    }
    else if (boost::iequals(strs[0], "trace")) {
        if (boost::iequals(strs[1], "on"))
            g_trace = true;
        else if (boost::iequals(strs[1], "off"))
            g_trace = false;
        else {
            cerr << positions.front() << "Expected trace=on/off" << endl;
            THROW("Invalid value of a trace setting");
        }
    }
    else {
        cerr << positions.front() << "Expected valid setting" << endl;
        THROW(string("Invalid setting name - ") + strs[0]);
    }
}

void processScript(string& str)
{
    if (g_trace) cout << "echo \"Executing \" " << str << endl;
    cout << str << endl << endl;
}

void processCommand(string& str)
{
    if (g_trace) cout << "echo \"Command: \" " << str << endl;
    cout << "$CDB_HOME/bin-dbg/cdbcon --silent --exec \"" << str << "\" > " << g_currentOut << " 2>&1" << endl;
#ifdef WIN32
    cout << "dos2unix " << g_currentOut << endl;
#endif
}

void closeInput()
{
    if (g_in) {
        g_in = false;
        cout << ") | $CDB_HOME/bin-dbg/cdbcon --silent > " << g_currentOut << " 2>&1" << endl;
#ifdef WIN32
        cout << "dos2unix " << g_currentOut << endl;
#endif
    }
}

void processOutput(string& str)
{
    closeInput();
    if (str == "<empty>") str = "";
    if (!g_out) 
        cout << "echo \"" << str << "\" " << "> " << g_currentOut << ".check" << endl;
    else
        cout << "echo \"" << str << "\" " << ">> " << g_currentOut << ".check" << endl;
    g_out = true;
}

void processInput(string& str)
{
    if (g_in) {
        cout << endl;
    }
    else {
        if (g_trace) cout << "echo \"Processing input lines in cdbcon\"" << endl;
        cout << "(" << endl;
    }
    g_in = true;
    cout << "echo \"" << str << "\"";
}

void processScriptedInput(string& str)
{
    if (g_in) {
        cout << endl;
    }
    else
        cout << "(" << endl;
    g_in = true;
    cout << str;
}

void processCheck(string& str)
{
    closeInput();

    for (size_t i=0; i < str.length(); i++) {
        if (str[i] != '=') break;
        str[i] = ' ';
    }

    boost::algorithm::trim(str);

    vector< string > strs;
    boost::split(strs, str, boost::is_any_of(" \t"));

    string checkName = g_currentCheck;
    string checkReport = "true";
    string checkOnFail = "ignore";
    string checkType = "equal";

    for (size_t i=0; i < strs.size(); i++) {
        vector< string > nv;
        boost::split(nv, strs[i], boost::is_any_of("="));

        if (nv.size() == 0) break;
        if (nv.size() == 1 && nv[0].empty()) break;

        if (nv.size() != 2) {
            cerr << positions.front() << "Expected name=value pair in CHECK" << endl;
            THROW("Failed to parse CHECK name value pair");
        }

        if (boost::iequals(nv[0], "name")) 
            checkName = nv[1];
        else if (boost::iequals(nv[0], "report"))
            checkReport = nv[1];
        else if (boost::iequals(nv[0], "onfail"))
            checkOnFail = nv[1];
        else if (boost::iequals(nv[0], "check"))
            checkType = nv[1];
        else {
            cerr << positions.front() << "Expected valid parameter in CHECK" << endl;
            THROW(string("Invalid parameter in CHECK: ")+nv[0]);
        }
    }

    if (checkName.empty()) {
        cerr << positions.front() << "Check name is not set" << endl;
        THROW("Check name is not set");
    }

    cout << "TEST_RESULT=\"failed\"" << endl;

    if (!g_out) { 
        // No output registered, meaning it must be empty
        cout << "rm -rf " << g_currentOut << ".check" << endl;
        cout << "touch "  << g_currentOut << ".check" << endl;
    }

    if (boost::iequals(checkType, "equal")) {
        cout << "DIFF_RESULT=`diff " << g_currentOut << "  " << g_currentOut << ".check | wc -l`" << endl;
        cout << "if [ $DIFF_RESULT -eq 0 ]; then" << endl;
    }
    else if (boost::iequals(checkType, "grep")) {
        cout << "CHECK_LINE=`head -1 " << g_currentOut << ".check`" << endl;
        cout << "CHECK_RESULT=`grep \"$CHECK_LINE\" " << g_currentOut << " | wc -l`" << endl;
        cout << "if [ $CHECK_RESULT -ne 0 ]; then" << endl;
    }
    else if (boost::iequals(checkType, "ngrep")) {
        cout << "CHECK_LINE=`head -1 " << g_currentOut << ".check`" << endl;
        cout << "CHECK_RESULT=`grep \"$CHECK_LINE\" " << g_currentOut << " | wc -l`" << endl;
        cout << "if [ $CHECK_RESULT -eq 0 ]; then" << endl;
    }
     else {
        cout << checkType << " " << g_currentOut << "  " << g_currentOut << ".check" << endl;
        cout << "if [ $? -eq 0 ]; then" << endl;
    }
    cout << "   TEST_RESULT=\"passed\"" << endl;
    cout << "fi" << endl;

    if (boost::iequals(checkReport, "true")) {
        cout << "if [ $TEST_RESULT == \"passed\" ]; then" << endl;
        cout << "    report \"PASSED\" \"$NAME." << checkName << "\" " << endl;
        //cout << "    echo \" PASSED\" \"    $NAME." << checkName << "\"" << endl;
        cout << "fi" << endl;
    }

    cout << "if [ $TEST_RESULT != \"passed\" ]; then" << endl;
        cout << "    report \"FAILED\" \"$NAME." << checkName << "\" " << endl;
        //cout << "    echo \">FAILED\" \"    $NAME." << checkName << "\"" << endl;
    if (!boost::iequals(checkOnFail, "ignore")) {
        cout << "    exit -1" << endl;
    }
    cout << "fi" << endl;

    g_currentCheck = "";
    g_currentOut = DEFAULT_OUTPUT;
    g_out = false;

    cout << endl;
}


string buildFileName(const string& currentFileName, const string& includeFileName)
{
    size_t pos = currentFileName.find_last_of('/');
    if (pos == string::npos) return includeFileName;
    return currentFileName.substr(0, pos+1) + includeFileName;
}

