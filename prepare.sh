export PROJECT_HOME=`pwd`
export LD_LIBRARY_PATH=$PROJECT_HOME/third-party/lib:$PROJECT_HOME/lib-dbg:$PROJECT_HOME/lib:$LD_LIBRARY_PATH

if [ ! -d pkg ]; then
    mkdir pkg
fi

if [ ! -d bin ]; then
    mkdir bin
fi

if [ ! -d lib ]; then
    mkdir lib
fi

if [ ! -d bin-dbg ]; then
    mkdir bin-dbg
fi

if [ ! -d lib-dbg ]; then
    mkdir lib-dbg
fi


