/**********************************************
   File:   concurrent_hash_table.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_UTILS_CONCURRENT_HASH_TABLE_H
#define CDB_UTILS_CONCURRENT_HASH_TABLE_H

#include "utils/atomic.h"
#include <string>
#include <stdint.h>
#include <stddef.h>

namespace cdb {

class ConcurrentHashTable
{
public:
    ConcurrentHashTable(size_t n=11);
    ~ConcurrentHashTable();

    void put(const char* name, void* value);
    bool get(const char* name, void** value) const;

    void put(const char* name, uint16_t value);
    bool get(const char* name, uint16_t* value) const;

    size_t size() const;
    void clear();

    void testDistribution() const;

private:
    struct Node
    {
        uint64_t m_hashHigh;
        uint64_t m_hashLow;
        std::string m_name;
        void* m_value;
        Node* m_next;
    };
    typedef Node* NodePtr;

    NodePtr* m_nodes;
    size_t m_bucketsCount;

public:
    class Cursor
    {
        friend class ConcurrentHashTable;
    private:
        Cursor(ConcurrentHashTable* parent) : m_parent(parent), m_bucket(-1), m_current(NULL) {}

    public:
        Cursor(const Cursor& another) { copy(another); }
        Cursor& operator=(const Cursor& another) { copy(another); return *this; }

    public:
        bool next(std::string& result)
        {
            while (m_current == NULL) {
                m_bucket++;
                if (m_bucket >= (int) m_parent->m_bucketsCount) return false;
                m_current = m_parent->m_nodes[m_bucket];
            }
            result = m_current->m_name;
            m_current = m_current->m_next;
            return true;
        }

    private:
        ConcurrentHashTable* m_parent;
        int m_bucket;
        NodePtr m_current;

    private:
        void copy(const Cursor& another)
        {
            m_parent = another.m_parent;
            m_bucket = another.m_bucket;
            m_current = another.m_current;
        }

    };

public:
    Cursor getCursor() { return Cursor(this); }

private:
    uint32_t calcHash(const void * key, const int len, uint64_t* hashHigh, uint64_t* hashLow) const;

};

} // namespace CDB_UTILS_CONCURRENT_HASH_TABLE_H

#endif // CDB_ATOMIC_H
