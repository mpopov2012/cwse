/**********************************************
   File:   concurrent_list.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_UTILS_CONCURRENT_LIST_H
#define CDB_UTILS_CONCURRENT_LIST_H

#include "utils/atomic.h"
#include <stddef.h>

namespace cdb {

template <typename T>
class ConcurrentList
{
public:
    struct Item
    {
        T m_value;
        Item* m_next;
    };

    class Iterator
    {
    public:
        Iterator(Item* item) : m_item(item) {}
        Iterator(const Iterator& another) : m_item(another.m_item) {}

        void next() { if (m_item != NULL) m_item = m_item->m_next; }
        bool hasNext() const { return m_item != NULL; }
        T value() const { return m_item->m_value; }
    private:
        Item* m_item;
    };

public:
    ConcurrentList() : m_head(NULL) {}
    ~ConcurrentList();
    Iterator getIterator() { return Iterator(m_head); }
    void add(T value);

private:
    Item* m_head;
};

template <typename T>
ConcurrentList<T>::~ConcurrentList()
{
    while (m_head != NULL) {
        Item* t = m_head->m_next;
        delete m_head;
        m_head = t;
    }
}

template <typename T>
void ConcurrentList<T>::add(T value)
{
    Item* item = new Item;
    item->m_value = value;

    bool success = true;
    do {
        item->m_next = m_head;
        success = Atomic::casptr((void* volatile*)&m_head, item->m_next, item);
    } while (!success);
}

} // namespace cdb

#endif // CDB_UTILS_CONCURRENT_LIST_H
