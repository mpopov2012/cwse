/**********************************************
   File:   timer.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "utils/timer.h"
#include <time.h>
#ifdef WIN32
#include <Windows.h>
#else
#include <sys/time.h>
#include <unistd.h>
#endif

namespace cdb {

uint64_t Timer::getCurrentTimestampMs()
{
    timeval tv;

#ifdef WIN32
#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif
    FILETIME ft;
    unsigned __int64 tmpres = 0;
    static int tzflag;
 
    GetSystemTimeAsFileTime(&ft);
 
    tmpres |= ft.dwHighDateTime;
    tmpres <<= 32;
    tmpres |= ft.dwLowDateTime;
 
    /*converting file time to unix epoch*/
    tmpres /= 10;  /*convert into microseconds*/
    tmpres -= DELTA_EPOCH_IN_MICROSECS; 
    tv.tv_sec = (long)(tmpres / 1000000UL);
    tv.tv_usec = (long)(tmpres % 1000000UL);
#else
    gettimeofday(&tv, NULL);
#endif

    return tv.tv_sec * 1000 + tv.tv_usec / 1000000;
}


void Timer::sleepSec(int sec)
{
#ifdef WIN32
    ::Sleep(sec * 1000);
#else
    sleep(sec);
#endif
}

void Timer::sleepMsec(int msec)
{
#ifdef WIN32
    ::Sleep(msec);
#else
    timespec t;
    t.tv_sec = 0;
    t.tv_nsec = msec * 1000000;
    nanosleep(&t, NULL);
#endif
}


} // namespace cdb
