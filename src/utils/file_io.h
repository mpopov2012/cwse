/**********************************************
   File:   file_io.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_UTILS_FILE_IO_H
#define CDB_UTILS_FILE_IO_H

#include <string>
#include <stddef.h>

namespace cdb {

class FileIo
{
private:
    FileIo(const FileIo&);
    FileIo& operator=(const FileIo&);

public:
    enum Access { READ, WRITE, APPEND };

public:
    FileIo();
    ~FileIo() { close(); }

    void open(const char* path, Access access);
    void close();
    void write(const void* buffer, size_t size);
    void read(void* buffer, size_t size);
    void seek(size_t pos);
    size_t tell();
    void flush();
    void sync();
    void truncate(size_t size);

public:
    static bool exists(const char* path);
    static void mkdir(const char* path);
    static void rmdir(const char* path);
    static void remove(const char* path);
    static void rename(const char* old, const char* path);
    static size_t getSize(const char* path);
    static std::string getTempName(const char* path);

private:
    void* m_file;
    std::string m_path;

};

}

#endif // CDB_UTILS_FILE_IO_H
