/**********************************************
   File:   mapped_region.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_UTILS_MAPPED_REGION_H
#define CDB_UTILS_MAPPED_REGION_H

#include <boost/shared_ptr.hpp>
#include <string>

namespace cdb
{

class MappedRegion;
typedef boost::shared_ptr<MappedRegion> MappedRegionPtr;

class MappedFile
{
private:
    MappedFile(const MappedFile&);
    MappedFile& operator=(const MappedFile&);

public:
    enum MappedFileAccess { READ_ONLY, READ_WRITE };

public:
    static void allocate(const char* path, size_t size);

public:
    MappedFile();
    ~MappedFile();

    void open(const char* path, MappedFileAccess access);
    void close();

    MappedRegionPtr get(size_t offset, size_t size, MappedFileAccess access);

private:
    std::string m_path;
    void* m_fileMapping; // void* is ugly but cleaner: no need to pull all other *.h

};

class MappedRegion
{
    friend class MappedFile;

private:
    MappedRegion(const MappedRegion&);
    MappedRegion& operator=(const MappedRegion&);

    MappedRegion(void* mappedRegion, void* fd, size_t offset, size_t size);

public:
    ~MappedRegion();

    char* get();
    void flush();

private:
    void*  m_mappedRegion;
    void*  m_pfd;
    size_t m_offset;
    size_t m_size;
};

}

#endif // CDB_UTILS_MAPPED_REGION_H
