/**********************************************
   File:   utest_file_io.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "clockwork_errors.h"
#include "utils/file_io.h"
#include "utils/file_lock.h"
#include "gtest/gtest.h"
#include <string>
#include <iostream>

using namespace std;
using namespace cdb;

TEST(UTILS,WriteFileOps)
{
    string path = FileIo::getTempName("/tmp");

    try {
        FileIo file;
        file.open(path.c_str(), FileIo::WRITE);

        file.write("0123456789ABCDEF", 16);
        file.close();

        file.open(path.c_str(), FileIo::READ);

        char test[16];
        file.read(test, 16);

        ASSERT_TRUE(0 == memcmp("0123456789ABCDEF", test, 16));
    }
    catch (CwException& ex) {
        FileIo::remove(path.c_str());
        ASSERT_TRUE(false) << "Exception: " << ex.what() << endl << ex.file << " : " << ex.line;
    }

    FileIo::remove(path.c_str());
    ASSERT_FALSE(FileIo::exists(path.c_str()));
}

TEST(UTILS,BasicFileOps)
{
    string path = FileIo::getTempName("/tmp");

    try {
        FileIo file;
        file.open(path.c_str(), FileIo::APPEND);

        file.write("0123456789ABCDEF", 16);
        file.close();

        file.open(path.c_str(), FileIo::READ);

        char test[16];
        file.read(test, 16);

        ASSERT_TRUE(0 == memcmp("0123456789ABCDEF", test, 16));
    }
    catch (CwException& ex) {
        FileIo::remove(path.c_str());
        ASSERT_TRUE(false) << "Exception: " << ex.what() << endl << ex.file << " : " << ex.line;
    }

    FileIo::remove(path.c_str());
    ASSERT_FALSE(FileIo::exists(path.c_str()));
}


TEST(UTILS,SeekFileOps)
{
    string path = FileIo::getTempName("/tmp");

    try {
        FileIo file;

        file.open(path.c_str(), FileIo::WRITE);
        file.write("0123456789ABCDEF", 16);
        file.close();

        file.open(path.c_str(), FileIo::APPEND);
        file.write("0123456789ABCDEF", 16);
        file.close();

        file.open(path.c_str(), FileIo::READ);
        file.seek(16);

        char test[16];
        file.read(test, 16);

        ASSERT_TRUE(0 == memcmp("0123456789ABCDEF", test, 16));
    }
    catch (CwException& ex) {
        FileIo::remove(path.c_str());
        ASSERT_TRUE(false) << "Exception: " << ex.what() << endl << ex.file << " : " << ex.line;
    }

    FileIo::remove(path.c_str());
    ASSERT_FALSE(FileIo::exists(path.c_str()));
}

TEST(UTILS,DirOps)
{
    string path = "/tmp/dirOpsUtest";

    if (FileIo::exists(path.c_str())) {
        FileIo::rmdir(path.c_str());
    }

    ASSERT_FALSE(FileIo::exists(path.c_str()));
    FileIo::mkdir(path.c_str());
    ASSERT_TRUE(FileIo::exists(path.c_str()));
    
    string filePath = path + "/blah.txt";
    FileIo file;
    file.open(filePath.c_str(), FileIo::WRITE);
    file.truncate(16);
    file.close();

    FileIo::rmdir(path.c_str());
    ASSERT_FALSE(FileIo::exists(path.c_str()));
}

TEST(UTILS,OverlapIo)
{
    string path = FileIo::getTempName("/tmp");

    try {
        FileIo file1;

        file1.open(path.c_str(), FileIo::WRITE);
        file1.write("0123456789ABCDEF", 16);

        //FileIo file2;
        //file2.open(path.c_str(), FileIo::APPEND);
        file1.write("0123456789ABCDEF", 16);

        FileIo file3;
        file3.open(path.c_str(), FileIo::READ);
        file3.seek(16);

        char test[16];
        file3.read(test, 16);

        ASSERT_TRUE(0 == memcmp("0123456789ABCDEF", test, 16));
    }
    catch (CwException& ex) {
        FileIo::remove(path.c_str());
        ASSERT_TRUE(false) << "Exception: " << ex.what() << endl << ex.file << " : " << ex.line;
    }

    FileIo::remove(path.c_str());
    ASSERT_FALSE(FileIo::exists(path.c_str()));
}

TEST(UTILS, FileLock)
{
#ifdef WIN32
    const char* path = "/tmp/test2.lock";
    {
        FileLock f1;
        ASSERT_TRUE(f1.lock(path));

        FileLock f2;
        ASSERT_FALSE(f2.lock(path));
    }

    FileLock f2;
    ASSERT_TRUE(f2.lock(path));
#endif
}
