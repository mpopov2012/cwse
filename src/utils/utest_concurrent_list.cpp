/**********************************************
   File:   utest_concurrent_list.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "utils/concurrent_list.h"
#include "gtest/gtest.h"
#include <boost/thread.hpp>
#include <boost/thread/barrier.hpp>
#include <boost/bind.hpp>

using namespace cdb;

static const int COUNT = 1024*8;

static void thread_fun(boost::barrier& bar, ConcurrentList<int>& lst)
{
    bar.wait();
    for (int index = 0; index < COUNT; ++index) {
        lst.add(index);
    }
}

TEST(UTILS,PutConcurrentList)
{
    unsigned int d = 4;
    boost::barrier bar(d);
    ConcurrentList<int> lst;

    boost::thread thr1(boost::bind(&thread_fun, boost::ref(bar), boost::ref(lst)));
    boost::thread thr2(boost::bind(&thread_fun, boost::ref(bar), boost::ref(lst)));
    boost::thread thr3(boost::bind(&thread_fun, boost::ref(bar), boost::ref(lst)));
    boost::thread thr4(boost::bind(&thread_fun, boost::ref(bar), boost::ref(lst)));
    thr1.join();
    thr2.join();
    thr3.join();
    thr4.join();

    ConcurrentList<int>::Iterator iter = lst.getIterator();
    int count = 0;
    while (iter.hasNext()) {
        count++;
        iter.next();
    }
    ASSERT_EQ(COUNT*d, count);
}
