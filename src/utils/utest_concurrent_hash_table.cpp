/**********************************************
   File:   utest_concurrent_hash_table.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "utils/concurrent_hash_table.h"
#include "gtest/gtest.h"
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/thread/barrier.hpp>
#include <boost/bind.hpp>
#include <string>
#include <set>
#include <iostream>

using namespace std;
using namespace cdb;

TEST(UTILS,GetValueFromEmptyTable)
{
    ConcurrentHashTable table;
    uint16_t value;
    bool b = table.get("one", &value);
    ASSERT_FALSE(b);
    ASSERT_EQ(table.size(), 0);
}

TEST(UTILS,PutGetValueFromTable)
{
    ConcurrentHashTable table;
    table.put("one", 1);
    uint16_t value;
    bool b = table.get("one", &value);
    ASSERT_TRUE(b);
    ASSERT_TRUE(value == 1);
    ASSERT_EQ(table.size(), 1);
}

TEST(UTILS,PutValueGetAnotherFromTable)
{
    ConcurrentHashTable table;
    table.put("one", 1);
    uint16_t value;
    bool b = table.get("two", &value);
    ASSERT_FALSE(b);
}


TEST(UTILS,PutGetManyValuesTable)
{
    const char* lines[] = {
        "\\RAS\\Bytes Transmitted By Disconnected Clients",
        "\\RAS\\Bytes Received By Disconnected Clients",
        "\\RAS\\Failed Authentications",
        "\\RAS\\Max Clients",
        "\\RAS\\Total Clients",
        "\\WSMan Quota Statistics(*)\\Process ID",
        "\\WSMan Quota Statistics(*)\\Active Users",
        "\\WSMan Quota Statistics(*)\\Active Operations",
        "\\WSMan Quota Statistics(*)\\Active Shells",
        "\\WSMan Quota Statistics(*)\\System Quota Violations/Second",
        "\\WSMan Quota Statistics(*)\\User Quota Violations/Second",
        "\\WSMan Quota Statistics(*)\\Total Requests/Second",
        "\\Network QoS Policy(*)\\Packets dropped/sec",
        "\\Network QoS Policy(*)\\Packets dropped",
        "\\Network QoS Policy(*)\\Bytes transmitted/sec",
        "\\Network QoS Policy(*)\\Bytes transmitted",
        "\\Network QoS Policy(*)\\Packets transmitted/sec",
        "\\Network QoS Policy(*)\\Packets transmitted",
        "\\SMB Client Shares(*)\\Credit Stalls/sec",
        "\\SMB Client Shares(*)\\Metadata Requests/sec",
        "\\SMB Client Shares(*)\\Avg. Data Queue Length",
        "\\SMB Client Shares(*)\\Avg. Write Queue Length",
        "\\SMB Client Shares(*)\\Avg. Read Queue Length",
        "\\SMB Client Shares(*)\\Current Data Queue Length",
        "\\SMB Client Shares(*)\\Avg. sec/Data Request",
        "\\SMB Client Shares(*)\\Avg. Data Bytes/Request",
        "\\SMB Client Shares(*)\\Data Requests/sec",
        "\\SMB Client Shares(*)\\Data Bytes/sec",
        "\\SMB Client Shares(*)\\Avg. sec/Write",
        "\\SMB Client Shares(*)\\Avg. sec/Read",
        "\\SMB Client Shares(*)\\Avg. Bytes/Write",
        "\\SMB Client Shares(*)\\Avg. Bytes/Read",
        "\\SMB Client Shares(*)\\Write Requests/sec",
        "\\SMB Client Shares(*)\\Read Requests/sec",
        "\\SMB Client Shares(*)\\Write Bytes/sec",
        "\\SMB Client Shares(*)\\Read Bytes/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Boost Shared Owners/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Boost Excl. Owner/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Set Owner Pointer Shared (Existing Owner)/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Set Owner Pointer Shared (New Owner)/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Set Owner Pointer Exclusive/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource no-Waits AcqShrdWaitForExcl/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Contention AcqShrdWaitForExcl/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Recursive Sh. Acquires AcqShrdWaitForExcl/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Acquires AcqShrdWaitForExcl/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Recursive Excl. Acquires AcqShrdWaitForExcl/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Attempts AcqShrdWaitForExcl/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource no-Waits AcqShrdStarveExcl/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Contention AcqShrdStarveExcl/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Recursive Sh. Acquires AcqShrdStarveExcl/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Acquires AcqShrdStarveExcl/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Recursive Excl. Acquires AcqShrdStarveExcl/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Attempts AcqShrdStarveExcl/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource no-Waits AcqShrdLite/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Contention AcqShrdLite/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Recursive Sh. Acquires AcqShrdLite/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Acquires AcqShrdLite/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Recursive Excl. Acquires AcqShrdLite/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Attempts AcqShrdLite/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource no-Waits AcqExclLite/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Contention AcqExclLite/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Recursive Excl. Acquires AcqExclLite/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Acquires AcqExclLite/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Attempts AcqExclLite/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Total Conv. Exclusive To Shared/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Total Shared Releases/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Total Exclusive Releases/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Total Contentions/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Total Acquires/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Total Delete/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Total Re-Initialize/sec",
        "\\SynchronizationNuma(*)\\Exec. Resource Total Initialize/sec",
        "\\SynchronizationNuma(*)\\IPI Send Software Interrupts/sec",
        "\\SynchronizationNuma(*)\\IPI Send Routine Requests/sec",
        "\\SynchronizationNuma(*)\\IPI Send Broadcast Requests/sec",
        "\\Event Tracing for Windows\\Total Memory Usage --- Non-Paged Pool",
        "\\Event Tracing for Windows\\Total Memory Usage --- Paged Pool",
        "\\Event Tracing for Windows\\Total Number of Active Sessions",
        "\\Event Tracing for Windows\\Total Number of Distinct Disabled Providers",
        "\\Event Tracing for Windows\\Total Number of Distinct Pre-Enabled Providers",
        "\\Event Tracing for Windows\\Total Number of Distinct Enabled Providers",
        "\\Thermal Zone Information(*)\\Throttle Reasons",
        "\\Thermal Zone Information(*)\\% Passive Limit",
        "\\Thermal Zone Information(*)\\Temperature",
        "\\Processor Information(*)\\Performance Limit Flags",
        "\\Processor Information(*)\\% Performance Limit",
        "\\Processor Information(*)\\% Privileged Utility",
        "\\Processor Information(*)\\% Processor Utility",
        "\\Processor Information(*)\\% Processor Performance",
        "\\Processor Information(*)\\Idle Break Events/sec",
        "\\Processor Information(*)\\Average Idle Time",
        "\\Processor Information(*)\\Clock Interrupts/sec",
        "\\Processor Information(*)\\Processor State Flags",
        "\\Processor Information(*)\\% of Maximum Frequency",
        "\\Processor Information(*)\\Processor Frequency",
        "\\Processor Information(*)\\Parking Status",
        "\\Processor Information(*)\\% Priority Time",
        "\\Processor Information(*)\\C3 Transitions/sec",
        "\\Processor Information(*)\\C2 Transitions/sec",
        "\\Processor Information(*)\\C1 Transitions/sec",
        "\\Processor Information(*)\\% C3 Time",
        "\\Processor Information(*)\\% C2 Time",
        "\\Processor Information(*)\\% C1 Time",
        "\\Processor Information(*)\\% Idle Time",
        "\\Processor Information(*)\\DPC Rate",
        "\\Processor Information(*)\\DPCs Queued/sec",
        "\\Processor Information(*)\\% Interrupt Time",
        "\\Processor Information(*)\\% DPC Time",
        "\\Processor Information(*)\\Interrupts/sec",
        "\\Processor Information(*)\\% Privileged Time",
        "\\Processor Information(*)\\% User Time",
        "\\Processor Information(*)\\% Processor Time",
        "\\Event Tracing for Windows Session(*)\\Number of Real-Time Consumers",
        "\\Event Tracing for Windows Session(*)\\Events Lost",
        "\\Event Tracing for Windows Session(*)\\Events Logged per sec",
        "\\Event Tracing for Windows Session(*)\\Buffer Memory Usage -- Non-Paged Pool",
        "\\Event Tracing for Windows Session(*)\\Buffer Memory Usage -- Paged Pool",
        "\\FileSystem Disk Activity(*)\\FileSystem Bytes Written",
        "\\FileSystem Disk Activity(*)\\FileSystem Bytes Read",
        "\\RDMA Activity(*)\\RDMA Outbound Frames/sec",
        "\\RDMA Activity(*)\\RDMA Inbound Frames/sec",
        "\\RDMA Activity(*)\\RDMA Outbound Bytes/sec",
        "\\RDMA Activity(*)\\RDMA Inbound Bytes/sec",
        "\\RDMA Activity(*)\\RDMA Completion Queue Errors",
        "\\RDMA Activity(*)\\RDMA Active Connections",
        "\\RDMA Activity(*)\\RDMA Connection Errors",
        "\\RDMA Activity(*)\\RDMA Failed Connection Attempts",
        "\\RDMA Activity(*)\\RDMA Accepted Connections",
        "\\RDMA Activity(*)\\RDMA Initiated Connections",
        "\\Per Processor Network Activity Cycles(*)\\Interrupt DPC Latency Cycles/sec",
        "\\Per Processor Network Activity Cycles(*)\\Stack Send Complete Cycles/sec",
        "\\Per Processor Network Activity Cycles(*)\\Miniport RSS Indirection Table Change Cycles",
        "\\Per Processor Network Activity Cycles(*)\\Build Scatter Gather Cycles/sec",
        "\\Per Processor Network Activity Cycles(*)\\NDIS Send Complete Cycles/sec",
        "\\Per Processor Network Activity Cycles(*)\\Miniport Send Cycles/sec",
        "\\Per Processor Network Activity Cycles(*)\\NDIS Send Cycles/sec",
        "\\Per Processor Network Activity Cycles(*)\\Miniport Return Packet Cycles/sec",
        "\\Per Processor Network Activity Cycles(*)\\NDIS Return Packet Cycles/sec",
        "\\Per Processor Network Activity Cycles(*)\\Stack Receive Indication Cycles/sec",
        "\\Per Processor Network Activity Cycles(*)\\NDIS Receive Indication Cycles/sec",
        "\\Per Processor Network Activity Cycles(*)\\Interrupt Cycles/sec",
        "\\Per Processor Network Activity Cycles(*)\\Interrupt DPC Cycles/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Packets Coalesced/sec",
        "\\Per Processor Network Interface Card Activity(*)\\DPCs Deferred/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Tcp Offload Send bytes/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Tcp Offload Receive bytes/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Tcp Offload Send Request Calls/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Tcp Offload Receive Indications/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Low Resource Received Packets/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Low Resource Receive Indications/sec",
        "\\Per Processor Network Interface Card Activity(*)\\RSS Indirection Table Change Calls/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Build Scatter Gather List Calls/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Sent Complete Packets/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Sent Packets/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Send Complete Calls/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Send Request Calls/sec",
        "\\Per Processor Network Interface Card Activity(*)\\DPCs Queued on Other CPUs/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Returned Packets/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Received Packets/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Return Packet Calls/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Receive Indications/sec",
        "\\Per Processor Network Interface Card Activity(*)\\Interrupts/sec",
        "\\Per Processor Network Interface Card Activity(*)\\DPCs Queued/sec",
        "\\Physical Network Interface Card Activity(*)\\Low Power Transitions (Lifetime)",
        "\\Physical Network Interface Card Activity(*)\\% Time Suspended (Lifetime)",
        "\\Physical Network Interface Card Activity(*)\\% Time Suspended (Instantaneous)",
        "\\Physical Network Interface Card Activity(*)\\Device Power State",
        "\\Distributed Routing Table(*)\\Unrecognized Messages Received",
        "\\Distributed Routing Table(*)\\Lookup Messages Received/second",
        "\\Distributed Routing Table(*)\\Lookup Messages Sent/second",
        "\\Distributed Routing Table(*)\\Ack Messages Received/second",
        "\\Distributed Routing Table(*)\\Ack Messages Sent/second",
        "\\Distributed Routing Table(*)\\Authority Messages Received/second",
        "\\Distributed Routing Table(*)\\Authority Sent/second",
        "\\Distributed Routing Table(*)\\Inquire Messages Received/second",
        "\\Distributed Routing Table(*)\\Inquire Messages Sent/second",
        "\\Distributed Routing Table(*)\\Flood Messages Received/second",
        "\\Distributed Routing Table(*)\\Flood Messages Sent/second",
        "\\Distributed Routing Table(*)\\Request Messages Received/second",
        "\\Distributed Routing Table(*)\\Request Messages Sent/second",
        "\\Distributed Routing Table(*)\\Advertise Messages Received/second",
        "\\Distributed Routing Table(*)\\Advertise Messages Sent/second",
        "\\Distributed Routing Table(*)\\Solicit Messages Received/second",
        "\\Distributed Routing Table(*)\\Solicit Messages Sent/second",
        "\\Distributed Routing Table(*)\\Receive Failures",
        "\\Distributed Routing Table(*)\\Send Failures",
        "\\Distributed Routing Table(*)\\Stale Cache Entries",
        "\\Distributed Routing Table(*)\\Estimated cloud size",
        "\\Distributed Routing Table(*)\\Average Bytes/second Received",
        "\\Distributed Routing Table(*)\\Average Bytes/second Sent",
        "\\Distributed Routing Table(*)\\Cache Entries",
        "\\Distributed Routing Table(*)\\Searches",
        };

    uint16_t count = (uint16_t) (sizeof(lines) / sizeof(*lines));
    ConcurrentHashTable table;
    for (uint16_t index = 0; index < count; ++index) {
        table.put(lines[index], index);
    }

    for (uint16_t index = 0; index < count; ++index) {
        uint16_t value;
        ASSERT_TRUE(table.get(lines[index], &value));
        ASSERT_TRUE(value == index);
    }

    //table.testDistribution();
}

TEST(UTILS,PutGetALotValuesTable)
{
    const uint16_t COUNT = 1024*8;
    ConcurrentHashTable table;
    for (uint16_t index = 0; index < COUNT; ++index) {
        string s = boost::lexical_cast<string>(index);
        table.put(s.c_str(), index);
    }

    for (uint16_t index = 0; index < COUNT; ++index) {
        string s = boost::lexical_cast<string>(index);
        uint16_t value;
        ASSERT_TRUE(table.get(s.c_str(), &value));
        ASSERT_TRUE(value == index);
    }

    //table.testDistribution();
}

static void thread_fun(boost::barrier& bar, ConcurrentHashTable& table)
{
    const uint16_t COUNT = 1024*8;
    bar.wait();
    for (uint16_t index = 0; index < COUNT; ++index) {
        string s = boost::lexical_cast<string>(index);
        table.put(s.c_str(), index);
    }
}

TEST(UTILS,PutConcurrentTable)
{
    boost::barrier bar(4);
    ConcurrentHashTable table(8);

    boost::thread thr1(boost::bind(&thread_fun, boost::ref(bar), boost::ref(table)));
    boost::thread thr2(boost::bind(&thread_fun, boost::ref(bar), boost::ref(table)));
    boost::thread thr3(boost::bind(&thread_fun, boost::ref(bar), boost::ref(table)));
    boost::thread thr4(boost::bind(&thread_fun, boost::ref(bar), boost::ref(table)));
    thr1.join();
    thr2.join();
    thr3.join();
    thr4.join();

    uint32_t result = table.size();
    ASSERT_EQ(1024*8, result);
}

TEST(UTILS,TableCursor)
{
    ConcurrentHashTable table(8);
    const uint16_t COUNT = 4;
    for (uint16_t index = 0; index < COUNT; ++index) {
        string s = boost::lexical_cast<string>(index);
        table.put(s.c_str(), index);
    }

    ConcurrentHashTable::Cursor cursor = table.getCursor();
    std::set<std::string> control;
    std::string str;
    while (cursor.next(str)) {
        control.insert(str);
    }

    ASSERT_EQ(COUNT, control.size());
    for (uint16_t index = 0; index < COUNT; ++index) {
        string s = boost::lexical_cast<string>(index);
        ASSERT_TRUE(control.find(s) != control.end());
    }
}
