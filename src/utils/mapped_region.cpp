/**********************************************
   File:   mapped_region.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "clockwork_errors.h"
#include "clockwork_base.h"
#include "utils/file_io.h"
#include "utils/mapped_region.h"
#include "utils/log.h"
#include "utils/verify.h"
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

using std:: string;

namespace cdb {

/***********************************************
 *   MappedFile
 */
MappedFile::MappedFile()
  : m_fileMapping(NULL)
{

}

MappedFile::~MappedFile()
{
    close();
}

void MappedFile::allocate(const char* path, size_t size)
{
    LOG_TRACE << "Allocating file " << path << " with size " << size;

    if (FileIo::exists(path)) {
        if (FileIo::getSize(path) >= size) {
            string errMsg = string("Reducing file size is not allowed: ") + path;
            LOG_ERROR << errMsg;
            THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
        }
    }

    FileIo file;
    file.open(path, FileIo::APPEND);
    file.truncate(size);
}

void MappedFile::open(const char* path, MappedFileAccess dataAccess)
{
	CHECK_ARG_NON_ZERO(path);
    m_path = path;

    LOG_TRACE << "Open file mapping on " << m_path << " with data access " << dataAccess;

    int dataAccessMode;
    if (dataAccess == MappedFile::READ_ONLY)
        dataAccessMode = O_RDONLY;
    else
        dataAccessMode = O_RDWR;

    int fd = ::open(m_path.c_str(), dataAccessMode);
    if (fd < 0) {
        string errMsg = string("Failed to map file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }

    m_fileMapping = new int(fd);
}

void MappedFile::close()
{
    if (m_fileMapping != NULL) {
        LOG_TRACE << "Close file mapping on " << m_path;
        int* pfd = static_cast<int*>(m_fileMapping);
        ::close(*pfd);
        delete pfd;
        m_fileMapping = NULL;
    }
}

MappedRegionPtr MappedFile::get(size_t offset, size_t size, MappedFileAccess dataAccess)
{
    LOG_TRACE << "Create mapped region on " << m_path << " offset=" << offset << " size=" << size;

    if (offset >= size) {
        THROW("Invalid size and offset", CWSE_ERROR_INVALID_ARG);
    }

    if (m_fileMapping == NULL) {
        string errMsg = string("File mapping for ") + m_path + " is not initialized";
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_OBJECT_NOT_INITIALIZED);
    }

    int dataAccessMode;
    if (dataAccess == MappedFile::READ_ONLY)
        dataAccessMode = PROT_READ;
    else
        dataAccessMode = PROT_READ | PROT_WRITE;

    int* pfd = static_cast<int*>(m_fileMapping);
    void* ptr = ::mmap(0, size, dataAccessMode, MAP_SHARED, *pfd, 0);
    if (ptr == MAP_FAILED) {
        string errMsg = string("Failed to create mapped region on ") + m_path;
        LOG_ERROR << errMsg << " offset=" << offset << " size=" << size;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }

    return MappedRegionPtr(new MappedRegion(ptr, pfd, offset, size));
}

/***********************************************
 *   MappedRegion
 */
MappedRegion::MappedRegion(void* mappedRegion, void* pfd, size_t offset, size_t size)
  : m_mappedRegion(mappedRegion),
    m_pfd(pfd),
    m_offset(offset),
    m_size(size)
{
    if (m_mappedRegion == NULL) {
        string errMsg = string("File region is not initialized");
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_OBJECT_NOT_INITIALIZED);
    }
}

MappedRegion::~MappedRegion()
{
    if (m_mappedRegion != NULL) {
        ::munmap(m_mappedRegion, m_size);
        m_mappedRegion = NULL;
    }
}

char* MappedRegion::get()
{
    return static_cast<char*>(m_mappedRegion) + m_offset;
}

void MappedRegion::flush()
{
    int* pfd = static_cast<int*>(m_pfd);
    fdatasync(*pfd);
}

}
