/**********************************************
   File:   file_lock.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "utils/file_lock.h"
#include "utils/file_io.h"
#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#else
#include <unistd.h>
#include <fcntl.h>
#endif

namespace cdb {

#ifdef WIN32
FileLock::~FileLock()
{
    if (m_lock != NULL) {
        HANDLE* ph = (HANDLE*) m_lock;
        CloseHandle(*ph);
        delete ph;
    }
}

bool FileLock::lock(const char* path)
{
    DWORD dwCreationDisposition = OPEN_ALWAYS;
    DWORD dwDesiredAccess  = FILE_WRITE_DATA;

    HANDLE h = CreateFileA(path, dwDesiredAccess , 0, NULL, dwCreationDisposition, FILE_ATTRIBUTE_NORMAL, NULL);
    if (h == INVALID_HANDLE_VALUE) return false;

    m_lock = new HANDLE(h);

    return true;
}
#else
FileLock::~FileLock()
{
    if (m_lock != NULL) {
        int* pfd = (int*) m_lock;

        struct flock fileLock;
        fileLock.l_type = F_UNLCK;
        fileLock.l_start = 0;
        fileLock.l_whence = SEEK_SET;
        fileLock.l_len = 0;
        fileLock.l_pid = getpid();

        fcntl(*pfd, F_SETLK, &fileLock);
        close(*pfd);
        delete pfd;
    }
}

bool FileLock::lock(const char* path)
{
    int fd = open(path, O_CREAT | O_RDWR, 0600);
    if (fd < 0) {
        return false;
    }

    struct flock fileLock;
    fileLock.l_type = F_WRLCK;
    fileLock.l_start = 0;
    fileLock.l_whence = SEEK_SET;
    fileLock.l_len = 0;
    fileLock.l_pid = getpid();

    int ret = fcntl(fd, F_SETLK, &fileLock);
    if (ret < 0) {
        close(fd);
        return false;
    }

    m_lock = new int(fd);

    return true;
}
#endif

} // namespace cdb
