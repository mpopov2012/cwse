/**********************************************
   File:   utest_string_utils.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "utils/string_utils.h"
#include "gtest/gtest.h"

using namespace std;
using namespace cdb;

TEST(UTILS,Casing)
{
    {  string s = "one"; StringUtils::toUpper(s); ASSERT_EQ(string("ONE"), s); }
}

TEST(UTILS,Trimming)
{
    {  string s = "  one  "; StringUtils::trim(s); ASSERT_EQ(string("one"), s); }
    {  string s = "one "; StringUtils::trim(s); ASSERT_EQ(string("one"), s); }
    {  string s = " one"; StringUtils::trim(s); ASSERT_EQ(string("one"), s); }
    {  string s = "  "; StringUtils::trim(s); ASSERT_EQ(string(""), s); }
    {  string s = ""; StringUtils::trim(s); ASSERT_EQ(string(""), s); }

    {  string s = "\tone \t \t "; StringUtils::trim(s); ASSERT_EQ(string("one"), s); }
    {  string s = "\t"; StringUtils::trim(s); ASSERT_EQ(string(""), s); }
}

void testOneTwoThree(const string& s, const char* delim)
{
    vector< string > t;
    StringUtils::split(s, delim, t);
    ASSERT_EQ(3, t.size());
    ASSERT_EQ(string("1"), t[0]); ASSERT_EQ(string("2"), t[1]); ASSERT_EQ(string("3"), t[2]);
}

TEST(UTILS,Splitting)
{
    testOneTwoThree("1 2 3", " ");
    testOneTwoThree("   1   2    3   ", " ");
    testOneTwoThree("\t  1 \t  2 \t   3 \t\t  ", " \t");
}
