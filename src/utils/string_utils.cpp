/**********************************************
   File:   string_utils.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "utils/string_utils.h"
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>

namespace cdb {

void StringUtils::split(const std::string& str, const char* delims, std::vector< std::string >& tokens)
{
    boost::char_separator< char > sep(delims);
    typedef boost::tokenizer< boost::char_separator<char> > tokenizer;
    tokenizer tok(str, sep);
    tokenizer::iterator iter = tok.begin();
    for (; iter != tok.end(); ++iter) {
        tokens.push_back(*iter);
    }
}

void StringUtils::trim(std::string& str)
{
    boost::algorithm::trim(str);
}

void StringUtils::toUpper(std::string& str)
{
    boost::algorithm::to_upper(str);
}

} // namespace cdb

