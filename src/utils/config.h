/**********************************************
   File:   config.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_UTILS_CONFIG_H
#define CDB_UTILS_CONFIG_H

#include <map>
#include <string>

namespace cdb {

class Config
{
public:
    typedef std::map< std::string, std::string >::const_iterator Iterator;

public:
    void process(const char* args[], int argv=0);
    void process(std::string str);
    bool find(const std::string& key, std::string& value) const;
    bool find(const std::string& key) const { return (m_items.find(key) != m_items.end()); }
    void add(const std::string& key, const std::string& value) { m_items[key] = value; }
    Iterator begin() const { return m_items.begin(); }
    Iterator end() const { return m_items.end(); }

private:
    std::map< std::string, std::string > m_items;

};

} // namespace cdb

#endif // CDB_UTILS_CONFIG_H
