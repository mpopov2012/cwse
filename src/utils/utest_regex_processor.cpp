/**********************************************
   File:   utest_regex_processor.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "utils/regex_processor.h"
#include "gtest/gtest.h"

using namespace std;
using namespace cdb;

TEST(UTILS,StraigntPattern)
{
    const char* NAME_PATTERN = "^[a-zA-Z_][a-zA-Z0-9_\\.]*$";
    Regex rx;
    rx.compile(NAME_PATTERN, Regex::STRAIGHT);

    ASSERT_TRUE(rx.match("abc"));
    ASSERT_TRUE(rx.match("abc.def"));
    ASSERT_TRUE(rx.match("_abc.def"));
    ASSERT_TRUE(rx.match("_abc.def.99"));

    ASSERT_FALSE(rx.match("1abc"));
    ASSERT_FALSE(rx.match(".abc"));
    ASSERT_FALSE(rx.match("a$bc"));
}

TEST(UTILS,WildCharsPattern)
{
    {
        const char* pattern = "abc.*";
        Regex rx;
        rx.compile(pattern, Regex::WILD_CHARS);
        ASSERT_TRUE(rx.match("abc.def"));
        ASSERT_TRUE(rx.match("abc.def.99"));
        ASSERT_FALSE(rx.match("bbc.def.99"));
        ASSERT_FALSE(rx.match("bbc.abc.def.99"));
    }

    {
        const char* pattern = "abc.*.*";
        Regex rx;
        rx.compile(pattern, Regex::WILD_CHARS);
        ASSERT_TRUE(rx.match("abc.def.99"));
    }
}
