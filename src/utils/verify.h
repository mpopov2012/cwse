/**********************************************
   File:   verify.h

   This code is based on the sources of a log class published in Dr.Dobbs

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_UTILS_VERIFY_H
#define CDB_UTILS_VERIFY_H

#include "clockwork_errors.h"
#include "clockwork_base.h"

#define METHOD_NOT_IMPLEMENTED(p) { THROW("Functionality " #p " not implemented", CWSE_ERROR_NOT_IMPLEMENTED); }
#define CHECK_ARG_NON_ZERO(p) { if ((p) == NULL) THROW("Argument " #p " must not be NULL", CWSE_ERROR_INVALID_ARG); }
#define CHECK_ARG_POSITIVE(p) { if ((p) == 0) THROW("Argument " #p " must be larger than 0", CWSE_ERROR_INVALID_ARG); }
#define CHECK_DATA_MEMBER_INITIALIZED(p) { if ((p) == NULL) THROW("Data member " #p " must not be NULL", CWSE_ERROR_OBJECT_NOT_INITIALIZED); }
#define CHECK_DATA_MEMBER_INITIALIZED_VAL(p,NULLVAL) { if ((p) == NULLVAL) THROW("Data member " #p " must not be NULL", CWSE_ERROR_OBJECT_NOT_INITIALIZED); }

#endif // CDB_UTILS_VERIFY_H
