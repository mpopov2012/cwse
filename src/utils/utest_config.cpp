/**********************************************
   File:   utest_config.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "utils/config.h"
#include "gtest/gtest.h"

using namespace std;
using namespace cdb;

TEST(UTILS,CommandLineArgs)
{
    const char* lines[] = { "--path", "one", "--create", "--access", "two", NULL };
    Config config;
    config.process(lines);

    string str;
    ASSERT_TRUE(config.find("path", str));
    ASSERT_EQ(string("one"), str);
    ASSERT_TRUE(config.find("access", str));
    ASSERT_EQ(string("two"), str);
    ASSERT_TRUE(config.find("create", str));

    int count = 0;
    for (Config::Iterator iter = config.begin(); iter != config.end(); ++iter) count++;
    ASSERT_EQ(3, count);
}

TEST(UTILS,ConfigFileLine)
{
    Config config;
    config.process("path=one");
    config.process("access=two");
    config.process("create=");

    string str;
    ASSERT_TRUE(config.find("path", str));
    ASSERT_EQ(string("one"), str);
    ASSERT_TRUE(config.find("access", str));
    ASSERT_EQ(string("two"), str);
    ASSERT_TRUE(config.find("create", str));

    int count = 0;
    for (Config::Iterator iter = config.begin(); iter != config.end(); ++iter) count++;
    ASSERT_EQ(3, count);
}
