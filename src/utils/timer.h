/**********************************************
   File:   timer.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_UTILS_TIMER_H
#define CDB_UTILS_TIMER_H

#include <stddef.h>
#include <stdint.h>

namespace cdb {

class Timer
{
public:
    static uint64_t getCurrentTimestampMs();
    static void sleepSec(int sec);
    static void sleepMsec(int msec);
};

} // namespace cdb

#endif // CDB_UTILS_TIMER_H
