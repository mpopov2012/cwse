/**********************************************
   File:   mapped_region_windows.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "clockwork_errors.h"
#include "clockwork_base.h"
#include "utils/file_io.h"
#include "utils/mapped_region.h"
#include "utils/log.h"
#include "utils/verify.h"
#include <Windows.h>

using std:: string;

namespace cdb {

/***********************************************
 *   MappedFile
 */
MappedFile::MappedFile()
  : m_fileMapping(NULL)
{

}

MappedFile::~MappedFile()
{
    close();
}

void MappedFile::allocate(const char* path, size_t size)
{
    LOG_TRACE << "Allocating file " << path << " with size " << size;

    if (FileIo::exists(path)) {
        if (FileIo::getSize(path) >= size) {
            string errMsg = string("Reducing file size is not allowed: ") + path;
            LOG_ERROR << errMsg;
            THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
        }
    }

    FileIo file;
    file.open(path, FileIo::APPEND);
    file.truncate(size);
}

void MappedFile::open(const char* path, MappedFileAccess dataAccess)
{
	CHECK_ARG_NON_ZERO(path);
    m_path = path;

    LOG_TRACE << "Open file mapping on " << m_path << " with data access " << dataAccess;

    DWORD dataAccessMode;
    DWORD sharingMode;
    DWORD mapAccessMode;
    if (dataAccess == MappedFile::READ_ONLY) {
        dataAccessMode = GENERIC_READ;
        sharingMode = FILE_SHARE_READ | FILE_SHARE_WRITE;
        mapAccessMode = PAGE_READONLY;
    }
    else {
        dataAccessMode = GENERIC_READ | GENERIC_WRITE;
        sharingMode = FILE_SHARE_READ | FILE_SHARE_WRITE;
        mapAccessMode = PAGE_READWRITE;
    }

	HANDLE hFile = ::CreateFileA(
		path,
		dataAccessMode,
		sharingMode,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);
	if (hFile == INVALID_HANDLE_VALUE) {
        THROW("Failed to open file for mapping", CWSE_ERROR_FAILED_FILE_OPERATION);
	}

 	HANDLE hMap = ::CreateFileMapping(
		hFile,
		NULL,
		mapAccessMode,
		0,
		0,
		NULL);
    ::CloseHandle(hFile);
	if (hMap == NULL) {
        THROW("Failed to map file", CWSE_ERROR_FAILED_FILE_OPERATION);
	}

    m_fileMapping = hMap;
}

void MappedFile::close()
{
    if (m_fileMapping != NULL) {
        LOG_TRACE << "Close file mapping on " << m_path;
        HANDLE hMap = static_cast<HANDLE>(m_fileMapping);
        ::CloseHandle(hMap);
        m_fileMapping = NULL;
    }
}

MappedRegionPtr MappedFile::get(size_t offset, size_t size, MappedFileAccess dataAccess)
{
    LOG_TRACE << "Create mapped region on " << m_path << " offset=" << offset << " size=" << size;

    if (offset >= size) {
        THROW("Invalid size and offset", CWSE_ERROR_INVALID_ARG);
    }

    if (m_fileMapping == NULL) {
        string errMsg = string("File mapping for ") + m_path + " is not initialized";
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_OBJECT_NOT_INITIALIZED);
    }

    int dataAccessMode;
    if (dataAccess == MappedFile::READ_ONLY)
        dataAccessMode = FILE_MAP_READ;
    else
        dataAccessMode = FILE_MAP_READ | FILE_MAP_WRITE;

    HANDLE hMap = static_cast<HANDLE>(m_fileMapping);
	char* ptr = (char*) ::MapViewOfFile(
		hMap,
		dataAccessMode,
		0,
		0,
		0);
	if (ptr == NULL) {
        DWORD err = GetLastError();
        THROW("Failed to map view", CWSE_ERROR_OBJECT_NOT_INITIALIZED);
	}

    return MappedRegionPtr(new MappedRegion(ptr, hMap, offset, size));
}

/***********************************************
 *   MappedRegion
 */
MappedRegion::MappedRegion(void* mappedRegion, void* pfd, size_t offset, size_t size)
  : m_mappedRegion(mappedRegion),
    m_pfd(pfd),
    m_offset(offset),
    m_size(size)
{
    if (m_mappedRegion == NULL) {
        string errMsg = string("File region is not initialized");
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_OBJECT_NOT_INITIALIZED);
    }
}

MappedRegion::~MappedRegion()
{
    if (m_mappedRegion != NULL) {
        ::UnmapViewOfFile(m_mappedRegion);
        m_mappedRegion = NULL;
    }
}

char* MappedRegion::get()
{
    return static_cast<char*>(m_mappedRegion) + m_offset;
}

void MappedRegion::flush()
{
    if (m_mappedRegion != NULL) {
        FlushViewOfFile(m_mappedRegion, m_size);
    }
}

}
