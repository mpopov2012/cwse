/**********************************************
   File:   thread_windows.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "clockwork_errors.h"
#include "clockwork_base.h"
#include "utils/thread.h"
#include <Windows.h>

namespace cdb {

static DWORD WINAPI thread_func(void* obj)
{
    Thread* thread = static_cast<Thread*>(obj);
    thread->run();
    return 0;
}

/****************************************************
 *
 * Thread
 *
 ****************************************************/

void Thread::start()
{
    if (m_thread != NULL) {
        THROW("Thread already running", CWSE_ERROR_SYSTEM);
    }

    m_keepGoing = true;
    DWORD threadId;
    m_thread = ::CreateThread(NULL, 0, thread_func, this, NULL, &threadId);
    if (m_thread == NULL) {
        THROW("Failed to create thread", CWSE_ERROR_SYSTEM);
    }
}

void Thread::stop()
{
    if (m_thread == NULL) return;

    m_keepGoing = false;

    ::WaitForSingleObject(m_thread, INFINITE);
    ::CloseHandle(m_thread);

    m_thread = NULL;
}

/****************************************************
 *
 * TimerThread
 *
 ****************************************************/

struct ThreadTimer
{
    HANDLE m_hStopEvent;
    HANDLE m_hWaitableTimer;
};

void TimerThread::start()
{
    if (m_timer != NULL) {
        THROW("Thread already running", CWSE_ERROR_SYSTEM);
    }

    ThreadTimer tt;
    memset(&tt, 0, sizeof(ThreadTimer));

    tt.m_hStopEvent     = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	tt.m_hWaitableTimer = ::CreateWaitableTimer(NULL, FALSE, NULL);

    LARGE_INTEGER dueTime;
    dueTime.QuadPart = m_ms * -10000LL;
	SetWaitableTimer(tt.m_hWaitableTimer, &dueTime, m_ms, NULL, NULL, 0);

    m_timer = new ThreadTimer(tt);

    Thread::start();
}

void TimerThread::stop()
{
    if (m_timer == NULL) return;

    ThreadTimer* p = static_cast<ThreadTimer*>(m_timer);
    ::SetEvent(p->m_hStopEvent);

    Thread::stop();

    ::CloseHandle(p->m_hStopEvent);
    ::CloseHandle(p->m_hWaitableTimer);
    delete p;

    m_timer = NULL;
}

void TimerThread::run()
{
    ThreadTimer* p = static_cast<ThreadTimer*>(m_timer);
    HANDLE handles[] = { p->m_hStopEvent, p->m_hWaitableTimer };

    while (m_keepGoing) {
        DWORD index = WaitForMultipleObjects(2, handles, FALSE, INFINITE);
        if (index == 0) {
            break;
        }
        onTimer(m_ptr);
    }
}


} // namespace cdb
