/**********************************************
   File:   atomic.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_UTILS_ATOMIC_H
#define CDB_UTILS_ATOMIC_H

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#ifdef WIN32
#include <Windows.h>
#endif

namespace cdb {

class Atomic
{
public:
    static inline void inc64(volatile int64_t* base, int64_t val)
    {
    #ifdef WIN32
        InterlockedExchangeAdd64(base, val);
    #else
        __sync_add_and_fetch(base, val);
    #endif
    }

    static inline void incd(volatile double* base, double val)
    {
       double oldVal, newVal;
       do {
           oldVal = *base;
           newVal = oldVal + val;
       } while (!Atomic::cas64(base, &oldVal, &newVal));
    }

    static inline bool cas64(volatile void* base, void* oldval, void* newval)
    {
        int64_t* oldValPtr = (int64_t*) oldval;
        int64_t* newValPtr = (int64_t*) newval;
        volatile int64_t* dest = (volatile int64_t*) base;

    #ifdef WIN32
        int64_t p = InterlockedCompareExchange64(dest, *newValPtr, *oldValPtr);
        return p == *oldValPtr;
    #else
        return __sync_bool_compare_and_swap(dest, *oldValPtr, *newValPtr);
    #endif
    }

    static inline bool casptr(void* volatile* base, void* oldval, void* newval)
    {
    #ifdef WIN32
        void* p = InterlockedCompareExchangePointer(base, newval, oldval);
        return p == oldval;
    #else
        return __sync_bool_compare_and_swap(base, oldval, newval);
    #endif
    }


};

} // namespace cdb

#endif // CDB_UTILS_ATOMIC_H
