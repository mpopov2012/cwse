/**********************************************
   File:   utest_thread.cpp

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "clockwork_errors.h"
#include "utils/thread.h"
#include "utils/timer.h"
#include "gtest/gtest.h"
#include <string>
#include <iostream>

using namespace std;
using namespace cdb;

class SimpleThread : public Thread
{
public:
    SimpleThread() : Thread(), m_touched(false) {}
    virtual void run() {
        m_touched = true;
    }

public:
    bool m_touched;
};

TEST(UTILS,BasicThread)
{
    SimpleThread thread;
    thread.start();
    thread.stop();
    ASSERT_TRUE(thread.m_touched);
}

class SimpleTimerThread : public TimerThread
{
public:
    SimpleTimerThread(int ms, void* ptr) : TimerThread(ms, ptr), m_count(0) {}
    virtual void onTimer(void*) { m_count++; }
public:
    int m_count;
};

TEST(UTILS,BasicTimerThread)
{
    SimpleTimerThread thread(20, NULL);
    thread.start();
    Timer::sleepMsec(240);
    thread.stop();
    ASSERT_TRUE(thread.m_count > 10) << "count=" << thread.m_count << endl;
}
