/**********************************************
   File:   thread.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_UTILS_THREAD_H
#define CDB_UTILS_THREAD_H

#include <stddef.h>

namespace cdb {

class Thread
{
public:
    Thread() : m_keepGoing(true), m_thread(NULL) {}
    virtual ~Thread() { stop(); }

    virtual void start();
    virtual void stop();
    virtual void run() = 0;

protected:
    bool   m_keepGoing;
    void*  m_thread;

};

class TimerThread : public Thread
{
public:
    TimerThread(int ms, void* ptr) : Thread(), m_timer(NULL), m_ms(ms), m_ptr(ptr) {}

    virtual void start();
    virtual void stop();
    virtual void run();
    virtual void onTimer(void* ptr) = 0;

    void setTimerHandler(void* ptr) { m_ptr = ptr; }

private:
    void* m_timer;
    int   m_ms;
    void* m_ptr;
};

} // namespace cdb

#endif // CDB_UTILS_THREAD_H
