/**********************************************
   File:   fast_rand.h

   Xorshift generator. See http://en.wikipedia.org/wiki/Xorshift

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_UTILS_FAST_RAND_H
#define CDB_UTILS_FAST_RAND_H

#include "utils/timer.h"
#include <stddef.h>
#include <stdint.h>

namespace cdb {

class FastRand
{
public:
    FastRand() {
        m_seed[0] = Timer::getCurrentTimestampMs() ^ (uint64_t)this;
        m_seed[1] = Timer::getCurrentTimestampMs() & (uint64_t)this;
    }
    FastRand(const FastRand& a) {
        m_seed[0] = a.m_seed[0];
        m_seed[1] = a.m_seed[1];
    }
    FastRand& operator=(const FastRand& a) { if (&a != this) { m_seed[0] = a.m_seed[0]; m_seed[1] = m_seed[1]; } return *this; }

    inline uint64_t next(void) { 
        uint64_t s1 = m_seed[0];
        const uint64_t s0 = m_seed[1];
        m_seed[0] = s0;
        s1 ^= s1 << 23;
        return (m_seed[1] = (s1 ^ s0 ^ (s1 >> 17) ^ (s0 >> 26))) + s0;
    }

private:
    uint64_t m_seed[2];
};

} // namespace cdb

#endif // CDB_UTILS_FAST_RAND_H
