/**********************************************
   File:   utest_utils.cpp

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "clockwork_errors.h"
#include "utils/log.h"
#include "utils/verify.h"
#include "utils/fast_rand.h"
#include "gtest/gtest.h"
#include <string>
#include <iostream>

using namespace std;
using namespace cdb;

TEST(UTILS,Exception)
{
    string file;
    int line = 0;
    string cause="test";
    bool caught = false;
    try { file=__FILE__; line=__LINE__; THROW(cause, CWSE_ERROR_INVALID_ARG); }
    catch(CwException& e) {
        ASSERT_EQ(line,e.line);
        ASSERT_EQ(file,e.file);
        ASSERT_EQ(cause,e.what());
        caught = true;
    }
    ASSERT_TRUE(caught);
}

TEST(UTILS,Verify)
{
    bool caught = false;
    const char* ptr = NULL;
    try {
        CHECK_ARG_NON_ZERO(ptr);
    }
    catch (CwException& e) {
        ASSERT_TRUE(e.errorID == CWSE_ERROR_INVALID_ARG);
        caught = true;
    }
    ASSERT_TRUE(caught);
}


