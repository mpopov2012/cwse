/**********************************************
   File:   utest_atomic.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "utils/atomic.h"
#include "gtest/gtest.h"
#include <boost/thread.hpp>
#include <boost/thread/barrier.hpp>
#include <boost/bind.hpp>

using namespace std;
using namespace cdb;

static const int COUNT = 1024*8;

static void thread_fun(boost::barrier& bar, double* target)
{
    bar.wait();
    for (int index = 0; index < COUNT; ++index) {
        Atomic::incd(target, 1);
    }
}

TEST(UTILS,AtomicDoubleIncrement)
{
    const int d = 4;
    double target = 0.0;
    boost::barrier bar(d);

    boost::thread thr1(boost::bind(&thread_fun, boost::ref(bar), &target));
    boost::thread thr2(boost::bind(&thread_fun, boost::ref(bar), &target));
    boost::thread thr3(boost::bind(&thread_fun, boost::ref(bar), &target));
    boost::thread thr4(boost::bind(&thread_fun, boost::ref(bar), &target));
    thr1.join();
    thr2.join();
    thr3.join();
    thr4.join();

    int val = (int) target;
    ASSERT_EQ(COUNT*d, val);
}
