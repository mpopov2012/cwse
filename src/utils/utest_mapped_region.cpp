/**********************************************
   File:   utest_mapped_region.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "clockwork_errors.h"
#include "utils/mapped_region.h"
#include "utils/file_io.h"
#include "utils/log.h"
#include "gtest/gtest.h"
#include <string>
#include <iostream>

using namespace std;
using namespace cdb;

TEST(UTILS,BasicMappedRegion)
{
    FILELog::Level() = LL_INFO;

    string path = FileIo::getTempName("/tmp");
    try {
        MappedFile::allocate(path.c_str(), 16);

        MappedFile mappedFile1;
        mappedFile1.open(path.c_str(), MappedFile::READ_WRITE);
        MappedRegionPtr p1 = mappedFile1.get(0, 16, MappedFile::READ_WRITE);
        char* buffer1 = p1->get();
        memcpy(buffer1, "0123456789ABCDEF", 16);
        p1->flush();

        MappedFile mappedFile2;
        mappedFile2.open(path.c_str(), MappedFile::READ_ONLY);
        MappedRegionPtr p2 = mappedFile2.get(0, 16, MappedFile::READ_ONLY);
        char* buffer2 = p2->get();
        ASSERT_TRUE(0 == memcmp(buffer2, "0123456789ABCDEF", 16));
    }
    catch(CwException& ) {
        FileIo::remove(path.c_str());
    }
    FileIo::remove(path.c_str());
}

TEST(UTILS,MappedRegionIoOverallped)
{
    FILELog::Level() = LL_INFO;

    string path = FileIo::getTempName("/tmp");
    try {
        FileIo file;
        file.open(path.c_str(), FileIo::WRITE);

        const char* buffer = "FFFFFFFFFFFFFFFF";
        file.write(buffer, 16);

        MappedFile mappedFile1;
        mappedFile1.open(path.c_str(), MappedFile::READ_WRITE);
        MappedRegionPtr p1 = mappedFile1.get(0, 16, MappedFile::READ_WRITE);
        char* buffer1 = p1->get();
        memcpy(buffer1, "0123456789ABCDEF", 16);
        p1->flush();

        MappedFile mappedFile2;
        mappedFile2.open(path.c_str(), MappedFile::READ_ONLY);
        MappedRegionPtr p2 = mappedFile2.get(0, 16, MappedFile::READ_ONLY);
        char* buffer2 = p2->get();
        ASSERT_TRUE(0 == memcmp(buffer2, "0123456789ABCDEF", 16));
    }
    catch(CwException& ) {
        FileIo::remove(path.c_str());
    }
    FileIo::remove(path.c_str());
}

