/**********************************************
   File:   thread.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "clockwork_errors.h"
#include "clockwork_base.h"
#include "utils/thread.h"
#include <pthread.h>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

namespace cdb {

static void* pthread_func(void* obj)
{
    Thread* thread = static_cast<Thread*>(obj);
    thread->run();
    return NULL;
}

/****************************************************
 *
 * Thread
 *
 ****************************************************/

void Thread::start()
{
    if (m_thread != NULL) {
        THROW("Thread already running", CWSE_ERROR_SYSTEM);
    }

    pthread_t* p = new pthread_t;

    m_keepGoing = true;
    int ret = pthread_create(p, NULL, pthread_func, this);
    if (ret != 0) {
        THROW("Failed to create thread", CWSE_ERROR_SYSTEM);
    }

    m_thread = p;
}

void Thread::stop()
{
    if (m_thread == NULL) return;

    m_keepGoing = false;

    pthread_t* p = static_cast<pthread_t*>(m_thread);
    pthread_t t = *p;
    delete p;

    void* ret;
    pthread_join(t, &ret);

    m_thread = NULL;
}

/****************************************************
 *
 * TimerThread
 *
 ****************************************************/

struct ThreadTimer
{
    static const int EPOLL_TIMEOUT = 100;
    int m_epoll;
    int m_pipe[2];
    int m_timerfd;

    void add(int fd) {
        epoll_event ev;
        memset(&ev, 0, sizeof(epoll_event));
        ev.events = EPOLLIN;
        ev.data.fd = fd;
        int ret = epoll_ctl(m_epoll, EPOLL_CTL_ADD, fd, &ev);
        if (ret != 0) {
            THROW("Failed to add fd to epoll", CWSE_ERROR_SYSTEM);
        }
    }
};

void TimerThread::start()
{
    if (m_timer != NULL) {
        THROW("Thread already running", CWSE_ERROR_SYSTEM);
    }

    ThreadTimer tt;
    memset(&tt, 0, sizeof(ThreadTimer));

    int ret = ::pipe(tt.m_pipe);
    if (ret != 0) {
        THROW("Failed to create pipe", CWSE_ERROR_SYSTEM);
    }
    
    tt.m_epoll = ::epoll_create(2);
    if (tt.m_epoll == -1) {
        THROW("Failed to create epoll", CWSE_ERROR_SYSTEM);
    }

    tt.m_timerfd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
    if (tt.m_timerfd < 0) {
        THROW("Failed to create timer fd", CWSE_ERROR_SYSTEM);
    }

    itimerspec its;
    its.it_value.tv_sec = 0;
    its.it_value.tv_nsec = m_ms * 1000000;
    its.it_interval.tv_sec = 0;
    its.it_interval.tv_nsec = m_ms * 1000000;
    ret = timerfd_settime(tt.m_timerfd, 0, &its, NULL);
    if (ret < 0) {
        THROW("Failed to set timer", CWSE_ERROR_SYSTEM);
    }

    tt.add(tt.m_pipe[0]);
    tt.add(tt.m_timerfd);

    m_timer = new ThreadTimer(tt);

    Thread::start();
}

void TimerThread::stop()
{
    if (m_timer == NULL) return;

    ThreadTimer* p = static_cast<ThreadTimer*>(m_timer);
    uint64_t dummy = 0;
    if (0 == ::write(p->m_pipe[1], &dummy, sizeof(uint64_t))) {} // fixing compiler warning

    Thread::stop();

    ::close(p->m_epoll);
    ::close(p->m_pipe[0]);
    ::close(p->m_pipe[1]);
    ::close(p->m_timerfd);
    delete p;

    m_timer = NULL;
}

void TimerThread::run()
{
    ThreadTimer* p = static_cast<ThreadTimer*>(m_timer);

    while (m_keepGoing) {
        epoll_event evs[2];
        int ret = epoll_wait(p->m_epoll, evs, 2, ThreadTimer::EPOLL_TIMEOUT);
        if (ret < 0) {
            if (errno == EINTR) continue;
            break; //  must notify somehow
        }

        for (int i=0; i < ret; i++) {
            uint64_t dummy;
            if (0 == ::read(evs[i].data.fd, &dummy, sizeof(uint64_t))) {} // fixing compiler warning
            
            if (evs[i].data.fd == p->m_pipe[0]) break;
            
            onTimer(m_ptr);
        }
    }
}


} // namespace cdb
