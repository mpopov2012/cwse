/**********************************************
   File:   file_io_windows.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "clockwork_errors.h"
#include "utils/file_io.h"
#include "utils/log.h"
#include "utils/verify.h"
#include <boost/lexical_cast.hpp>
#include <stdio.h>

using std:: string;

namespace cdb {

FileIo::FileIo() 
  : m_file(INVALID_HANDLE_VALUE) 
{

}

size_t FileIo::getSize(const char* path)
{
    CHECK_ARG_NON_ZERO(path);
	
    WIN32_FILE_ATTRIBUTE_DATA fad;
	if (!GetFileAttributesExA(path, GetFileExInfoStandard, &fad)) {
        THROW("Failed to get file attributes", CWSE_ERROR_FAILED_FILE_OPERATION);
    }
	
    LARGE_INTEGER size;
	size.HighPart = fad.nFileSizeHigh;
	size.LowPart = fad.nFileSizeLow;
	
    return (size_t) size.QuadPart;
}

bool FileIo::exists(const char* path)
{
    CHECK_ARG_NON_ZERO(path);
	DWORD dwAttrib = GetFileAttributesA(path);
	return (dwAttrib != INVALID_FILE_ATTRIBUTES);
}

void FileIo::remove(const char* path)
{
    CHECK_ARG_NON_ZERO(path);
	if (!DeleteFileA(path)) {
        THROW("Failed to delete file", CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

void FileIo::rename(const char* old, const char* path)
{
    CHECK_ARG_NON_ZERO(old);
    CHECK_ARG_NON_ZERO(path);
    if (!MoveFileA(old, path)) {
        THROW("Failed to rename file", CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

void FileIo::mkdir(const char* path)
{
    CHECK_ARG_NON_ZERO(path);
	BOOL ret = CreateDirectoryA(path, NULL);
	if (!ret) {
        THROW("Failed to create directory", CWSE_ERROR_FAILED_FILE_OPERATION);
	}
}

void FileIo::rmdir(const char* path)
{
    CHECK_ARG_NON_ZERO(path);
	char dir[MAX_PATH];

	strncpy_s(dir, path, sizeof(dir));
	strncat_s(dir, "\\*", sizeof(dir));

	WIN32_FIND_DATAA FindFileData;
	HANDLE hFind = FindFirstFileA(dir, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) return;

	try {
		while (true) {
			if (!FindNextFileA(hFind, &FindFileData)) {
				if (GetLastError() == ERROR_NO_MORE_FILES) break;
                THROW("Failed to find a file", CWSE_ERROR_FAILED_FILE_OPERATION);
			}

			if (FindFileData.cFileName[0] == '.' && FindFileData.cFileName[1] == '\0') continue;
			if (FindFileData.cFileName[0] == '.' && FindFileData.cFileName[1] == '.' && FindFileData.cFileName[2] == '\0') continue;

			char file[MAX_PATH];
			strncpy_s(file, path, sizeof(file));
			strncat_s(file, "\\", sizeof(file));
			strncat_s(file, FindFileData.cFileName, sizeof(file));

			if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
				rmdir(file);
				if (!RemoveDirectoryA(file)) {
                    THROW("Failed to delete directory", CWSE_ERROR_FAILED_FILE_OPERATION);
                }
			}
			else {
				if (!DeleteFileA(file)) {
                    THROW("Failed to delete file", CWSE_ERROR_FAILED_FILE_OPERATION);
                }
			}
		}
	}
	catch (...) {
		FindClose(hFind);  // closing file handle
		throw;
	}
	FindClose(hFind);  // closing file handle

	if (!RemoveDirectoryA(path)) {
        THROW("Failed to delete directory", CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

void FileIo::open(const char* path, Access access)
{
    CHECK_ARG_NON_ZERO(path);

    if (m_file != INVALID_HANDLE_VALUE) {
        THROW("FileIo object already initialized", CWSE_ERROR_FAILED_FILE_OPERATION);
    }

    DWORD dwCreationDisposition = OPEN_EXISTING;
    DWORD dwDesiredAccess  = FILE_READ_DATA;
    if (access == WRITE) {
        dwDesiredAccess  = FILE_WRITE_DATA;
        dwCreationDisposition = OPEN_ALWAYS;
    }
    else if (access == APPEND) {
        dwCreationDisposition = OPEN_ALWAYS;
        dwDesiredAccess  = FILE_WRITE_DATA | FILE_APPEND_DATA;
    }

    HANDLE h = CreateFileA(path, dwDesiredAccess , FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, dwCreationDisposition, FILE_ATTRIBUTE_NORMAL, NULL);
    if (h == INVALID_HANDLE_VALUE) {
        string errMsg = string("Failed to open file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }

    if (access == APPEND) {
        DWORD p = SetFilePointer(h, 0, NULL, FILE_END);
        if (p == INVALID_SET_FILE_POINTER) {
            CloseHandle(h);
            string errMsg = string("Failed to open file for appending - ") + path;
            LOG_ERROR << errMsg;
            THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
        }
    }

    m_file = h;
    m_path = path;
}

void FileIo::close()
{
    if (m_file != INVALID_HANDLE_VALUE) {
        CloseHandle(static_cast<HANDLE>(m_file));
        m_file = INVALID_HANDLE_VALUE;
        m_path = "";
    }
}

void FileIo::write(const void* buffer, size_t size)
{
    CHECK_ARG_NON_ZERO(buffer);
    CHECK_ARG_POSITIVE(size);
    CHECK_DATA_MEMBER_INITIALIZED_VAL(m_file,INVALID_HANDLE_VALUE);

    DWORD processed;
    BOOL success = WriteFile(static_cast<HANDLE>(m_file), buffer, size, &processed, NULL);
    if (!success || (processed != size)) {
        string errMsg = string("Failed to write file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

void FileIo::read(void* buffer, size_t size)
{
    CHECK_ARG_NON_ZERO(buffer);
    CHECK_ARG_POSITIVE(size);
    CHECK_DATA_MEMBER_INITIALIZED_VAL(m_file,INVALID_HANDLE_VALUE);

    DWORD processed;
    BOOL success = ReadFile(static_cast<HANDLE>(m_file), buffer, size, &processed, NULL);
    if (!success || (processed != size)) {
        string errMsg = string("Failed to read file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

void FileIo::seek(size_t pos)
{
    CHECK_DATA_MEMBER_INITIALIZED_VAL(m_file,INVALID_HANDLE_VALUE);

    DWORD p = SetFilePointer(static_cast<HANDLE>(m_file), pos, NULL, FILE_BEGIN);
    if (p == INVALID_SET_FILE_POINTER) {
        string errMsg = string("Failed to seek file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

size_t FileIo::tell()
{
    CHECK_DATA_MEMBER_INITIALIZED_VAL(m_file,INVALID_HANDLE_VALUE);

    DWORD p = SetFilePointer(static_cast<HANDLE>(m_file), 0, NULL, FILE_CURRENT);
    if (p == INVALID_SET_FILE_POINTER) {
        string errMsg = string("Failed to seek file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
    return p;
}

void FileIo::flush()
{
    CHECK_DATA_MEMBER_INITIALIZED_VAL(m_file,INVALID_HANDLE_VALUE);
    // Nothing to do here
}

void FileIo::sync()
{
    CHECK_DATA_MEMBER_INITIALIZED_VAL(m_file,INVALID_HANDLE_VALUE);
    FlushFileBuffers(static_cast<HANDLE>(m_file));
}

void FileIo::truncate(size_t size)
{
    CHECK_DATA_MEMBER_INITIALIZED_VAL(m_file,INVALID_HANDLE_VALUE);

    seek(size);
    BOOL success = SetEndOfFile(static_cast<HANDLE>(m_file));
    if (!success) {
        string errMsg = string("Failed t!successte file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

string FileIo::getTempName(const char* path)
{
    CHECK_ARG_NON_ZERO(path);
    return string(path) + "/" + boost::lexical_cast<string>(GetTickCount());
}

} // namespace cdb
