/**********************************************
   File:   regex_processor.cpp

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifdef WIN32
// Turning off warning generated for xpressive
#pragma warning( disable : 4996 )
#endif

#include "clockwork_errors.h"
#include "clockwork_base.h"
#include "utils/regex_processor.h"
#include <stdlib.h>
#include <iostream>

#ifdef WIN32
#include <boost/xpressive/xpressive.hpp>
using namespace boost::xpressive;
#else
#include <regex.h>
#include <sys/types.h>
#include <string.h>
#endif

using namespace std;

namespace cdb {

static 
std::string convertPattern(const std::string& pattern)
{
    const string symbol = "[a-zA-Z0-9_\\.]";
    const string openParen = "(";
    const string closeParen= ")";

    string regExPattern = "^";
    for (size_t i=0; i < pattern.length(); i++) {
        if (pattern[i] == '?') regExPattern += openParen + symbol + closeParen;
        else if (pattern[i] == '*') regExPattern += openParen + symbol + "*" + closeParen;
        else if (pattern[i] == '.') regExPattern += "\\.";
        else regExPattern.push_back(pattern[i]);
    }
    regExPattern.push_back('$');
    return regExPattern;
}


#ifdef WIN32
//=============================================================================

void Regex::release() 
{ 
    if (m_sregex != NULL) {
        sregex* rx = static_cast<sregex*>(m_sregex);
        delete rx;
    }
    m_sregex = NULL;
}

void Regex::compile(const std::string& expression, RegexMode mode)
{
    release();

    string pattern = (mode == WILD_CHARS) ? convertPattern(expression) : expression;
    bool caught = false;
	try {
		m_sregex = new sregex(sregex::compile(pattern));
	}
	catch(...) {
        caught = true;
	}
    if (caught) {
        THROW(string("Failed to compile regex pattern \"")+pattern+"\"", CWSE_ERROR_INVALID_ARG);
    }
}

bool Regex::match(const std::string& str)
{
    if (m_sregex == NULL) {
        THROW("RegexProcessor is not initialized", CWSE_ERROR_OBJECT_NOT_INITIALIZED);
    }

    smatch what;
    sregex* rx = static_cast<sregex*>(m_sregex);
    return regex_match(str, what, *rx);
}

#else
//=============================================================================

void Regex::release() 
{ 
    if (m_sregex != NULL) {
        regex_t* rx = static_cast<regex_t*>(m_sregex);
        regfree(rx);
        free(rx);
    }
    m_sregex = NULL;
}

void Regex::compile(const std::string& expression, RegexMode mode)
{
    release();

    string pattern = (mode == WILD_CHARS) ? convertPattern(expression) : expression;

    m_sregex = malloc(sizeof(regex_t));
    memset(m_sregex, 0, sizeof(regex_t));

    regex_t* rx = static_cast<regex_t*>(m_sregex);
    int ret = regcomp(rx, pattern.c_str(), REG_EXTENDED);
    if (ret != 0) {
        THROW(string("Failed to compile regex pattern \"")+pattern+"\"", CWSE_ERROR_INVALID_ARG);
    }
}

bool Regex::match(const std::string& str)
{
    if (m_sregex == NULL) {
        THROW("RegexProcessor is not initialized", CWSE_ERROR_OBJECT_NOT_INITIALIZED);
    }

    regmatch_t m;
    regex_t* rx = static_cast<regex_t*>(m_sregex);
    int ret = regexec(rx, str.c_str(), 1, &m, 0);
    if (ret != 0) return false;
    return (m.rm_so == 0);
}

#endif

} // namespace cdb

