/**********************************************
   File:   file_io.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "clockwork_errors.h"
#include "utils/file_io.h"
#include "utils/log.h"
#include "utils/verify.h"
#include <boost/lexical_cast.hpp>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <ftw.h>

using std::string;

namespace cdb {

FileIo::FileIo()
  : m_file(NULL) 
{

}

size_t FileIo::getSize(const char* path)
{
    CHECK_ARG_NON_ZERO(path);
    struct stat st;
    int ret = ::stat(path, &st);
    if (ret < 0) {
        THROW("File doesn't exist", CWSE_ERROR_FAILED_FILE_OPERATION);
    }
    return st.st_size;
}

bool FileIo::exists(const char* path)
{
    CHECK_ARG_NON_ZERO(path);
    struct stat st;
    int ret = ::stat(path, &st);
    return (ret == 0);
}

void FileIo::remove(const char* path)
{
    CHECK_ARG_NON_ZERO(path);
    int ret = ::unlink(path);
    if (ret != 0) {
        string errMsg = string("Failed to remove file ") + path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

void FileIo::rename(const char* old, const char* path)
{
    CHECK_ARG_NON_ZERO(old);
    CHECK_ARG_NON_ZERO(path);
    int ret = ::rename(old, path);
    if (ret != 0) {
        string errMsg = string("Failed to rename file ") + old + " to " + path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

void FileIo::mkdir(const char* path)
{
    CHECK_ARG_NON_ZERO(path);
    int ret = ::mkdir(path, 0770);
    if (ret != 0) {
        string errMsg = string("Failed to create directory  ") + path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

static int unlink_cb(const char *fpath, const struct stat* /*sb*/, int /*typeflag*/, struct FTW* /*ftwbuf*/)
{
    return ::remove(fpath);
}

void FileIo::rmdir(const char* path)
{
    CHECK_ARG_NON_ZERO(path);
    int ret = ::nftw(path, unlink_cb, 64, FTW_DEPTH | FTW_PHYS);
    if (ret != 0) {
        string errMsg = string("Failed to remove directory  ") + path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

void FileIo::open(const char* path, Access access)
{
    CHECK_ARG_NON_ZERO(path);

    if (m_file != NULL) {
        THROW("FileIo object already initialized", CWSE_ERROR_FAILED_FILE_OPERATION);
    }

    const char *accessStr = (access == READ) ? "rb" :
                            (access == APPEND) ? "ab" :
                            "wb";
    FILE* f = fopen(path, accessStr);
    if (f == NULL) {
        string errMsg = string("Failed to open file ") + path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }

    setvbuf (f, NULL, _IONBF, 0);

    m_file = f;
    m_path = path;
}

void FileIo::close()
{
    if (m_file != NULL) {
        fclose(static_cast<FILE*>(m_file));
        m_file = NULL;
        m_path = "";
    }
}

void FileIo::write(const void* buffer, size_t size)
{
    CHECK_ARG_NON_ZERO(buffer);
    CHECK_ARG_POSITIVE(size);
    CHECK_DATA_MEMBER_INITIALIZED(m_file);

    size_t ret = fwrite(buffer, 1, size, static_cast<FILE*>(m_file));
    if (ret != size) {
        string errMsg = string("Failed to write file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

void FileIo::read(void* buffer, size_t size)
{
    CHECK_ARG_NON_ZERO(buffer);
    CHECK_ARG_POSITIVE(size);
    CHECK_DATA_MEMBER_INITIALIZED(m_file);

    ssize_t ret = fread(buffer, 1, size, static_cast<FILE*>(m_file));
    if (ret != (ssize_t)size) {
        string errMsg = string("Failed to read file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

void FileIo::seek(size_t pos)
{
    CHECK_DATA_MEMBER_INITIALIZED(m_file);

    int ret = fseek (static_cast<FILE*>(m_file), pos, SEEK_SET);
    if (ret != 0) {
        string errMsg = string("Failed to seek file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

size_t FileIo::tell()
{
    CHECK_DATA_MEMBER_INITIALIZED(m_file);

    return ftell(static_cast<FILE*>(m_file));
}

void FileIo::flush()
{
    CHECK_DATA_MEMBER_INITIALIZED(m_file);

    int ret = fflush(static_cast<FILE*>(m_file));
    if (ret != 0) {
        string errMsg = string("Failed to flush file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

void FileIo::sync()
{
    CHECK_DATA_MEMBER_INITIALIZED(m_file);

    flush();

    FILE* f = static_cast<FILE*>(m_file);

    int ret = fsync(fileno(f));
    if (ret != 0) {
        string errMsg = string("Failed to sync file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

void FileIo::truncate(size_t size)
{
    CHECK_DATA_MEMBER_INITIALIZED(m_file);

    flush();
    FILE* f = static_cast<FILE*>(m_file);

    int ret;
    ret = ftruncate(fileno(f), size);
    if (ret != 0) {
        string errMsg = string("Failed to truncate file ") + m_path;
        LOG_ERROR << errMsg;
        THROW(errMsg, CWSE_ERROR_FAILED_FILE_OPERATION);
    }
}

string FileIo::getTempName(const char* path)
{
    struct timeval tv;  // TODO: make it portable
    gettimeofday(&tv,0);
    string name = boost::lexical_cast<string>(tv.tv_sec) + "." + boost::lexical_cast<string>(tv.tv_usec);
    return string(path) + "/" + name;
}

} // namespace cdb
