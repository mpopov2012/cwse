/**********************************************
   File:   names_impl.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DATASTOR_NAMES_H
#define CDB_DATASTOR_NAMES_H

#include "clockwork_names.h"
#include "clockwork_base.h"
#include <string>
#include <vector>
#include <algorithm>

namespace cdb {

class NamesImpl : public Names
{
private:
    NamesImpl(const NamesImpl&);
    NamesImpl& operator=(const NamesImpl&);

public:
    NamesImpl() : m_index(0) {}
    virtual ~NamesImpl() {}
    
    virtual bool next(std::string& name) {
        if (m_index >= m_items.size()) return false;
        name = m_items[m_index];
        m_index++;
        return true;
    }

    void reserve(size_t count) { m_items.reserve(count); }
    
    void add(const std::string& name) {
        m_items.push_back(name);
    }

    void sort() { std::sort(m_items.begin(), m_items.end()); }

private:
    typedef std::vector<std::string> Items;

    Items m_items;
    size_t m_index;
};

} // namespace cdb

#endif // CDB_DATASTOR_NAMES_H

