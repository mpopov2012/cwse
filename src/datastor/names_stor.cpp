/**********************************************
   File:   names_stor.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "clockwork_errors.h"
#include "clockwork_base.h"
#include "datastor/names_stor.h"
#include "utils/file_io.h"
#include <string>

using std::string;

namespace cdb {

string NamesStor::fullPath(const char* path)
{
    return string(path) + "/" + CWSE_NAMES_FILE_NAME;
}

void NamesStor::add(const char* path, uint16_t id, const std::string& name)
{
    if (name.size() > CWSE_MAX_NAME_LENGTH) {
        THROW("Metric name too long", CWSE_ERROR_INVALID_ARG);
    }

    FileIo storage;
    storage.open(fullPath(path).c_str(), FileIo::APPEND);
    uint16_t length = static_cast<uint16_t>(name.size());
    storage.write(&id, sizeof(id)); // 2 bytes
    storage.write(&length, sizeof(length)); // 2bytes
    storage.write(name.c_str(), length);
}

void NamesStor::load(const char* path, ConcurrentHashTable& container)
{
    uint16_t length;
    uint16_t id;
    char buffer[CWSE_MAX_NAME_LENGTH+1];
    size_t pos = 0;

    string filePath = fullPath(path);

    size_t fileSize = FileIo::getSize(filePath.c_str());

    FileIo storage;
    storage.open(filePath.c_str(), FileIo::READ);

    while (pos < fileSize) {
        storage.read(&id, sizeof(id));
        storage.read(&length, sizeof(length));
        pos += sizeof(id) + sizeof(length);

        storage.read(buffer, length);
        pos += length;
        buffer[length] = '\0';
        container.put(buffer, id);
    }
}

}
