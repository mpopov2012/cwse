/**********************************************
   File:   dataset_impl.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License")
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "datastor/dataset_impl.h"
#include "datastor/names_impl.h"
#include "datastor/datastor_base.h"
#include "datastor/names_stor.h"
#include "utils/verify.h"
#include "utils/timer.h"
#include "utils/atomic.h"
#include "utils/log.h"
#include "utils/file_io.h"
#include "utils/mapped_region.h"
#include "utils/regex_processor.h"
#include "clockwork_errors.h"
#include "clockwork_base.h"
#include <boost/lexical_cast.hpp>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>

using std::string;
using std::vector;
using std::ofstream;
using std::ifstream;
using std::endl;

#define NAME_PATTERN "^[a-zA-Z_][a-zA-Z0-9_\\.]*$"
#define CHECK_ACCESS_PERMISSIONS \
    if (m_mappedFileAccess != MappedFile::READ_WRITE) { \
        THROW("Operation prohibited. Dataset is opened with READ_ONLY access", CWSE_ERROR_INVALID_ACCESS_PERMISSIONS); \
    }


namespace cdb {

/***********************************************
 * Dataset
 ***********************************************/
// static
void Dataset::setLogging(int level)
{
    switch(level) {
        case CWSE_LOG_LEVEL_MUTE: FILELog::Level() = LL_CRITICAL; break;
        case CWSE_LOG_LEVEL_ERROR: FILELog::Level() = LL_ERROR; break;
        case CWSE_LOG_LEVEL_WARN: FILELog::Level() = LL_WARN; break;
        case CWSE_LOG_LEVEL_TRACE: FILELog::Level() = LL_TRACE; break;
        default: FILELog::Level() = LL_CRITICAL; break;
    }
}

// static
CwseDatasetConfig Dataset::getDefaultDatasetConfig()
{
	CwseDatasetConfig cfg;
	cfg.metricsMaxCount = 256;
	return cfg;
}

// static
CwseColumnConfig Dataset::getDefaultColumnConfig()
{
	CwseColumnConfig cfg;
    cfg.type = CWSE_TYPE_DOUBLE;
	return cfg;
}

// static
void  Dataset::releaseNamesIterator(void* handle)
{
    CHECK_ARG_NON_ZERO(handle);
}

// static
void Dataset::create(const char* path, const CwseDatasetConfig* cfg)
{
    LOG_TRACE << "Create dataset at " << path;

	CHECK_ARG_NON_ZERO(path);
	CHECK_ARG_NON_ZERO(cfg);

    if (cfg->metricsMaxCount < 1 || cfg->metricsMaxCount > CWSE_COLUMN_COUNT_LIMIT) {
        string msg = "Invalid configuration value metricsMaxCount";
        LOG_TRACE << msg << " " << cfg->metricsMaxCount;
        THROW(msg, CWSE_ERROR_INVALID_ARG);
    }

    if (FileIo::exists(path)) {
        string msg = string("Dataset directory already exists: ") + path;
        LOG_TRACE << msg;
        THROW(msg, CWSE_ERROR_INVALID_ARG);
    }

    MainFileMarker marker;
    marker.m_magicNumber = CWSE_MAGIC_NUMBER;
    marker.m_version = CWSE_CURRENT_VERSION;
    marker.m_sizeOfHeader = sizeof(MainFileHeader);

    MainFileHeader header;
    memset(&header, 0, sizeof(MainFileHeader));
    header.m_columnsReserved = cfg->metricsMaxCount;
    header.m_sizeOfMetricRecord = sizeof(ColumnRecord);

    size_t metricsSize = static_cast<size_t>(header.m_columnsReserved) * sizeof(ColumnRecord);
    size_t size = sizeof(MainFileMarker) + 
                  sizeof(MainFileHeader) + 
                  metricsSize;

    FileIo::mkdir(path);

    string fullPath = string(path) + "/" + CWSE_MAIN_FILE_NAME;
    MappedFile::allocate(fullPath.c_str(), size);

    MappedFile mappedFile;
    mappedFile.open(fullPath.c_str(), MappedFile::READ_WRITE);

    MappedRegionPtr region = mappedFile.get(0, size, MappedFile::READ_WRITE);
    char* ptr = region->get();

    memset(ptr, 0, size);
    memcpy(ptr, &marker, sizeof(marker)); ptr += sizeof(marker);
    memcpy(ptr, &header, sizeof(header)); ptr += sizeof(header);
}

// static
Dataset* Dataset::open(const char* path, int dataAccess)
{
	CHECK_ARG_NON_ZERO(path);

	DatasetImpl* dataset = new DatasetImpl;
    try {
        dataset->init(path, dataAccess);
    }
    catch(...) {
        delete dataset;
        throw;
    }

    return dataset;
}

/**********************************************
 * DatasetImpl
 ***********************************************/
DatasetImpl::~DatasetImpl()
{
}

// Dataset configuration
CwseDatasetConfig DatasetImpl::getDatasetConfig()
{
    CHECK_DATA_MEMBER_INITIALIZED(m_header);

	CwseDatasetConfig result;
    result.metricsMaxCount = static_cast<uint64_t>(m_header->m_columnsReserved);
	return result;
}

void DatasetImpl::alterDataset(const CwseDatasetConfig* cfg)
{
	CHECK_ARG_NON_ZERO(cfg);
    CHECK_DATA_MEMBER_INITIALIZED(m_header);
    CHECK_ACCESS_PERMISSIONS

    if (cfg->metricsMaxCount <= m_header->m_columnsReserved) {
        THROW("Invalid alter configuration: new metrics count must be more than existing metrics count", CWSE_ERROR_INVALID_ARG);
    }

    string fullPath = m_path + "/" + CWSE_MAIN_FILE_NAME;
    size_t diffSize = static_cast<size_t>(cfg->metricsMaxCount - m_header->m_columnsReserved) * sizeof(ColumnRecord);
    size_t currentFileSize = FileIo::getSize(fullPath.c_str());
    size_t newSize = currentFileSize + diffSize;
    MappedFile::allocate(fullPath.c_str(), newSize);

    size_t metricsSize = static_cast<size_t>(cfg->metricsMaxCount) * sizeof(ColumnRecord);

    m_regionMetrics.reset();
    size_t offset = sizeof(MainFileMarker) + sizeof(MainFileHeader);
    m_regionMetrics = m_mappedFile.get(offset, metricsSize, m_mappedFileAccess);
    m_columns = (ColumnRecord*)m_regionMetrics->get();

    size_t newMetricsOffset = static_cast<size_t>(m_header->m_columnsReserved) * sizeof(ColumnRecord);
    memset(m_columns+newMetricsOffset, 0, diffSize);

    m_header->m_columnsReserved = cfg->metricsMaxCount;
}


// Metrics management
void DatasetImpl::addColumn(const char* name, const CwseColumnConfig* cfg, bool ignoreIfExist)
{
    CHECK_ARG_NON_ZERO(name);
    CHECK_DATA_MEMBER_INITIALIZED(m_header);
    CHECK_ACCESS_PERMISSIONS

	if (m_header->m_columnsCount >= m_header->m_columnsReserved) {
        THROW("No more metrics empty slot", CWSE_ERROR_LOGICAL_ERROR);
	}

    if (strlen(name) > CWSE_MAX_NAME_LENGTH) {
        THROW("Metric name is too long", CWSE_ERROR_INVALID_ARG);
    }

    Regex rx;
    rx.compile(NAME_PATTERN, Regex::STRAIGHT);
	bool matched = rx.match(name);
	if (!matched) {
        THROW("Metric name is invalid", CWSE_ERROR_INVALID_ARG);
	}

    if (m_schemaIdCheck != m_header->m_schemaID) {
        NamesStor::load(m_path.c_str(), m_names);
        m_schemaIdCheck = m_header->m_schemaID;
    }

    uint16_t id;
    bool b = m_names.get(name, &id);
    if (b) {
        if (!ignoreIfExist) {
            THROW("Metric name already exists", CWSE_ERROR_INVALID_ARG);
        }
        else {
            if (m_columns[id].m_type != cfg->type) {
                THROW("Metric with different type already exists", CWSE_ERROR_INVALID_ARG);
            }
        }
	}

    uint16_t result = (uint16_t) m_header->m_columnsCount;

    NamesStor::add(m_path.c_str(), result, name);
    m_names.put(name, result);

    m_columns[result].m_type = cfg->type;

    m_header->m_columnsCount++;
    m_header->m_schemaID++;

    flush();
}

uint16_t DatasetImpl::findColumn(const char* name)
{
	CHECK_ARG_NON_ZERO(name);
    CHECK_DATA_MEMBER_INITIALIZED(m_header);

    if (m_schemaIdCheck != m_header->m_schemaID) {
        NamesStor::load(m_path.c_str(), m_names);
        m_schemaIdCheck = m_header->m_schemaID;
    }

    uint16_t id;
    bool b = m_names.get(name, &id);
    if (!b) {
        id = CWSE_INVALID_COLUMN_ID;
    }

    return id;
}

CwseColumnConfig DatasetImpl::getColumnConfig(const char* name)
{
    CHECK_ARG_NON_ZERO(name);

    uint16_t id = findColumn(name);

    CwseColumnConfig result;
    result.type = m_columns[id].m_type;
    return result;
}


// Data management: current values
void DatasetImpl::set(double value, uint16_t id, uint64_t* ts)
{
    CHECK_DATA_MEMBER_INITIALIZED(m_header);
    CHECK_ACCESS_PERMISSIONS

    if (id >= m_header->m_columnsReserved) {
        THROW("Invalid metric id", CWSE_ERROR_LOGICAL_ERROR);
    }

    int64_t temp;
    switch (m_columns[id].m_type) {
        case ColumnType::DOUBLE:
            m_columns[id].m_value = value;
            break;

        case ColumnType::INT64:
            temp = (int64_t) value;
            memcpy(&m_columns[id].m_value, &temp, 8);
            break;

        default:
            THROW("Invalid metric id", CWSE_ERROR_LOGICAL_ERROR);
    }

    m_columns[id].m_timestamp = (ts != NULL) ? *ts : Timer::getCurrentTimestampMs();
}

void DatasetImpl::inc(double value, uint16_t id, uint64_t* ts)
{
    CHECK_DATA_MEMBER_INITIALIZED(m_header);
    CHECK_ACCESS_PERMISSIONS

    if (id >= m_header->m_columnsReserved) {
        THROW("Invalid metric id", CWSE_ERROR_LOGICAL_ERROR);
    }

    int64_t* base;
    switch (m_columns[id].m_type) {
        case ColumnType::DOUBLE:
          Atomic::incd(&m_columns[id].m_value, value);
          break;

        case ColumnType::INT64:
            base = (int64_t*) &m_columns[id].m_value;
            Atomic::inc64(base, (int64_t) value);
            break;

        default:
            THROW("Invalid metric id", CWSE_ERROR_LOGICAL_ERROR);
    }

    m_columns[id].m_timestamp = (ts != NULL) ? *ts : Timer::getCurrentTimestampMs();
}

double DatasetImpl::get(uint16_t id, uint64_t* ts) const
{
    CHECK_DATA_MEMBER_INITIALIZED(m_header);

    if (id >= m_header->m_columnsReserved) {
        THROW("Invalid metric id", CWSE_ERROR_LOGICAL_ERROR);
    }

    int64_t* base;
    double result = 0.0;
    switch (m_columns[id].m_type) {
        case ColumnType::DOUBLE:
            result = m_columns[id].m_value;
            break;

        case ColumnType::INT64:
            base = (int64_t*) &m_columns[id].m_value;
            result = (double) (*base);
            break;

        default:
            THROW("Invalid metric id", CWSE_ERROR_LOGICAL_ERROR);
    }

    if (ts != NULL) *ts = m_columns[id].m_timestamp;

    return result;
}

void DatasetImpl::flush()
{
    CHECK_DATA_MEMBER_INITIALIZED(m_header);

    m_regionHeader->flush();
    m_regionMetrics->flush();
}

// Names iterator
Names* DatasetImpl::getNames(const char* pattern)
{
    CHECK_DATA_MEMBER_INITIALIZED(m_header);

    if (m_schemaIdCheck != m_header->m_schemaID) {
        NamesStor::load(m_path.c_str(), m_names);
        m_schemaIdCheck = m_header->m_schemaID;
    }

    Regex rx;
    if (pattern != NULL) {
        rx.compile(pattern, Regex::WILD_CHARS);
    }

    ConcurrentHashTable::Cursor cursor = m_names.getCursor();

    NamesImpl* names = new NamesImpl;
    string str;
    while (cursor.next(str)) {
        if (pattern != NULL) {
            if (rx.match(str)) names->add(str);
        }
        else {
            names->add(str);
        }
    }

    names->sort();

    return names;
}

/**************************************************
 * Interal functions
 **************************************************/

DatasetImpl::DatasetImpl()
  : m_mappedFileAccess(MappedFile::READ_ONLY),
    m_header(NULL),
    m_columns(NULL)
{

}

void DatasetImpl::init(const char* path, int dataAccess)
{
	CHECK_ARG_NON_ZERO(path);

    m_path = path;
    m_mappedFileAccess = (dataAccess == CWSE_DATA_ACCESS_READ_ONLY) ? MappedFile::READ_ONLY : MappedFile::READ_WRITE;

    LOG_TRACE << "Open dataset at " << m_path;

    if (!FileIo::exists(path)) {
        string msg = string("Dataset directory doesn't exist: ") + m_path;
        LOG_TRACE << msg;
        THROW(msg, CWSE_ERROR_INVALID_ARG);
    }

    if (m_mappedFileAccess == MappedFile::READ_WRITE) {
        string lockPath = string(m_path) + "/" + CWSE_LOCK_FILE_NAME;
        if (!m_fileLock.lock(lockPath.c_str())) {
            string msg = string("Failed to lock for writing: ") + m_path;
            LOG_TRACE << msg;
            THROW(msg, CWSE_ERROR_INVALID_ACCESS_PERMISSIONS);
        }
    }

    string fullPath = string(m_path) + "/" + CWSE_MAIN_FILE_NAME;
    m_mappedFile.open(fullPath.c_str(), m_mappedFileAccess);

    size_t pos = 0;

    MappedRegionPtr markerRegion = m_mappedFile.get(pos, sizeof(MainFileMarker), MappedFile::READ_ONLY);
    MainFileMarker* markerPtr = (MainFileMarker*)markerRegion->get();

    if (markerPtr->m_magicNumber != CWSE_MAGIC_NUMBER || markerPtr->m_sizeOfHeader != sizeof(MainFileHeader)) {
        string msg = string("Dataset file corrupted");
        LOG_TRACE << msg;
        THROW(msg, CWSE_ERROR_LOGICAL_ERROR);
    }

    pos += sizeof(MainFileMarker);
    m_regionHeader = m_mappedFile.get(pos, sizeof(MainFileHeader), m_mappedFileAccess);
    m_header = (MainFileHeader*)m_regionHeader->get();

    if (m_header->m_sizeOfMetricRecord != sizeof(ColumnRecord)) {
        string msg = string("Dataset file corrupted");
        LOG_TRACE << msg;
        THROW(msg, CWSE_ERROR_LOGICAL_ERROR);
    }

    size_t metricsSize = static_cast<size_t>(m_header->m_columnsReserved) * sizeof(ColumnRecord);
    size_t size = sizeof(MainFileMarker) + 
                  sizeof(MainFileHeader) + 
                  metricsSize;

    size_t fileSize = FileIo::getSize(fullPath.c_str());
    if (fileSize != size) {
        string msg = string("Invalid file size at ") + m_path;
        LOG_TRACE << msg << "  fileSize=" << fileSize << "  expectedSize=" << size;
        THROW(msg, CWSE_ERROR_INVALID_ARG);
    }

    pos += sizeof(MainFileHeader);
    m_regionMetrics = m_mappedFile.get(pos, metricsSize, m_mappedFileAccess);
    m_columns = (ColumnRecord*)m_regionMetrics->get();

    m_schemaIdCheck = m_header->m_schemaID;

    if (m_header->m_columnsCount > 0) {
        NamesStor::load(m_path.c_str(), m_names);
    }
}

uint64_t DatasetImpl::getSchemaID() const
{
    CHECK_DATA_MEMBER_INITIALIZED(m_header);
    return m_header->m_schemaID;
}


} // namespace cdb
