/**********************************************
   File:   dataset_impl.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DATASTOR_DATASET_IMPL_H
#define CDB_DATASTOR_DATASET_IMPL_H

#include "clockwork.h"
#include "utils/mapped_region.h"
#include "utils/file_lock.h"
#include "utils/concurrent_hash_table.h"
#include "datastor/datastor_base.h"
#include <string>
#include <set>

namespace cdb {

class DatasetImpl : public Dataset
{
    friend class Dataset;

public:
    virtual ~DatasetImpl();

    // Dataset configuration
    virtual CwseDatasetConfig getDatasetConfig();
    virtual void alterDataset(const CwseDatasetConfig* cfg);

    // Columns management
    virtual void addColumn(const char* name, const CwseColumnConfig* cfg, bool ignoreIfExist);
    virtual CwseColumnConfig getColumnConfig(const char* name);
    virtual uint16_t findColumn(const char* name);

    // Data management: current values
    virtual void set(double value, uint16_t id, uint64_t* ts=NULL);
    virtual void inc(double value, uint16_t id, uint64_t* ts=NULL);
    virtual double get(uint16_t id, uint64_t* ts=NULL) const;

    // Query names
    virtual Names* getNames(const char* pattern);

public:
    DatasetImpl();
    void init(const char* path, int dataAccess);
    uint64_t getSchemaID() const;

private:
    std::string m_path;
    FileLock m_fileLock;
    MappedFile::MappedFileAccess m_mappedFileAccess;
    ConcurrentHashTable m_names;
    MappedFile m_mappedFile;
    MappedRegionPtr m_regionHeader;
    MainFileHeader* m_header;
    MappedRegionPtr m_regionMetrics;
    ColumnRecord*   m_columns;

    volatile uint64_t m_schemaIdCheck;

private:
    void flush();

};


} // namespace cdb

#endif // CDB_DATASTOR_DATASET_IMPL_H
