/**********************************************
   File:   utest_dataset_impl.cpp

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "datastor/dataset_impl.h"
#include "datastor/names_impl.h"
#include "utils/file_io.h"
#include "utils/log.h"
#include "clockwork_base.h"
#include "gtest/gtest.h"
#include <string>
#include <iostream>

using namespace std;
using namespace cdb;

TEST(DATASTOR,DatasetImpl)
{
    FILELog::Level() = LL_INFO;
    string path = FileIo::getTempName("/tmp");
    CwseDatasetConfig cfg = Dataset::getDefaultDatasetConfig();
    Dataset::create(path.c_str(), &cfg);
    Dataset* p = Dataset::open(path.c_str(), CWSE_DATA_ACCESS_READ_ONLY);
    delete p;
    FileIo::rmdir(path.c_str());
}

TEST(DATASTOR,Metrics)
{
    FILELog::Level() = LL_INFO;
    string path = FileIo::getTempName("/tmp");
    CwseDatasetConfig cfg = Dataset::getDefaultDatasetConfig();
    Dataset::create(path.c_str(), &cfg);
    Dataset* p = Dataset::open(path.c_str(), CWSE_DATA_ACCESS_READ_ONLY);
    delete p;
    FileIo::rmdir(path.c_str());
}

