/**********************************************
   File:   utest_names.cpp

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "datastor/dataset_impl.h"
#include "datastor/names_impl.h"
#include "datastor/names_stor.h"
#include "utils/file_io.h"
#include "utils/log.h"
#include "clockwork_base.h"
#include "gtest/gtest.h"
#include <string>
#include <iostream>

using namespace std;
using namespace cdb;

TEST(DATASTOR,NamesImpl)
{
    FILELog::Level() = LL_INFO;

    string path = FileIo::getTempName("/tmp");
    CwseDatasetConfig cfg = Dataset::getDefaultDatasetConfig();
    Dataset::create(path.c_str(), &cfg);
    Dataset* p = Dataset::open(path.c_str(), CWSE_DATA_ACCESS_READ_ONLY);

    Names* names = p->getNames(NULL);
    string name;
    ASSERT_FALSE(names->next(name));
    delete names;

    names = p->getNames(NULL);
    NamesImpl* nimpl = static_cast<NamesImpl*>(names);
    nimpl->reserve(5);
    nimpl->add("one");
    nimpl->add("two");
    nimpl->add("three");
    ASSERT_TRUE(names->next(name));
    ASSERT_EQ(name, string("one"));
    ASSERT_TRUE(names->next(name));
    ASSERT_EQ(name, string("two"));
    ASSERT_TRUE(names->next(name));
    ASSERT_EQ(name, string("three"));
    ASSERT_FALSE(names->next(name));
    delete names;

    delete p;
    FileIo::rmdir(path.c_str());
}

TEST(DATASTOR,NamesStor)
{
    FILELog::Level() = LL_INFO;
    string path = FileIo::getTempName("/tmp");

    FileIo::mkdir(path.c_str());

    NamesStor::add(path.c_str(), 1, "one");
    NamesStor::add(path.c_str(), 2, "two");
    NamesStor::add(path.c_str(), 3, "three");

    ConcurrentHashTable table;
    NamesStor::load(path.c_str(), table);

    uint16_t id;
    bool b = table.get("one", &id);
    ASSERT_TRUE(b);
    ASSERT_EQ(1, id);

    b = table.get("three", &id);
    ASSERT_TRUE(b);
    ASSERT_EQ(3, id);

    b = table.get("four", &id);
    ASSERT_FALSE(b);

    FileIo::rmdir(path.c_str());
}
