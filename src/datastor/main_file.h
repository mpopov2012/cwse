/**********************************************
   File:   main_file.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DATASTOR_MAIN_FILE_H
#define CDB_DATASTOR_MAIN_FILE_H

#include "datastor/datastor_base.h"
#include "clockwork_base.h"

namespace cdb {

class MainFile
{
private:
	MainFile(const MainFile&);
	MainFile& operator=(const MainFile&);

public:
	MainFile();
	~MainFile();

	void create(const char* path, DatasetConfig& cfg);
	void open(const char* path);
	DatasetConfig getDatasetConfig();
	void alter(DatasetConfig& cfg);

private:
	MainFileHeader* m_header;
    file_mapping    m_file;
    mapped_region   m_regionHeader;
    mapped_region   m_regionCachePageOne;
    mapped_region   m_regionCachePageTwo;
    mapped_region   m_regionMediumSlots;
    mapped_region   m_regionLongSlots;
    mapped_region   m_regionPresentValues;

};

}

#endif // CDB_DATASTOR_MAIN_FILE_H
