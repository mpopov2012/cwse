/**********************************************
   File:   datastor_base.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DATASTOR_DATASTOR_BASE_H
#define CDB_DATASTOR_DATASTOR_BASE_H

#include "clockwork_base.h"

namespace cdb {

#define CWSE_MAGIC_NUMBER        0xDEADBEEFBADCAFFE
#define CWSE_CURRENT_VERSION     1000001
#define CWSE_COLUMN_COUNT_LIMIT  16000

#define CWSE_MAIN_FILE_NAME      "clockwork.dat"
#define CWSE_NAMES_FILE_NAME     "names.dat"
#define CWSE_LOCK_FILE_NAME      "clockwork.lock"

struct ColumnType
{
    enum { UNKNOWN=CWSE_TYPE_UNKNOWN, DOUBLE=CWSE_TYPE_DOUBLE, INT64=CWSE_TYPE_INT64 };
};

struct MainFileMarker
{
	uint64_t m_magicNumber;
	uint64_t m_version;
	uint64_t m_sizeOfHeader;
};

struct MainFileHeader
{
    uint64_t m_sizeOfMetricRecord;
    uint64_t m_columnsReserved;
    uint64_t m_columnsCount;
    uint64_t m_lastTime;
    uint64_t m_schemaID;
};

struct ColumnRecord
{
    uint64_t m_timestamp;
    uint64_t m_type;
    double   m_value;
};


}

#endif // CDB_DATASTOR_DATASTOR_BASE_H
