/**********************************************
   File:   names_stor.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DATASTOR_NAMES_STOR_H
#define CDB_DATASTOR_NAMES_STOR_H

#include "clockwork_base.h"
#include "datastor/datastor_base.h"
#include "utils/concurrent_hash_table.h"
#include <string>

namespace cdb {

class NamesStor
{
public:
    static void add(const char* path, uint16_t id, const std::string& name);
    static void load(const char* path, ConcurrentHashTable& container);

private:
    static std::string fullPath(const char* path);
};

}

#endif // CDB_DATASTOR_NAMES_STOR_H
