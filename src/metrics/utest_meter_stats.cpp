/**********************************************
   File:   utest_meter_stats.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License")
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "metrics_plus_plus.h"
#include "clockwork.h"
#include "metrics/metrics.h"
#include "utils/file_io.h"
#include "utils/log.h"
#include "gtest/gtest.h"
#include <boost/thread.hpp>
#include <boost/thread/barrier.hpp>
#include <boost/bind.hpp>
#include <string>
#include <iostream>
#include <stdexcept>

using namespace std;
using namespace cdb;
using namespace metrics_plus_plus;

class METER_STATS_Fixture: public ::testing::Test
{
public:
    METER_STATS_Fixture() : factory(NULL), path("/tmp/factory.test") {}
    void SetUp() {
        FILELog::Level() = LL_INFO;
        if (FileIo::exists(path)) {
            FileIo::rmdir(path);
        }
        factory = MetricsFactory::create(path);
    }
    void TearDown() { delete factory; }
public:
    MetricsFactory* factory;
    const char* path;
};

TEST_F(METER_STATS_Fixture,TestMeter)
{
    bool failure = false;
    try {
        factory->process("create counter three");
        factory->process("create meter one.two with time_window=30 collect_period=10 rate_period=2");
        factory->process("link three one.two");
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    try {
        uint16_t id = dataset->findColumn("one.two");
        ASSERT_TRUE(id != CWSE_INVALID_COLUMN_ID);
        ASSERT_EQ(0.0, dataset->get(id));

        Metric& counter = factory->get("three");

        Metric* metric = factory->find("one.two");
        ASSERT_FALSE(metric == NULL);

        Meter* meter = dynamic_cast<Meter*>(metric);
        ASSERT_FALSE(meter == NULL);

        meter->resetNextTimePoint(10);

        for (int j=0; j < 3; j++) {
            for (int i=10*j+1; i < 10*(j+1); i++) {
                counter.update(1);
                meter->onTimeTick(i);
                ASSERT_EQ(10*j/2, dataset->get(id));
            }

            counter.update(1);
            meter->onTimeTick(10*(j+1));
            ASSERT_EQ(10*(j+1)/2, dataset->get(id)) << "  iteration=" << j << endl;
            ASSERT_EQ(10*(j+2), meter->getNextTimePoint());
        }

        for (int j=3; j < 10; j++) {
            for (int i=10*j+1; i < 10*(j+1); i++) {
                counter.update(1);
                meter->onTimeTick(i);
                ASSERT_EQ(30/2, dataset->get(id));
            }

            counter.update(1);
            meter->onTimeTick(10*(j+1));
            ASSERT_EQ(30/2, dataset->get(id));
            ASSERT_EQ(10*(j+2), meter->getNextTimePoint());
        }

    }
    catch(...) {
        delete dataset;
        ASSERT_TRUE(false);
    }
    delete dataset;
}

TEST_F(METER_STATS_Fixture,GaugeToStats)
{
    bool failure = false;
    try {
        factory->process("create gauge three");
        factory->process("create stats one.two with time_window=30 collect_period=10");
        factory->process("link three one.two");
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    try {
        uint16_t countId = dataset->findColumn("one.two.count");
        uint16_t minId = dataset->findColumn("one.two.min");
        uint16_t maxId = dataset->findColumn("one.two.max");
        uint16_t avgId = dataset->findColumn("one.two.avg");

        ASSERT_TRUE(countId != CWSE_INVALID_COLUMN_ID); ASSERT_EQ(0.0, dataset->get(countId));
        ASSERT_TRUE(minId != CWSE_INVALID_COLUMN_ID);   ASSERT_EQ(0.0, dataset->get(minId));
        ASSERT_TRUE(maxId != CWSE_INVALID_COLUMN_ID);   ASSERT_EQ(0.0, dataset->get(maxId));
        ASSERT_TRUE(avgId != CWSE_INVALID_COLUMN_ID);   ASSERT_EQ(0.0, dataset->get(avgId));

        Metric& gauge = factory->get("three");

        Metric* metric = factory->find("one.two");
        ASSERT_FALSE(metric == NULL);

        Stats* stats = dynamic_cast<Stats*>(metric);
        ASSERT_FALSE(stats == NULL);

        stats->resetNextTimePoint(10);

        for (int j=0; j < 3; j++) {
            for (int i=10*j+1; i < 10*(j+1); i++) {
                gauge.update(i);
                stats->onTimeTick(i);
                ASSERT_EQ(10*j, dataset->get(countId));
            }

            gauge.update(10*(j+1));
            stats->onTimeTick(10*(j+1));
            ASSERT_EQ(10*(j+2), stats->getNextTimePoint());

            ASSERT_EQ(10*(j+1), dataset->get(countId));
            ASSERT_EQ(1, dataset->get(minId));
            ASSERT_EQ(10*(j+1), dataset->get(maxId));
        }

        for (int j=3; j < 10; j++) {
            for (int i=10*j+1; i < 10*(j+1); i++) {
                gauge.update(i);
                stats->onTimeTick(i);
                ASSERT_EQ(30, dataset->get(countId));
            }

            gauge.update(10*(j+1));
            stats->onTimeTick(10*(j+1));
            ASSERT_EQ(10*(j+2), stats->getNextTimePoint());

            ASSERT_EQ(30, dataset->get(countId));
            ASSERT_EQ((j-2)*10+1, dataset->get(minId));
            ASSERT_EQ(10*(j+1), dataset->get(maxId));
        }

    }
    catch(...) {
        delete dataset;
        ASSERT_TRUE(false);
    }
    delete dataset;
}

TEST_F(METER_STATS_Fixture,CounterToStats)
{
    bool failure = false;
    try {
        factory->process("create counter three");
        factory->process("create stats one.two with time_window=30 collect_period=10");
        factory->process("link three one.two");
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    try {
        uint16_t countId = dataset->findColumn("one.two.count");
        uint16_t minId = dataset->findColumn("one.two.min");
        uint16_t maxId = dataset->findColumn("one.two.max");
        uint16_t avgId = dataset->findColumn("one.two.avg");

        ASSERT_TRUE(countId != CWSE_INVALID_COLUMN_ID); ASSERT_EQ(0.0, dataset->get(countId));
        ASSERT_TRUE(minId != CWSE_INVALID_COLUMN_ID);   ASSERT_EQ(0.0, dataset->get(minId));
        ASSERT_TRUE(maxId != CWSE_INVALID_COLUMN_ID);   ASSERT_EQ(0.0, dataset->get(maxId));
        ASSERT_TRUE(avgId != CWSE_INVALID_COLUMN_ID);   ASSERT_EQ(0.0, dataset->get(avgId));

        Metric& counter = factory->get("three");

        Metric* metric = factory->find("one.two");
        ASSERT_FALSE(metric == NULL);

        Stats* stats = dynamic_cast<Stats*>(metric);
        ASSERT_FALSE(stats == NULL);

        stats->resetNextTimePoint(10);

        for (int j=0; j < 3; j++) {
            for (int i=10*j+1; i < 10*(j+1); i++) {
                counter.update(1);
                stats->onTimeTick(i);
                ASSERT_EQ(10*j, dataset->get(countId));
            }

            counter.update(1);
            stats->onTimeTick(10*(j+1));
            ASSERT_EQ(10*(j+2), stats->getNextTimePoint());

            ASSERT_EQ(10*(j+1), dataset->get(countId));
            ASSERT_EQ(1, dataset->get(avgId));
        }

        for (int j=3; j < 10; j++) {
            for (int i=10*j+1; i < 10*(j+1); i++) {
                counter.update(1);
                stats->onTimeTick(i);
                ASSERT_EQ(30, dataset->get(countId));
            }

            counter.update(1);
            stats->onTimeTick(10*(j+1));
            ASSERT_EQ(10*(j+2), stats->getNextTimePoint());

            ASSERT_EQ(30, dataset->get(countId));
            ASSERT_EQ(1, dataset->get(avgId));
        }
    }
    catch(...) {
        delete dataset;
        ASSERT_TRUE(false);
    }
    delete dataset;
}

TEST_F(METER_STATS_Fixture,TestStats)
{
    bool failure = false;
    try {
        factory->process("create stats aaa with time_window=30 collect_period=10");
    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    try {
        uint16_t ids[6];
        ids[0] = dataset->findColumn("aaa.count");
        ids[1] = dataset->findColumn("aaa.stddev");
        ids[2] = dataset->findColumn("aaa.min");
        ids[3] = dataset->findColumn("aaa.max");
        ids[4] = dataset->findColumn("aaa.avg");
        ids[5] = dataset->findColumn("aaa.sum");

        for (size_t i=0; i < sizeof(ids)/sizeof(*ids); i++) {
            ASSERT_TRUE(ids[i] != CWSE_INVALID_COLUMN_ID);
            ASSERT_EQ(0, dataset->get(ids[i]));
        }

        Metric* metric = factory->find("aaa");
        ASSERT_FALSE(metric == NULL);

        Stats* p = dynamic_cast<Stats*>(metric);
        ASSERT_FALSE(p == NULL);

        p->resetNextTimePoint(10);
        double sum = 0.0;
        double src[] = { 9, 2, 5, 4, 12, 7, 8, 11, 9, 3, 7, 4, 12, 5, 4, 10, 9, 6, 9, 4 };
        for (size_t i=0; i < sizeof(src)/sizeof(*src); i++) {
            metric->update(src[i]);
            sum += src[i];
        }
        p->onTimeTick(10);

        ASSERT_EQ(20, dataset->get(ids[0]));  // count
        ASSERT_NEAR(3.061,  dataset->get(ids[1]), 0.1);   // sdev
        ASSERT_EQ(2,  dataset->get(ids[2])); // min
        ASSERT_EQ(12, dataset->get(ids[3])); // max
        ASSERT_EQ(7,  dataset->get(ids[4])); // avg
        ASSERT_DOUBLE_EQ(sum,  dataset->get(ids[5])); // sum

    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        delete dataset;
        ASSERT_TRUE(false);
    }
    delete dataset;
}

static void thread_func_TestStats(boost::barrier& bar, MetricsFactory* factory, const char* name, double* src, size_t count)
{
    Metric& metric = factory->get(name);
    bar.wait();
    for (size_t j=0; j < count; j++) {
        metric.update(src[j]);
    }
}

TEST_F(METER_STATS_Fixture,TestStatsMultiThreaded)
{
    bool failure = false;
    try {
        factory->process("create stats aaa with time_window=30 collect_period=10");
    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    try {
        uint16_t ids[5];
        ids[0] = dataset->findColumn("aaa.count");
        ids[1] = dataset->findColumn("aaa.stddev");
        ids[2] = dataset->findColumn("aaa.min");
        ids[3] = dataset->findColumn("aaa.max");
        ids[4] = dataset->findColumn("aaa.avg");

        for (size_t i=0; i < sizeof(ids)/sizeof(*ids); i++) {
            ASSERT_TRUE(ids[i] != CWSE_INVALID_COLUMN_ID);
            ASSERT_EQ(0, dataset->get(ids[i]));
        }

        Metric* metric = factory->find("aaa");
        ASSERT_FALSE(metric == NULL);

        Stats* p = dynamic_cast<Stats*>(metric);
        ASSERT_FALSE(p == NULL);

        p->resetNextTimePoint(10);
        
        double src1[] = { 9, 2, 5, 4, 12, };
        double src2[] = { 7, 8, 11, 9, 3, };
        double src3[] = { 7, 4, 12, 5, 4, };
        double src4[] = { 10, 9, 6, 9, 4, };

        boost::barrier bar(4);
        const char* name = "aaa";
        boost::thread thr1(boost::bind(&thread_func_TestStats, boost::ref(bar), factory, name, src1, 5));
        boost::thread thr2(boost::bind(&thread_func_TestStats, boost::ref(bar), factory, name, src2, 5));
        boost::thread thr3(boost::bind(&thread_func_TestStats, boost::ref(bar), factory, name, src3, 5));
        boost::thread thr4(boost::bind(&thread_func_TestStats, boost::ref(bar), factory, name, src4, 5));
        thr1.join();
        thr2.join();
        thr3.join();
        thr4.join();

        p->onTimeTick(10);

        ASSERT_EQ(20, dataset->get(ids[0]));  // count
        ASSERT_NEAR(3.061,  dataset->get(ids[1]), 0.1);   // sdev
        ASSERT_EQ(2,  dataset->get(ids[2])); // min
        ASSERT_EQ(12, dataset->get(ids[3])); // max
        ASSERT_EQ(7,  dataset->get(ids[4])); // avg

    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        delete dataset;
        ASSERT_TRUE(false);
    }
    delete dataset;
}


