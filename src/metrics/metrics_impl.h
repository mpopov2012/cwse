/**********************************************
   File:   metrics_impl.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_METRICS_METRICS_IMPL_H
#define CDB_METRICS_METRICS_IMPL_H

#include "metrics_plus_plus.h"
#include "clockwork.h"
#include "metrics/metrics.h"
#include "utils/concurrent_hash_table.h"
#include "utils/concurrent_list.h"
#include "utils/thread.h"
#include <string>

namespace metrics_plus_plus {

class MetricsFactoryImpl : public MetricsFactory
{
public:
    MetricsFactoryImpl();
    virtual ~MetricsFactoryImpl();

    void init(const char* path);

    virtual void start();
    virtual void stop();

    virtual void process(const char* str);

    virtual void createGauge(const char* name);
    virtual void createCounter(const char* name);
    virtual void createMeter(const char* name, int timeWindow, int collectPeriod, int ratePeriod);
    virtual void createStats(const char* name, int timeWindow, int collectPeriod);
    virtual void createPercentiles(const char* name, int timeWindow, int collectPeriod,
                                   const double* percentiles, int count,
                                   int subsetSize = 0);
    virtual void createHistogram(const char* name, int timeWindow, int collectPeriod,
                            const BucketDescr* buckets, int count);

    virtual Metric* find(const char* name);
    virtual Metric& get(const char* name);

    virtual void linkMetrics(const char* from, const char* to);

    virtual void dumpMissingMetrics(const char* path = NULL);

private:
    class DummyMetric : public Metric
    {
    public:
        virtual void update(double) {}
    };
    DummyMetric m_dummy;

    class MetricsThread : public cdb::TimerThread
    {
    public:
        MetricsThread(int ms, void* ptr) : TimerThread(ms, ptr) {}
        virtual void onTimer(void* ptr) {
           MetricsFactoryImpl* pmf = static_cast<MetricsFactoryImpl*>(ptr);
           pmf->backgroundProcess();
        }
    };
    MetricsThread m_backgroundThread;

private:
    typedef cdb::ConcurrentList<TimedMetric*> TimedMetrics;

    cdb::ConcurrentHashTable m_table;
    cdb::ConcurrentList<std::string> m_missing;
    TimedMetrics m_timedMetrics;
    cdb::Dataset* m_dataset;
    bool m_keepGoing;

private:
    void backgroundProcess();
    void checkMetricExists(const char* name, BaseMetric::Type type);
    uint16_t createDatasetColumn(const char* name);
};

} // namespace metrics_plus_plus



#endif // CDB_METRICS_METRICS_IMPL_H
