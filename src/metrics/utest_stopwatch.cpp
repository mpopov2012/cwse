/**********************************************
   File:   utest_stopwatch.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License")
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "metrics_plus_plus.h"
#include "clockwork.h"
#include "utils/timer.h"
#include "gtest/gtest.h"
#include <string>
#include <iostream>

using namespace std;
using namespace cdb;
using namespace metrics_plus_plus;

TEST(METRICS_STOPWATCH, Short)
{
    Stopwatch sw;
    sw.start();
    Timer::sleepMsec(1);
    int64_t diff = sw.stop();
    ASSERT_EQ(diff, 1);
}

TEST(METRICS_STOPWATCH, Long)
{
    Stopwatch sw;
    sw.start();
    Timer::sleepMsec(100);
    int64_t diff = sw.stop();
    ASSERT_TRUE(diff >= 99 && diff <= 101);
}

/***************
TEST(METRICS_STOPWATCH, ExtraLong)
{
    Stopwatch sw;
    sw.start();
    Timer::sleepSec(1);
    int64_t diff = sw.stop();
    ASSERT_EQ(diff, 1000);
}
****************/
