/**********************************************
   File:   utest_counter_gauge.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License")
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "metrics_plus_plus.h"
#include "clockwork.h"
#include "metrics/metrics.h"
#include "utils/file_io.h"
#include "utils/log.h"
#include "gtest/gtest.h"
#include <boost/thread.hpp>
#include <boost/thread/barrier.hpp>
#include <boost/bind.hpp>
#include <string>
#include <iostream>

using namespace std;
using namespace cdb;
using namespace metrics_plus_plus;

class COUNTER_GAUGE_Fixture: public ::testing::Test
{
public:
    COUNTER_GAUGE_Fixture() : factory(NULL), path("/tmp/factory.test") {}
    void SetUp() {
        FILELog::Level() = LL_INFO;
        if (FileIo::exists(path)) {
            FileIo::rmdir(path);
        }
        factory = MetricsFactory::create(path);
    }
    void TearDown() { delete factory; }

    void checkValue(const char* name, double value) {
        Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
        uint16_t id = dataset->findColumn(name);
        ASSERT_TRUE(id != CWSE_INVALID_COLUMN_ID);
        ASSERT_EQ(value, dataset->get(id));
        delete dataset;
    }

public:
    MetricsFactory* factory;
    const char* path;
};

static const int COUNT = 1024*8;

static void thread_func_TestGauge(boost::barrier& bar, MetricsFactory* factory, const char* name)
{
    Metric& gauge = factory->get(name);
    bar.wait();
    for (int index = 1; index <= COUNT; ++index) {
        gauge.update(index);
    }
}

static void thread_func_TestCounter(boost::barrier& bar, MetricsFactory* factory, const char* name)
{
    Metric& gauge = factory->get(name);
    bar.wait();
    for (int index = 1; index <= COUNT; ++index) {
        gauge.update(1);
    }
}

TEST_F(COUNTER_GAUGE_Fixture,TestGaugeLinkedGauge)
{
    string link = "link ";
    string create  = "create gauge ";
    const char* name = "one.two.three";
    const char* linked = "four.five";
    bool failure = false;
    try {
        factory->process((create+name).c_str());
        factory->process((create+linked).c_str());
        factory->process((link+name+" "+linked).c_str());
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);

    const int d = 4;
    boost::barrier bar(d);

    boost::thread thr1(boost::bind(&thread_func_TestGauge, boost::ref(bar), factory, name));
    boost::thread thr2(boost::bind(&thread_func_TestGauge, boost::ref(bar), factory, name));
    boost::thread thr3(boost::bind(&thread_func_TestGauge, boost::ref(bar), factory, name));
    boost::thread thr4(boost::bind(&thread_func_TestGauge, boost::ref(bar), factory, name));
    thr1.join();
    thr2.join();
    thr3.join();
    thr4.join();

    checkValue(name, COUNT);
    checkValue(linked, COUNT);
}

TEST_F(COUNTER_GAUGE_Fixture,TestCounterLinkedCounterLinkedGauge)
{
    string link = "link ";
    string createCounter = "create counter ";
    string createGauge = "create gauge ";
    const char* name = "one.two.three";
    const char* linked = "four.five";
    const char* linkedGauge = "six.seven";

    bool failure = false;
    try {
        factory->process((createCounter+name).c_str());
        factory->process((createCounter+linked).c_str());
        factory->process((createGauge+linkedGauge).c_str());
        factory->process((link+name+" "+linked).c_str());
        factory->process((link+linked+" "+linkedGauge).c_str());
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);

    const int d = 4;
    boost::barrier bar(d);

    boost::thread thr1(boost::bind(&thread_func_TestCounter, boost::ref(bar), factory, name));
    boost::thread thr2(boost::bind(&thread_func_TestCounter, boost::ref(bar), factory, name));
    boost::thread thr3(boost::bind(&thread_func_TestCounter, boost::ref(bar), factory, name));
    boost::thread thr4(boost::bind(&thread_func_TestCounter, boost::ref(bar), factory, name));
    thr1.join();
    thr2.join();
    thr3.join();
    thr4.join();

    checkValue(name, COUNT*d);
    checkValue(linked, COUNT*d);
    checkValue(linkedGauge, 1);
}


