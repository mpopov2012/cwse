/**********************************************
   File:   utest_metrics_factory.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License")
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "metrics_plus_plus.h"
#include "clockwork.h"
#include "utils/file_io.h"
#include "utils/log.h"
#include "gtest/gtest.h"
#include <string>
#include <iostream>

using namespace std;
using namespace cdb;
using namespace metrics_plus_plus;

class METRICS_FACTORY_Fixture: public ::testing::Test
{
public:
    METRICS_FACTORY_Fixture() : factory(NULL), path("/tmp/factory.test") {}
    void SetUp() {
        FILELog::Level() = LL_INFO;
        if (FileIo::exists(path)) {
            FileIo::rmdir(path);
        }
        factory = MetricsFactory::create(path);
    }
    void TearDown() { delete factory; }
public:
    MetricsFactory* factory;
    const char* path;
};

TEST_F(METRICS_FACTORY_Fixture,SimpleCreateFactory)
{
    ASSERT_TRUE(factory != NULL);
    factory->get("non-existent").update(1.1);
    ASSERT_TRUE(factory->find("non-existent") == NULL);
}

TEST_F(METRICS_FACTORY_Fixture,CreateDoubleFactory)
{
#ifdef WIN32
    bool failure = false;
    MetricsFactory* factory2 = NULL;
    try {
        factory2 = MetricsFactory::create(path);
    }
    catch(...) {
        failure = true;
    }
    ASSERT_TRUE(failure);
    ASSERT_TRUE(factory2 == NULL);
#endif
}


TEST_F(METRICS_FACTORY_Fixture,CreateGauge)
{
    bool failure = false;
    try {
        factory->createGauge("one.two.three");
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);
    factory->get("one.two.three").update(1.1);
    ASSERT_TRUE(factory->find("one.two.three") != NULL);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    uint16_t id = dataset->findColumn("one.two.three");
    ASSERT_TRUE(id != CWSE_INVALID_COLUMN_ID);
    ASSERT_EQ(1.1, dataset->get(id));
    delete dataset;
}

TEST_F(METRICS_FACTORY_Fixture,CreateGaugeDDL)
{
    bool failure = false;
    try {
        factory->process("create gauge one.two.three");
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);
    factory->get("one.two.three").update(1.1);
    ASSERT_TRUE(factory->find("one.two.three") != NULL);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    uint16_t id = dataset->findColumn("one.two.three");
    ASSERT_TRUE(id != CWSE_INVALID_COLUMN_ID);
    ASSERT_EQ(1.1, dataset->get(id));
    delete dataset;
}

TEST_F(METRICS_FACTORY_Fixture,CreateCounter)
{
    bool failure = false;
    try {
        factory->createCounter("one.two.three");
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);
    factory->get("one.two.three").update(1);
    factory->get("one.two.three").update(1);
    factory->get("one.two.three").update(1);
    ASSERT_TRUE(factory->find("one.two.three") != NULL);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    uint16_t id = dataset->findColumn("one.two.three");
    ASSERT_TRUE(id != CWSE_INVALID_COLUMN_ID);
    ASSERT_EQ(3, dataset->get(id));
    delete dataset;
}

TEST_F(METRICS_FACTORY_Fixture,CreateCounterDDL)
{
    bool failure = false;
    try {
        factory->process("create counter one.two.three");
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);
    factory->get("one.two.three").update(1);
    factory->get("one.two.three").update(1);
    factory->get("one.two.three").update(1);
    ASSERT_TRUE(factory->find("one.two.three") != NULL);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    uint16_t id = dataset->findColumn("one.two.three");
    ASSERT_TRUE(id != CWSE_INVALID_COLUMN_ID);
    ASSERT_EQ(3, dataset->get(id));
    delete dataset;
}

TEST_F(METRICS_FACTORY_Fixture,CreateMeter)
{
    bool failure = false;
    try {
        factory->createMeter("one.two.three", 60, 10, 60);
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);
    ASSERT_TRUE(factory->find("one.two.three") != NULL);
}

TEST_F(METRICS_FACTORY_Fixture,CreateMeterDDL)
{
    bool failure = false;
    try {
        factory->process("create meter one.two.three with tw=60 cp=10 rp=10");
        factory->process("create meter four.five with time_window=60 collect_period=10 rate_period=10");
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);
    ASSERT_TRUE(factory->find("one.two.three") != NULL);
    ASSERT_TRUE(factory->find("four.five") != NULL);
}

TEST_F(METRICS_FACTORY_Fixture,CreateStats)
{
    bool failure = false;
    try {
        factory->createStats("one.two.three", 60, 10);
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);
    ASSERT_TRUE(factory->find("one.two.three") != NULL);
}

TEST_F(METRICS_FACTORY_Fixture,CreateStatsDDL)
{
    bool failure = false;
    try {
        factory->process("create stats one.two.three with tw=60 cp=10");
        factory->process("create stats four.five with time_window=60 collect_period=10");
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);
    ASSERT_TRUE(factory->find("one.two.three") != NULL);
    ASSERT_TRUE(factory->find("four.five") != NULL);
}

TEST_F(METRICS_FACTORY_Fixture,CreateHistogram)
{
    bool failure = false;
    BucketDescr buckets[2];
    buckets[0].count = 1;
    buckets[0].size = 5;
    buckets[1].count = 1;
    buckets[1].size = 2;
    try {
        factory->createHistogram("one.two.three", 60, 10, buckets, 2);
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);
    ASSERT_TRUE(factory->find("one.two.three") != NULL);
}

TEST_F(METRICS_FACTORY_Fixture,CreateHistogramDDL)
{
    bool failure = false;
    try {
        factory->process("create histogram one.two.three with tw=60 cp=10 bs=1:5,1:2");
        factory->process("create histogram four.five with time_window=60 collect_period=10 buckets=1:5,1:2");
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);
    ASSERT_TRUE(factory->find("one.two.three") != NULL);
    ASSERT_TRUE(factory->find("four.five") != NULL);
}

TEST_F(METRICS_FACTORY_Fixture,CreatePerentiles)
{
    bool failure = false;
    double percentiles[] = { 50, 90, 99, 99.9 };
    try {
        factory->createPercentiles("perc", 60, 10, percentiles, sizeof(percentiles) / sizeof(*percentiles));
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);
    ASSERT_TRUE(factory->find("perc") != NULL);
}

TEST_F(METRICS_FACTORY_Fixture,CreatePercentilesDDL)
{
    bool failure = false;
    try {
        factory->process("create percentiles perc1 with tw=60 cp=10 ps=50:90:99:99.9");
        factory->process("create percentiles perc2 with time_window=60 collect_period=10 percentiles=50:90:99:99.9");
        factory->process("create percentiles perc3 with tw=60 cp=10 ps=50:90:99:99.9 sz=1024");
        factory->process("create percentiles perc4 with time_window=60 collect_period=10 percentiles=50:90:99:99.9 subset_size=2048");
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);
    ASSERT_TRUE(factory->find("perc1") != NULL);
    ASSERT_TRUE(factory->find("perc2") != NULL);
    ASSERT_TRUE(factory->find("perc3") != NULL);
    ASSERT_TRUE(factory->find("perc4") != NULL);
}


