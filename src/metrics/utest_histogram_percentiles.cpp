/**********************************************
   File:   utest_histogram_percentiles.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License")
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "metrics_plus_plus.h"
#include "clockwork.h"
#include "metrics/metrics.h"
#include "utils/file_io.h"
#include "utils/log.h"
#include "gtest/gtest.h"
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/thread/barrier.hpp>
#include <boost/bind.hpp>
#include <string>
#include <iostream>
#include <stdexcept>

using namespace std;
using namespace cdb;
using namespace metrics_plus_plus;

class HIST_PERC_Fixture: public ::testing::Test
{
public:
    HIST_PERC_Fixture() : factory(NULL), path("/tmp/hist_perc.test") {}
    void SetUp() {
        FILELog::Level() = LL_INFO;
        if (FileIo::exists(path)) {
            FileIo::rmdir(path);
        }
        factory = MetricsFactory::create(path);
    }
    void TearDown() { delete factory; }
public:
    MetricsFactory* factory;
    const char* path;
};

static void thread_func_TestHistogram(boost::barrier& bar, MetricsFactory* factory, const char* name)
{
    Metric& metric = factory->get(name);
    bar.wait();
    for (int j=0; j < 100; j++) {
        metric.update(0);
        metric.update(1);
        metric.update(10);
        metric.update(30);
        metric.update(31);
    }
}

TEST_F(HIST_PERC_Fixture,TestHistogram)
{
    bool failure = false;
    try {
        factory->process("create histogram aaa with time_window=30 collect_period=10 bs=3:10");
    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    try {
        uint16_t ids[4];
        ids[0] = dataset->findColumn("aaa.10");
        ids[1] = dataset->findColumn("aaa.20");
        ids[2] = dataset->findColumn("aaa.30");
        ids[3] = dataset->findColumn("aaa.inf");

        for (int i=0; i < 4; i++) {
            ASSERT_TRUE(ids[i] != CWSE_INVALID_COLUMN_ID);
            ASSERT_EQ(0, dataset->get(ids[i]));
        }

        Metric* metric = factory->find("aaa");
        ASSERT_FALSE(metric == NULL);

        Histogram* hist = dynamic_cast<Histogram*>(metric);
        ASSERT_FALSE(hist == NULL);

        hist->resetNextTimePoint(10);

        boost::barrier bar(4);
        const char* name = "aaa";
        boost::thread thr1(boost::bind(&thread_func_TestHistogram, boost::ref(bar), factory, name));
        boost::thread thr2(boost::bind(&thread_func_TestHistogram, boost::ref(bar), factory, name));
        boost::thread thr3(boost::bind(&thread_func_TestHistogram, boost::ref(bar), factory, name));
        boost::thread thr4(boost::bind(&thread_func_TestHistogram, boost::ref(bar), factory, name));
        thr1.join();
        thr2.join();
        thr3.join();
        thr4.join();

        hist->onTimeTick(10);

        ASSERT_EQ(1200, dataset->get(ids[0]));
        ASSERT_EQ(0, dataset->get(ids[1]));
        ASSERT_EQ(400, dataset->get(ids[2]));
        ASSERT_EQ(400, dataset->get(ids[3]));
    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        delete dataset;
        ASSERT_TRUE(false);
    }
    delete dataset;
}

TEST_F(HIST_PERC_Fixture,TestPercentilesExample)
{
    bool failure = false;
    try {
        factory->process("create percentiles aaa with time_window=30 collect_period=10 ps=20:50:90");
    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    try {
        uint16_t ids[3];
        ids[0] = dataset->findColumn("aaa.20");
        ids[1] = dataset->findColumn("aaa.50");
        ids[2] = dataset->findColumn("aaa.90");

        for (size_t i=0; i < sizeof(ids)/sizeof(*ids); i++) {
            ASSERT_TRUE(ids[i] != CWSE_INVALID_COLUMN_ID);
            ASSERT_EQ(0, dataset->get(ids[i]));
        }

        Metric* metric = factory->find("aaa");
        ASSERT_FALSE(metric == NULL);

        Percentiles* p = dynamic_cast<Percentiles*>(metric);
        ASSERT_FALSE(p == NULL);

        p->resetNextTimePoint(10);

        double source[] = { 43, 54, 56, 61, 62, 66, 68, 69, 69, 70, 71, 72, 77, 78, 79, 85, 87, 88, 89, 93, 95, 96, 98, 99, 99 };
        for (size_t i=0; i < sizeof(source)/sizeof(*source); i++) {
            metric->update(source[i]);
        }

        p->onTimeTick(10);

        ASSERT_EQ(64, dataset->get(ids[0]));
        ASSERT_EQ(77, dataset->get(ids[1]));
        ASSERT_EQ(98, dataset->get(ids[2]));
    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        delete dataset;
        ASSERT_TRUE(false);
    }
    delete dataset;
}

static void thread_func_TestPercentiles(boost::barrier& bar, MetricsFactory* factory, const char* name)
{
    Metric& metric = factory->get(name);
    bar.wait();
    for (int j=0; j < 25; j++) {
        double source[] = { 43, 54, 56, 61, 62, 66, 68, 69, 69, 70, 71, 72, 77, 78, 79, 85, 87, 88, 89, 93, 95, 96, 98, 99, 99 };
        for (size_t i=0; i < sizeof(source)/sizeof(*source); i++) {
            metric.update(source[i]);
        }
    }
}


TEST_F(HIST_PERC_Fixture,TestPercentilesExampleMany)
{
    bool failure = false;
    try {
        factory->process("create percentiles aaa with time_window=30 collect_period=10 ps=20:50:90");
    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    try {
        uint16_t ids[3];
        ids[0] = dataset->findColumn("aaa.20");
        ids[1] = dataset->findColumn("aaa.50");
        ids[2] = dataset->findColumn("aaa.90");

        for (size_t i=0; i < sizeof(ids)/sizeof(*ids); i++) {
            ASSERT_TRUE(ids[i] != CWSE_INVALID_COLUMN_ID);
            ASSERT_EQ(0, dataset->get(ids[i]));
        }

        Metric* metric = factory->find("aaa");
        ASSERT_FALSE(metric == NULL);

        Percentiles* p = dynamic_cast<Percentiles*>(metric);
        ASSERT_FALSE(p == NULL);

        p->resetNextTimePoint(10);

        boost::barrier bar(4);
        const char* name = "aaa";
        boost::thread thr1(boost::bind(&thread_func_TestPercentiles, boost::ref(bar), factory, name));
        boost::thread thr2(boost::bind(&thread_func_TestPercentiles, boost::ref(bar), factory, name));
        boost::thread thr3(boost::bind(&thread_func_TestPercentiles, boost::ref(bar), factory, name));
        boost::thread thr4(boost::bind(&thread_func_TestPercentiles, boost::ref(bar), factory, name));
        thr1.join();
        thr2.join();
        thr3.join();
        thr4.join();

        p->onTimeTick(10);

        int results[] = { 64, 77, 98 };
        for (size_t i=0; i < sizeof(results)/sizeof(*results); i++) {
            int val = int(dataset->get(ids[i]));
            int d = 5;
            ASSERT_TRUE(results[i]-d <= val && val <= results[i]+d) << "val=" << val << "   expected=" << results[i] << endl;
        }
    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        delete dataset;
        ASSERT_TRUE(false);
    }
    delete dataset;
}

TEST_F(HIST_PERC_Fixture,TestPercentilesAnotherExample)
{
    bool failure = false;
    try {
        factory->process("create percentiles aaa with time_window=30 collect_period=10 ps=25:85");
    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    try {
        uint16_t ids[2];
        ids[0] = dataset->findColumn("aaa.25");
        ids[1] = dataset->findColumn("aaa.85");

        for (size_t i=0; i < sizeof(ids)/sizeof(*ids); i++) {
            ASSERT_TRUE(ids[i] != CWSE_INVALID_COLUMN_ID);
            ASSERT_EQ(0, dataset->get(ids[i]));
        }

        Metric* metric = factory->find("aaa");
        ASSERT_FALSE(metric == NULL);

        Percentiles* p = dynamic_cast<Percentiles*>(metric);
        ASSERT_FALSE(p == NULL);

        p->resetNextTimePoint(10);

        double source[] = { 4, 4, 5, 5, 5, 5, 6, 6, 6, 7, 7, 7, 8, 8, 9, 9, 9, 10, 10, 10 };
        for (size_t i=0; i < sizeof(source)/sizeof(*source); i++) {
            metric->update(source[i]);
        }

        p->onTimeTick(10);

        ASSERT_EQ(5, dataset->get(ids[0]));
        ASSERT_TRUE(9.5 == dataset->get(ids[1])) << "  val=" << dataset->get(ids[1]) << endl;
    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        delete dataset;
        ASSERT_TRUE(false);
    }
    delete dataset;
}

TEST_F(HIST_PERC_Fixture,TestPercentilesMany)
{
    bool failure = false;
    try {
        factory->process("create percentiles aaa with time_window=30 collect_period=10 ps=50:90:99:99.9");
    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        failure = true;
    }
    ASSERT_FALSE(failure);

    Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    try {
        uint16_t ids[4];
        ids[0] = dataset->findColumn("aaa.50");
        ids[1] = dataset->findColumn("aaa.90");
        ids[2] = dataset->findColumn("aaa.99");
        ids[3] = dataset->findColumn("aaa.99.9");

        for (int i=0; i < 4; i++) {
            ASSERT_TRUE(ids[i] != CWSE_INVALID_COLUMN_ID);
            ASSERT_EQ(0, dataset->get(ids[i]));
        }

        Metric* metric = factory->find("aaa");
        ASSERT_FALSE(metric == NULL);

        Percentiles* p = dynamic_cast<Percentiles*>(metric);
        ASSERT_FALSE(p == NULL);

        p->resetNextTimePoint(10);

        for (int i=1; i <= 100; i++) {
            metric->update(i);
        }

        p->onTimeTick(10);

        ASSERT_EQ(50.5, dataset->get(ids[0]));
        ASSERT_EQ(90.5, dataset->get(ids[1]));
        ASSERT_EQ(99.5, dataset->get(ids[2]));
        ASSERT_EQ(100, dataset->get(ids[3]));
    }
    catch(std::runtime_error& ex) {
        cout << "Failed: " << ex.what() << endl;
        failure = true;
    }
    catch(...) {
        delete dataset;
        ASSERT_TRUE(false);
    }
    delete dataset;
}

