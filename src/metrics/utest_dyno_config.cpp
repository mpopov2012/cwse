/**********************************************
   File:   utest_dyno_config.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License")
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "metrics_plus_plus.h"
#include "clockwork.h"
#include "metrics/dyno_config.h"
#include "utils/file_io.h"
#include "utils/log.h"
#include "gtest/gtest.h"
#include <string>
#include <iostream>

using namespace std;
using namespace cdb;
using namespace metrics_plus_plus;

class DYNO_CONFIG_Fixture: public ::testing::Test
{
public:
    DYNO_CONFIG_Fixture() : dynoConfig(NULL), path("/tmp/dyno_config.test") {}

    void SetUp()
    {
        FILELog::Level() = LL_INFO;
        if (FileIo::exists(path)) {
            FileIo::rmdir(path);
        }
        createConfigDataset();
        dynoConfig = DynoConfig::create(path);
    }

    void TearDown()
    {
        delete dynoConfig;
    }

public:
    DynoConfig* dynoConfig;
    const char* path;

public:
    void createConfigDataset()
    {
        CwseDatasetConfig cfg = Dataset::getDefaultDatasetConfig();
        Dataset::create(path, &cfg);

        Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_WRITE);

        CwseColumnConfig ccfg = Dataset::getDefaultColumnConfig();
        dataset->addColumn("one", &ccfg, false);
        dataset->addColumn("two", &ccfg, false);
        dataset->addColumn("three", &ccfg, false);

        dataset->set(1.1, dataset->findColumn("one"));
        dataset->set(2.2, dataset->findColumn("two"));
        dataset->set(3.3, dataset->findColumn("three"));

        delete dataset;
    }

    void setValue(const char* name, double value)
    {
        Dataset* dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_WRITE);
        dataset->set(value, dataset->findColumn(name));
        delete dataset;
    }
};

TEST_F(DYNO_CONFIG_Fixture,Basics)
{
    {
        DynoConfigValue value = dynoConfig->get("one", 0.0);
        ASSERT_EQ(1.1, value());
    }

    {
        DynoConfigValue value = dynoConfig->get("two", 0.0);
        ASSERT_EQ(2.2, value());
    }

    {
        DynoConfigValue value = dynoConfig->get("three", 0.0);
        ASSERT_EQ(3.3, value());
    }

    {
        DynoConfigValue value = dynoConfig->get("zero", 4.4);
        ASSERT_EQ(4.4, value());
    }
}

TEST_F(DYNO_CONFIG_Fixture,ConfigUpdates)
{
    DynoConfigValue value = dynoConfig->get("one", 0.0);
    ASSERT_EQ(1.1, value());

    setValue("one", 5.5);
    ASSERT_EQ(5.5, value());
}

TEST_F(DYNO_CONFIG_Fixture,NonExistentDataset)
{
    const char* badPath = "non-existent-path";

    DynoConfig* dyno = DynoConfig::create(badPath);

    DynoConfigValue value = dyno->get("eight", 8.8);
    ASSERT_EQ(8.8, value());

    if (FileIo::exists(badPath)) {
        FileIo::rmdir(badPath);
    }
}
