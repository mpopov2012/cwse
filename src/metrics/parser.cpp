/**********************************************
   File:   parser.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "metrics/parser.h"
#include "utils/config.h"
#include "utils/string_utils.h"
#include <boost/lexical_cast.hpp>
#include <string>
#include <vector>
#include <stdexcept>

using namespace std;

namespace metrics_plus_plus {

static void createMetric(vector< string >& tokens, MetricsFactoryImpl& factory);
static void linkMetrics(vector< string >& tokens, MetricsFactoryImpl& factory);
static void createMeter(vector< string >& tokens, MetricsFactoryImpl& factory);
static void createStats(vector< string >& tokens, MetricsFactoryImpl& factory);
static void createHistogram(vector< string >& tokens, MetricsFactoryImpl& factory);
static void createPercentiles(vector< string >& tokens, MetricsFactoryImpl& factory);
static int extractParameter(cdb::Config& cfg, const char* longName, const char* shortName);
static string extractStringParameter(cdb::Config& cfg, const char* longName, const char* shortName);

void parse_metrics_ddl(const char* str, MetricsFactoryImpl& factory)
{
    std::string s = str;
    cdb::StringUtils::trim(s);

    std::vector< std::string > tokens;
    cdb::StringUtils::split(s, " \t", tokens);

    if (tokens.size() < 3) {
        throw std::runtime_error("Invalid DDL string: too short");
    }

    if (tokens[0] == "create") {
        createMetric(tokens, factory);
    }
    else if (tokens[0] == "link") {
        linkMetrics(tokens, factory);
    }
    else {
        throw std::runtime_error("Invalid DDL string: unrecognized command");
    }
}

static void createMetric(vector< string >& tokens, MetricsFactoryImpl& factory)
{
    if (tokens[1] == "gauge") {
        if (tokens.size() != 3) {
            throw std::runtime_error("Invalid DDL string: too long");
        }
        factory.createGauge(tokens[2].c_str());
    }
    else if (tokens[1] == "counter") {
        if (tokens.size() != 3) {
            throw std::runtime_error("Invalid DDL string: too long");
        }
        factory.createCounter(tokens[2].c_str());
    }
    else if (tokens[1] == "meter") {
        if (tokens.size() != 7) {
            throw std::runtime_error("Invalid DDL string: invalid number of tokens");
        }
        if (tokens[3] != "with") {
            throw std::runtime_error("Invalid DDL string: missing keyword with");
        }
        createMeter(tokens, factory);
    }
    else if (tokens[1] == "stats") {
        if (tokens.size() != 6) {
            throw std::runtime_error("Invalid DDL string: invalid number of tokens");
        }
        if (tokens[3] != "with") {
            throw std::runtime_error("Invalid DDL string: missing keyword with");
        }
        createStats(tokens, factory);
    }
    else if (tokens[1] == "histogram") {
        if (tokens.size() != 7) {
            throw std::runtime_error("Invalid DDL string: invalid number of tokens");
        }
        if (tokens[3] != "with") {
            throw std::runtime_error("Invalid DDL string: missing keyword with");
        }
        createHistogram(tokens, factory);
    }
    else if (tokens[1] == "percentiles") {
        if (tokens.size() != 7 && tokens.size() != 8) {
            throw std::runtime_error("Invalid DDL string: invalid number of tokens");
        }
        if (tokens[3] != "with") {
            throw std::runtime_error("Invalid DDL string: missing keyword with");
        }
        createPercentiles(tokens, factory);
    }
    else {
        throw std::runtime_error("Invalid DDL string: unrecognized metric type");
    }
}

static void linkMetrics(vector< string >& tokens, MetricsFactoryImpl& factory)
{
    factory.linkMetrics(tokens[1].c_str(), tokens[2].c_str());
}

static void createMeter(vector< string >& tokens, MetricsFactoryImpl& factory)
{
    cdb::Config cfg;
    cfg.process(tokens[4]);
    cfg.process(tokens[5]);
    cfg.process(tokens[6]);

    int timeWindow = extractParameter(cfg, "time_window", "tw");
    int collectPeriod = extractParameter(cfg, "collect_period", "cp");
    int ratePeriod = extractParameter(cfg, "rate_period", "rp");

    factory.createMeter(tokens[2].c_str(), timeWindow, collectPeriod, ratePeriod);
}

static void createStats(vector< string >& tokens, MetricsFactoryImpl& factory)
{
    cdb::Config cfg;
    cfg.process(tokens[4]);
    cfg.process(tokens[5]);

    int timeWindow = extractParameter(cfg, "time_window", "tw");
    int collectPeriod = extractParameter(cfg, "collect_period", "cp");

    factory.createStats(tokens[2].c_str(), timeWindow, collectPeriod);
}

static void createHistogram(vector< string >& tokens, MetricsFactoryImpl& factory)
{
    cdb::Config cfg;
    cfg.process(tokens[4]);
    cfg.process(tokens[5]);
    cfg.process(tokens[6]);

    int timeWindow = extractParameter(cfg, "time_window", "tw");
    int collectPeriod = extractParameter(cfg, "collect_period", "cp");
    string bucketsStr = extractStringParameter(cfg, "buckets", "bs");

    vector< string > bucketTokens;
    cdb::StringUtils::split(bucketsStr, ",", bucketTokens);
    if (bucketTokens.size() == 0) {
        throw std::runtime_error("Invalid DDL string: empty buckets string");
    }

    vector<BucketDescr> descrs;
    descrs.reserve(bucketTokens.size());
    for (size_t i=0; i < bucketTokens.size(); i++) {
        vector< string > params;
        cdb::StringUtils::split(bucketTokens[i], ":", params);
        if (params.size() != 2) {
            throw std::runtime_error(string("Invalid DDL string: invalid buckets string [")+bucketTokens[i]+"]");
        }
        BucketDescr descr;
        descr.count = boost::lexical_cast<int>(params[0]);
        descr.size = boost::lexical_cast<int>(params[1]);
        descrs.push_back(descr);
    }

    factory.createHistogram(tokens[2].c_str(), timeWindow, collectPeriod, &descrs[0], (int)descrs.size());
}

static void createPercentiles(vector< string >& tokens, MetricsFactoryImpl& factory)
{
    cdb::Config cfg;
    cfg.process(tokens[4]);
    cfg.process(tokens[5]);
    cfg.process(tokens[6]);
    if (tokens.size() == 8) {
        cfg.process(tokens[7]);
    }

    int timeWindow = extractParameter(cfg, "time_window", "tw");
    int collectPeriod = extractParameter(cfg, "collect_period", "cp");
    string percStr = extractStringParameter(cfg, "percentiles", "ps");

    int subsetSize = 0;
    if (tokens.size() == 8) {
        subsetSize = extractParameter(cfg, "subset_size", "sz");
    }

    vector< string > percTokens;
    cdb::StringUtils::split(percStr, ":", percTokens);
    if (percTokens.size() == 0) {
        throw std::runtime_error("Invalid DDL string: empty percentiles string");
    }

    vector<double> perc;
    perc.reserve(percTokens.size());
    for (size_t i=0; i < percTokens.size(); i++) {
        perc.push_back(boost::lexical_cast<double>(percTokens[i]));
    }

    factory.createPercentiles(tokens[2].c_str(), timeWindow, collectPeriod, &perc[0], (int)perc.size(), subsetSize);
}

static int extractParameter(cdb::Config& cfg, const char* longName, const char* shortName)
{
    string str;
    if (cfg.find(longName, str) || cfg.find(shortName, str)) {
        return boost::lexical_cast<int>(str);
    }
    throw std::runtime_error(string("Invalid DDL string: missing parameter ") + longName);
}

static string extractStringParameter(cdb::Config& cfg, const char* longName, const char* shortName)
{
    string str;
    if (cfg.find(longName, str) || cfg.find(shortName, str)) {
        return str;
    }
    throw std::runtime_error(string("Invalid DDL string: missing parameter ") + longName);
}

} // namespace metrics_plus_plus
