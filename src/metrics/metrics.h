/**********************************************
   File:   metrics.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_METRICS_METRICS_H
#define CDB_METRICS_METRICS_H

#include "metrics_plus_plus.h"
#include "clockwork.h"
#include "utils/concurrent_list.h"
#include "utils/fast_rand.h"
#include <vector>

namespace metrics_plus_plus {

class BaseMetric : public Metric
{
public:
    enum Type { GAUGE, COUNTER, METER, STATS, HIST, PERC };

public:
    BaseMetric(BaseMetric::Type type) : m_type(type) {}
    BaseMetric::Type type() const { return m_type; }

    void link(BaseMetric* toMetric);
    void checkLink(int prevDepth, BaseMetric* fromMetric);

private:
    BaseMetric::Type m_type;

    typedef cdb::ConcurrentList<BaseMetric*> Links;
    Links m_links;

protected:
    void updateLinkedMetrics(double val);
};

class Gauge : public BaseMetric
{
public:
    Gauge(cdb::Dataset* dataset, uint16_t id) : BaseMetric(BaseMetric::GAUGE), m_dataset(dataset), m_id(id) {}
    virtual void update(double val) { m_dataset->set(val, m_id); updateLinkedMetrics(val); }
private:
    cdb::Dataset* m_dataset;
    uint16_t m_id;
};

class Counter : public BaseMetric
{
public:
    Counter(cdb::Dataset* dataset, uint16_t id) : BaseMetric(BaseMetric::COUNTER), m_dataset(dataset), m_id(id) {}
    virtual void update(double val) { m_dataset->inc(val, m_id); updateLinkedMetrics(val); }
private:
    cdb::Dataset* m_dataset;
    uint16_t m_id;
};

class TimedMetric : public BaseMetric
{
public:
    TimedMetric(BaseMetric::Type type, cdb::Dataset* dataset, uint16_t id, uint64_t timeWindow, uint64_t collectPeriod)
        : BaseMetric(type),
          m_dataset(dataset),
          m_id(id),
          m_timeWindow(timeWindow),
          m_collectPeriod(collectPeriod),
          m_count(0),
          m_current(0),
          m_nextTimePoint(0)
    {
    }

    virtual void init();
    void onTimeTick(uint64_t timeTick);

    void resetNextTimePoint(uint64_t nextTimePoint) { m_nextTimePoint = nextTimePoint; }
    uint64_t getNextTimePoint() const { return m_nextTimePoint; }

protected:
    cdb::Dataset* m_dataset;
    uint16_t m_id;
    uint64_t m_timeWindow;
    uint64_t m_collectPeriod;
    uint32_t m_count;
    uint32_t m_current;
    uint64_t m_nextTimePoint;

protected:
    virtual uint64_t getMaxCount() const = 0;
    virtual void onTimeProcess() = 0;
};

class Meter : public TimedMetric
{
public:
    Meter(cdb::Dataset* dataset, uint16_t id, uint64_t timeWindow, uint64_t collectPeriod, uint64_t ratePeriod)
      : TimedMetric(BaseMetric::METER, dataset, id, timeWindow, collectPeriod),
        m_ratePeriod(ratePeriod),
        m_values(NULL)
    {
    }
    virtual ~Meter() { delete[] m_values; }
    virtual void update(double val);

    virtual void init();

protected:
    static const uint64_t MAX_COUNT = 3600;

    virtual uint64_t getMaxCount() const { return MAX_COUNT; }
    virtual void onTimeProcess();

private:
    uint64_t m_ratePeriod;
    double* m_values;
};

struct StatsColumns
{
    uint16_t m_countId;
    uint16_t m_sumId;
    uint16_t m_minId;
    uint16_t m_maxId;
    uint16_t m_avgId;
    uint16_t m_sdevId;
};

class Stats : public TimedMetric
{
public:
    Stats(cdb::Dataset* dataset,
            const StatsColumns& cols,
            uint64_t timeWindow, uint64_t collectPeriod)
      : TimedMetric(BaseMetric::STATS, dataset, cols.m_countId, timeWindow, collectPeriod),
        m_items(NULL),
        m_cols(cols)
    {
    }
    virtual ~Stats() { delete[] m_items; }
    virtual void update(double val);

    virtual void init();

protected:
    static const uint64_t MAX_COUNT = 3600;

    virtual uint64_t getMaxCount() const { return MAX_COUNT; }
    virtual void onTimeProcess();

private:
    struct StatsItem
    {
        double m_min;
        double m_max;
        double m_sum;
        int64_t m_count;
        double m_old[2];

        void update(double val);
    };

    StatsItem* m_items;
    StatsColumns m_cols;
};

class Histogram : public TimedMetric
{
public:
    Histogram(cdb::Dataset* dataset, const std::vector<double>& edges, const std::vector<uint16_t>& ids,
            uint64_t timeWindow, uint64_t collectPeriod)
      : TimedMetric(BaseMetric::HIST, dataset, CWSE_INVALID_COLUMN_ID, timeWindow, collectPeriod)
    {
        prepare(edges, ids);
    }
    virtual ~Histogram();
    virtual void update(double val);
    virtual void init();

protected:
    static const uint64_t MAX_COUNT = 100;

    virtual uint64_t getMaxCount() const { return MAX_COUNT; }
    virtual void onTimeProcess();

private:
    std::vector<double> m_edges;
    std::vector<uint16_t> m_ids;
    std::vector<int64_t*> m_items;
    std::vector<int64_t> m_values;

private:
    void prepare(const std::vector<double>& edges, const std::vector<uint16_t>& ids);
};

class Percentiles : public TimedMetric
{
public:
    Percentiles(cdb::Dataset* dataset, 
            uint16_t subsetSize,
            const std::vector<double>& perc,
            const std::vector<uint16_t>& ids,
            uint64_t timeWindow, uint64_t collectPeriod)
      : TimedMetric(BaseMetric::PERC, dataset, CWSE_INVALID_COLUMN_ID, timeWindow, collectPeriod),
        m_subsetSize(DEFAULT_SUBSET_SIZE)
    {
        prepare(subsetSize, perc, ids);    
    }
    virtual ~Percentiles();
    virtual void update(double val);
    virtual void init();

protected:
    static const uint64_t MAX_COUNT = 100;
    static const uint16_t DEFAULT_SUBSET_SIZE = 1024;

    virtual uint64_t getMaxCount() const { return MAX_COUNT; }
    virtual void onTimeProcess();

private:
    struct PercBucket
    {
        PercBucket(uint16_t sz) : m_count(0) { m_values.resize(sz); }

        int64_t m_count;
        cdb::FastRand m_rand;
        std::vector<double> m_values;

        void add(double val);
        bool isAdded(uint16_t count, uint16_t& index);
    };

private:
    uint16_t m_subsetSize;
    std::vector<double> m_perc;
    std::vector<uint16_t> m_ids;
    std::vector<PercBucket*> m_items;
    std::vector<double> m_values;

private:
    void prepare(uint16_t subsetSize, const std::vector<double>& perc, const std::vector<uint16_t>& ids);
    double getPercentileValue(uint16_t count, double perc);
};



}

#endif // CDB_METRICS_METRICS_H
