/**********************************************
   File:   dyno_config.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_METRICS_DYNO_CONFIG_H
#define CDB_METRICS_DYNO_CONFIG_H

#include "metrics_plus_plus.h"
#include "clockwork.h"
#include <map>
#include <string>

namespace metrics_plus_plus {

class DynoConfigImpl : public DynoConfig
{
    friend class DynoConfig;

public:
    static void TestOnly_DeleteDynoConfig(const std::string& path);

public:
    DynoConfigImpl(const char* path);
    ~DynoConfigImpl();

    virtual DynoConfigValue get(const char* name, double defaultValue);

private:
    cdb::Dataset* m_dataset;
};

} // namespace metrics_plus_plus

#endif // CDB_METRICS_DYNO_CONFIG_H
