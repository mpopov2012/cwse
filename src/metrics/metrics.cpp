/**********************************************
   File:   metrics.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "utils/atomic.h"
#include "utils/timer.h"
#include "metrics/metrics.h"
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <map>
#include <math.h>

#define MAX_LINK_DEPTH 16

using namespace std;

namespace metrics_plus_plus {

/************************************************************
 *
 *       BaseMetric
 */

void BaseMetric::link(BaseMetric* toMetric)
{
    toMetric->checkLink(1, this);

    Links::Iterator iter = m_links.getIterator();
    while (iter.hasNext()) {
        if (iter.value() == toMetric) {
            throw std::runtime_error("Duplicated link");
        }
        iter.next();
    }

    m_links.add(toMetric);
}

void BaseMetric::checkLink(int prevDepth, BaseMetric* fromMetric)
{
    if (prevDepth >= MAX_LINK_DEPTH-1) {
        throw std::runtime_error("Link depth is too deep");
    }

    Links::Iterator iter = m_links.getIterator();
    while (iter.hasNext()) {
        if (iter.value() == fromMetric) {
            throw std::runtime_error("Circular link");
        }
        iter.value()->checkLink(prevDepth+1, fromMetric);
        iter.next();
    }
}

void BaseMetric::updateLinkedMetrics(double val)
{
    Links::Iterator iter = m_links.getIterator();
    while (iter.hasNext()) {
        iter.value()->update(val);
        iter.next();
    }
}

/************************************************************
 *
 *       TimedMetric
 */

void TimedMetric::init()
{
    if (m_collectPeriod == 0) {
        m_collectPeriod = 1;
    }

    m_count = (uint32_t)(m_timeWindow / m_collectPeriod);

    if (m_count > getMaxCount()) {
        m_count = (uint32_t)getMaxCount();
    }

    uint64_t ts = cdb::Timer::getCurrentTimestampMs();
    ts /= 1000;
    m_nextTimePoint = ts + m_timeWindow;
}

void TimedMetric::onTimeTick(uint64_t timeTick)
{
    if (timeTick < m_nextTimePoint) return;
    onTimeProcess();
    m_nextTimePoint += m_collectPeriod;
    m_current = (m_current + 1) % m_count;
}

/************************************************************
 *
 *       Meter
 */

void Meter::init()
{
    TimedMetric::init();
    m_values = new double[m_count];
    memset(m_values, 0, m_count * sizeof(double));
}

void Meter::update(double val)
{
    for (uint16_t i = 0; i < m_count; i++) {
        cdb::Atomic::incd(&m_values[i], val);
    }
    updateLinkedMetrics(val);
}

void Meter::onTimeProcess()
{
    m_dataset->set(m_values[m_current] / m_ratePeriod, m_id);
    m_values[m_current] = 0.0;
}

/************************************************************
 *
 *       Stats
 */

void Stats::init()
{
    TimedMetric::init();
    m_items = new StatsItem[m_count];
    memset(m_items, 0, m_count * sizeof(StatsItem));
}

void Stats::update(double val)
{
    for (uint16_t i = 0; i < m_count; i++) {
        m_items[i].update(val);
    }
    updateLinkedMetrics(val);
}

void Stats::onTimeProcess()
{
    StatsItem cur;
    memcpy(&cur, m_items+m_current, sizeof(StatsItem));
    memset(m_items+m_current, 0, sizeof(StatsItem));

    m_dataset->set(cur.m_sum, m_cols.m_sumId);
    m_dataset->set(cur.m_max, m_cols.m_maxId);
    m_dataset->set(cur.m_min, m_cols.m_minId);
    m_dataset->set((double)cur.m_count, m_cols.m_countId);
    if (cur.m_count == 0) {
        m_dataset->set(0.0, m_cols.m_avgId);
        m_dataset->set(0.0, m_cols.m_sdevId);
    }
    else {
        double variance = cur.m_count <= 1 ? 0.0 : cur.m_old[1] / (cur.m_count - 1);
        double stdDev = sqrt(variance);
        m_dataset->set(cur.m_sum / cur.m_count, m_cols.m_avgId);
        m_dataset->set(stdDev, m_cols.m_sdevId);
    }
}

void Stats::StatsItem::update(double val)
{
    // http://www.johndcook.com/standard_deviation.html
    // Copy data on stack to avoid too much grossness of multithreading calculation
    double old[2];
    double cur[2];
    int64_t count = m_count;
    memcpy(old, m_old, sizeof(m_old));
    memcpy(cur, old, sizeof(m_old));
    count++;
    if (count == 1) {
        old[0] = cur[0] = val;
    }
    else {
        cur[0] = old[0] + (val - old[0]) / count;
        cur[1] = old[1] + (val - old[0]) * (val - cur[0]);
    }
    memcpy(m_old, cur, sizeof(m_old));

    cdb::Atomic::incd(&m_sum, val);
    if (m_max < val) m_max = val;
    if (m_count == 0) {
        m_min = val;
    }
    else {
        if (m_min > val) m_min = val;
    }
    cdb::Atomic::inc64(&m_count, 1);
}

/************************************************************
 *
 *       Histogram
 */

Histogram::~Histogram()
{
    for (size_t i=0; i < m_count; i++) {
        delete[] m_items[i];
    }
}

void Histogram::update(double val)
{
    vector<double>::iterator iter = lower_bound(m_edges.begin(), m_edges.end(), val);
    size_t index = (iter == m_edges.end()) ? m_edges.size() : (iter - m_edges.begin());
    for (size_t i=0; i < m_count; i++) {
        cdb::Atomic::inc64(m_items[i] + index, 1);
    }
    updateLinkedMetrics(val);
}

void Histogram::init()
{
    TimedMetric::init();

    m_items.reserve(m_count);
    for (size_t i=0; i < m_count; i++) {
        int64_t* values = new int64_t[m_ids.size()];
        memset(values, 0, m_ids.size() * sizeof(int64_t));
        m_items.push_back(values);
    }
}

void Histogram::onTimeProcess()
{
    memcpy(&m_values[0], m_items[m_current], sizeof(int64_t) * m_ids.size());
    memset(m_items[m_current], 0, sizeof(int64_t) * m_ids.size());
    for (size_t i=0; i < m_ids.size(); i++) {
        m_dataset->set((double)m_values[i], m_ids[i]);
    }
}

void Histogram::prepare(const std::vector<double>& edges, const std::vector<uint16_t>& ids)
{
    if (edges.size()+1 != ids.size() || edges.size() == 0) {
        throw std::runtime_error("Invalid edges and ids");
    }

    m_edges.reserve(edges.size());
    for (size_t i=0; i < edges.size(); i++) {
        m_edges.push_back(edges[i]);
    }

    m_ids.reserve(ids.size());
    for (size_t i=0; i < ids.size(); i++) {
        m_ids.push_back(ids[i]);
    }

    m_values.resize(ids.size());
}

/************************************************************
 *
 *       Perventiles
 */

Percentiles::~Percentiles()
{
    for (size_t i=0; i < m_items.size(); i++) {
        delete m_items[0];
    }
}

void Percentiles::prepare(uint16_t subsetSize, const std::vector<double>& perc, const std::vector<uint16_t>& ids)
{
    if (ids.size() == 0 || ids.size() != perc.size()) {
        throw std::runtime_error("Invalid perc and ids");
    }

    if (subsetSize > 0) {
        m_subsetSize = subsetSize;
    }

    m_perc.reserve(perc.size());
    m_ids.reserve(ids.size());
    for (size_t i=0; i < perc.size(); i++) {
        m_perc.push_back(perc[i]);
        m_ids.push_back(ids[i]);
    }
}

void Percentiles::update(double val)
{
    for (size_t i=0; i < m_count; i++) {
        m_items[i]->add(val);
    }
    updateLinkedMetrics(val);
}

void Percentiles::init()
{
    TimedMetric::init();

    m_items.reserve(m_count);
    for (size_t i=0; i < m_count; i++) {
        m_items.push_back(new PercBucket(m_subsetSize));
    }

    m_values.resize(m_subsetSize);
}

void Percentiles::onTimeProcess()
{
    //cout << "******************************" << endl;
    //cout << "COUNT=" << m_items[m_current]->m_count << endl;

    double* src = &(m_items[m_current]->m_values[0]);
    
    memcpy(&m_values[0], src, sizeof(double) * m_values.size());
    uint16_t count = (uint16_t) m_items[m_current]->m_count;
    if (count >= m_values.size()) count = m_values.size() - 1; 

    memset(src, 0, sizeof(double) * m_values.size());
    m_items[m_current]->m_count = 0;

    std::sort(&m_values[0], &m_values[count]);

    //std::map<double, size_t> vals;
    //for (size_t i=0; i < count; i++) {
    //    if (vals.find(m_values[i]) == vals.end()) {
    //        vals[m_values[i]] = 1;
    //    }
    //    else {
    //        vals[m_values[i]]++;
    //    }
    //}
    //cout << "======================================" << endl;
    //for (std::map<double, size_t>::iterator iter = vals.begin(); iter != vals.end(); ++iter) {
    //    cout << iter->first << " => " << iter->second << endl;
    //}

    for (size_t i=0; i < m_perc.size(); i++) {
        double val = getPercentileValue(count, m_perc[i]);
        m_dataset->set(val, m_ids[i]);
    }
}

double Percentiles::getPercentileValue(uint16_t count, double perc)
{
    double intpart;
    double fractpart = modf((perc * count) / 100.0, &intpart);
    
    double result = 0.0;
    if (fractpart == 0) {
        uint16_t low = uint16_t(intpart - 1);
        uint16_t high = uint16_t(intpart);
        if (low >= count) low = count - 1;
        if (high >= count) high = count - 1;
        result = (m_values[low] + m_values[high]) / 2.0;
    }
    else {
        uint16_t index = (fractpart < 0.5) ? (uint16_t) intpart - 1 : (uint16_t) intpart; // adjust to 0 started indexing
        if (index >= count) index = count - 1;
        result = m_values[index];
    }
    return result;
}

void Percentiles::PercBucket::add(double val)
{
    uint16_t count = uint16_t(m_count);

    uint16_t index = count;
    if (index < m_values.size()) {
        m_values[index] = val;
        m_count = count + 1;
    }
    else {
        bool b = isAdded(count, index);
        if (b) m_values[index] = val;
        cdb::Atomic::inc64(&m_count, 1);
    }

}

bool Percentiles::PercBucket::isAdded(uint16_t count, uint16_t& index)
{
    int64_t sz = (int64_t) m_values.size();

    int64_t r = m_rand.next();
    r = r % count;

    if (r >= sz) {
        return false;
    }

    index = (uint16_t)(m_rand.next() % sz);
    return true;
}

} // namespace metrics_plus_plus
