/**********************************************
   File:   dyno_config.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "metrics/dyno_config.h"

using std::string;

namespace metrics_plus_plus {

/**************************************************************
 *
 *   DynoConfig
 *
 */

// static
DynoConfig* DynoConfig::create(const char* path)
{
    return new DynoConfigImpl(path);
}

/**************************************************************
 *
 *   DynoConfigValue
 *
 */

DynoConfigValue::DynoConfigValue(cdb::Dataset* dataset, uint16_t id, double defaultValue)
  : m_dataset(dataset),
    m_id(id),
    m_defaultValue(defaultValue)
{

}

DynoConfigValue::DynoConfigValue(const DynoConfigValue& another)
  : m_dataset(another.m_dataset),
    m_id(another.m_id),
    m_defaultValue(another.m_defaultValue)
{

}

DynoConfigValue& DynoConfigValue::operator=(const DynoConfigValue& another)
{
    if (this != &another) {
        m_dataset = another.m_dataset;
        m_id = another.m_id;
        m_defaultValue = another.m_defaultValue;
    }
    return *this;
}

double DynoConfigValue::operator()() const
{
    if (m_dataset == NULL || m_id == CWSE_INVALID_COLUMN_ID) {
        return m_defaultValue;
    }
    return m_dataset->get(m_id);
}

/**************************************************************
 *
 *   DynoConfigImpl
 *
 */

DynoConfigImpl::DynoConfigImpl(const char* path)
{
    try {
        m_dataset = cdb::Dataset::open(path, CWSE_DATA_ACCESS_READ_ONLY);
    }
    catch (...) {
        m_dataset = NULL;
    }
}

DynoConfigImpl::~DynoConfigImpl()
{
    if (m_dataset != NULL) {
        delete m_dataset;
    }
}

DynoConfigValue DynoConfigImpl::get(const char* name, double defaultValue)
{
    if (m_dataset == NULL) {
        return DynoConfigValue(NULL, CWSE_INVALID_COLUMN_ID, defaultValue);
    }

    uint16_t id;
    try {
        id = m_dataset->findColumn(name);
    }
    catch (...) {
        id = CWSE_INVALID_COLUMN_ID;
    }
    return DynoConfigValue(m_dataset, id, defaultValue);
}


} // namespace metrics_plus_plus
