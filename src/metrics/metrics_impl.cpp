/**********************************************
   File:   metrics_impl.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License")
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "metrics/metrics_impl.h"
#include "metrics/metrics.h"
#include "metrics/parser.h"
#include "utils/file_io.h"
#include "utils/timer.h"
#include "utils/log.h"
#include <boost/lexical_cast.hpp>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>

#define CHECK_ZERO_PTR(p) if (p == NULL) throw std::runtime_error("Invalid argument")

using namespace cdb;
using namespace std;

namespace metrics_plus_plus {

/*********************************************
 *   MetricsFactory
 */
// static
MetricsFactory* MetricsFactory::create(const char* path)
{
    CHECK_ZERO_PTR(path);

    MetricsFactoryImpl* impl = new MetricsFactoryImpl;
    try {
        impl->init(path);
    }
    catch(...) {
        delete impl;
        throw;
    }
    return impl;
}


/*********************************************
 *   MetricsFactoryImpl
 */

MetricsFactoryImpl::MetricsFactoryImpl()
  : m_backgroundThread(20, NULL),
    m_dataset(NULL)
{
    m_backgroundThread.setTimerHandler(this);
}

MetricsFactoryImpl::~MetricsFactoryImpl()
{
    stop();
    delete m_dataset;
}

void MetricsFactoryImpl::init(const char* path)
{
    if (!FileIo::exists(path)) {
        CwseDatasetConfig cfg = Dataset::getDefaultDatasetConfig();
        Dataset::create(path, &cfg);
    }
    m_dataset = Dataset::open(path, CWSE_DATA_ACCESS_READ_WRITE);
}

void MetricsFactoryImpl::start()
{
    m_backgroundThread.start();
}

void MetricsFactoryImpl::stop()
{
    m_backgroundThread.stop();
}

void MetricsFactoryImpl::process(const char* str)
{
    CHECK_ZERO_PTR(str);
    LOG_DEBUG << "Process: " << str;
    parse_metrics_ddl(str, *this);
}

void MetricsFactoryImpl::checkMetricExists(const char* name, BaseMetric::Type type)
{
    void* ptr;
    if (m_table.get(name, &ptr)) {
        BaseMetric* baseMetric = static_cast<BaseMetric*>(ptr);
        if (baseMetric->type() == type) {
            return;
        }
        throw std::runtime_error("Metric with another type and same name already exists");
    }
}

uint16_t MetricsFactoryImpl::createDatasetColumn(const char* name)
{
    CwseColumnConfig cfg = Dataset::getDefaultColumnConfig();
    m_dataset->addColumn(name, &cfg, true);
    uint16_t id = m_dataset->findColumn(name);
    if (id == CWSE_INVALID_COLUMN_ID) {
        throw std::runtime_error("Failed to find column");
    }
    m_dataset->set(0.0, id);
    return id;
}

void MetricsFactoryImpl::createGauge(const char* name)
{
    CHECK_ZERO_PTR(name);

    checkMetricExists(name, BaseMetric::GAUGE);
    uint16_t id = createDatasetColumn(name);

    Gauge* gauge = new Gauge(m_dataset, id);
    m_table.put(name, gauge);
}

void MetricsFactoryImpl::createCounter(const char* name)
{
    CHECK_ZERO_PTR(name);

    checkMetricExists(name, BaseMetric::COUNTER);
    uint16_t id = createDatasetColumn(name);

    Counter* counter = new Counter(m_dataset, id);
    m_table.put(name, counter);
}

void MetricsFactoryImpl::createMeter(const char* name, int timeWindow, int collectPeriod, int ratePeriod)
{
    CHECK_ZERO_PTR(name);

    checkMetricExists(name, BaseMetric::METER);
    uint16_t id = createDatasetColumn(name);

    Meter* meter = new Meter(m_dataset, id, timeWindow, collectPeriod, ratePeriod);
    meter->init();
    m_table.put(name, meter);
    m_timedMetrics.add(meter);
}

void MetricsFactoryImpl::createStats(const char* name, int timeWindow, int collectPeriod)
{
    CHECK_ZERO_PTR(name);

    checkMetricExists(name, BaseMetric::STATS);

    StatsColumns cols;
    std::string base = name;
    cols.m_sumId = createDatasetColumn((base + ".sum").c_str());
    cols.m_countId = createDatasetColumn((base + ".count").c_str());
    cols.m_maxId = createDatasetColumn((base + ".max").c_str());
    cols.m_minId = createDatasetColumn((base + ".min").c_str());
    cols.m_avgId = createDatasetColumn((base + ".avg").c_str());
    cols.m_sdevId = createDatasetColumn((base + ".stddev").c_str());

    Stats* stats = new Stats(m_dataset, cols, timeWindow, collectPeriod);
    stats->init();
    m_table.put(name, stats);
    m_timedMetrics.add(stats);
}

void MetricsFactoryImpl::createPercentiles(const char* name, int timeWindow, int collectPeriod,
                               const double* percentiles, int count,
                               int subsetSize)
{
    CHECK_ZERO_PTR(name);
    CHECK_ZERO_PTR(percentiles);
    if (count <= 0) {
        throw std::runtime_error("Invalid argument");
    }

    vector<double> perc;
    perc.reserve(count);
    for (int i=0; i < count; i++) {
        if (percentiles[i] < 0 || percentiles[i] > 100) {
            throw std::runtime_error("Invalid percentiles");
        }
        perc.push_back(percentiles[i]);
    }
    std::sort(perc.begin(), perc.end());

    vector<uint16_t> ids;
    ids.reserve(count);
    for (int i=0; i < count; i++) {
        if (i > 0 && perc[i] == perc[i-1]) {
            throw std::runtime_error("Invalid percentiles");
        }
        ostringstream ostr;
        ostr << name << "." << perc[i];
        uint16_t id = createDatasetColumn(ostr.str().c_str());
        ids.push_back(id);
    }

    Percentiles* p = new Percentiles(m_dataset, subsetSize, perc, ids, timeWindow, collectPeriod);
    p->init();
    m_table.put(name, p);
    m_timedMetrics.add(p);
}

void MetricsFactoryImpl::createHistogram(const char* name, int timeWindow, int collectPeriod,
                                         const BucketDescr* buckets, int count)
{
    CHECK_ZERO_PTR(name);
    CHECK_ZERO_PTR(buckets);
    if (count <= 0) {
        throw std::runtime_error("Invalid argument");
    }

    size_t bucketsCount = 0;
    for (int i=0; i < count; i++) {
        bucketsCount += buckets[i].count;
    }

    vector<double> edges;
    edges.reserve(bucketsCount);
    double edge = 0.0;
    for (int i=0; i < count; i++) {
        for (int j=0; j < buckets[i].count; j++) {
            edge += buckets[i].size;
            edges.push_back(edge);
        }
    }

    vector<uint16_t> ids;
    ids.reserve(bucketsCount+1);
    for (size_t i=0; i < bucketsCount+1; i++) {
        string base = name;
        if (i < bucketsCount) {
            base += "." + boost::lexical_cast<string>(edges[i]);
        }
        else {
            base += ".inf";
        }
        uint16_t id = createDatasetColumn(base.c_str());
        ids.push_back(id);
    }

    Histogram* histogram = new Histogram(m_dataset, edges, ids, timeWindow, collectPeriod);
    histogram->init();
    m_table.put(name, histogram);
    m_timedMetrics.add(histogram);
}

Metric* MetricsFactoryImpl::find(const char* name)
{
    CHECK_ZERO_PTR(name);
    void* ptr;
    if (m_table.get(name, &ptr)) {
        return static_cast<Metric*>(ptr);
    }
    return NULL;
}

Metric& MetricsFactoryImpl::get(const char* name)
{
    CHECK_ZERO_PTR(name);
    void* ptr;
    if (m_table.get(name, &ptr)) {
        Metric* result = static_cast<Metric*>(ptr);
        return *result;
    }

    m_missing.add(name);

    return m_dummy;
}

void MetricsFactoryImpl::linkMetrics(const char* from, const char* to)
{
    Metric* fromMetric = find(from);
    Metric* toMetric = find(to);
    if (fromMetric == NULL) {
        throw std::runtime_error(std::string("Failed to find metric ") + from);
    }
    if (toMetric == NULL) {
        throw std::runtime_error(std::string("Failed to find metric ") + to);
    }

    BaseMetric* fromMetricBase = dynamic_cast<BaseMetric*>(fromMetric);
    BaseMetric* toMetricBase = dynamic_cast<BaseMetric*>(toMetric);

    fromMetricBase->link(toMetricBase);
}

void MetricsFactoryImpl::dumpMissingMetrics(const char* path)
{
    std::ofstream ofs;
    if (path != NULL) {
        ofs.open(path);
    }
    ConcurrentList<std::string>::Iterator iter = m_missing.getIterator();
    while (iter.hasNext()) {
        if (path == NULL) {
            cout << iter.value() << endl;
        }
        else {
            ofs << iter.value() << endl;
        }
        iter.next();
    }
 }

void MetricsFactoryImpl::backgroundProcess()
{
    uint64_t ts = Timer::getCurrentTimestampMs();
    ts /= 1000;

    TimedMetrics::Iterator iter = m_timedMetrics.getIterator();
    while (iter.hasNext()) {
        iter.value()->onTimeTick(ts);
        iter.next();
    }
}

} // namespace metrics_plus_plus
