/**********************************************
   File:   stopwatch.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "metrics_plus_plus.h"
#ifdef WIN32
#include <Windows.h>
#else
#include <time.h>
#endif

namespace metrics_plus_plus {

#ifdef WIN32

void Stopwatch::start()
{
    LARGE_INTEGER t;
    QueryPerformanceCounter(&t);
    m_high = t.HighPart;
    m_low = t.LowPart;
}

int64_t Stopwatch::stop()
{
    LARGE_INTEGER start, finish, elapsed, freq;

    start.HighPart = (LONG) m_high;
    start.LowPart = (DWORD) m_low;

    QueryPerformanceFrequency(&freq);
    QueryPerformanceCounter(&finish);

    elapsed.QuadPart = finish.QuadPart - start.QuadPart;

    elapsed.QuadPart *= 1000;
    elapsed.QuadPart /= freq.QuadPart;

    return elapsed.QuadPart;
}

#else

void Stopwatch::start()
{
    timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    m_high = t.tv_sec;
    m_low = t.tv_nsec;
}

int64_t Stopwatch::stop()
{
    timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);

    int64_t sec, nsec;

    if (t.tv_nsec-m_low < 0) {
        sec = t.tv_sec - m_high -1;
        nsec = 1000000000 + t.tv_nsec - m_low;
    }
    else {
        sec = t.tv_sec - m_high;
        nsec = t.tv_nsec - m_low;
    }

    return sec * 1000 + nsec / 1000000;
}

#endif

} //  namespace metrics_plus_plus
