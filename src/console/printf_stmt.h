/**********************************************
   File:   printf_stmt.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_CDBCON_PRINTF_STMT_H
#define CDB_CDBCON_PRINTF_STMT_H

#include "clockwork.h"
#include <string>
#include <vector>

class PrintfStmt
{
public:
    PrintfStmt() : m_dataset(NULL) {}
    bool init(const std::string& str, cdb::Dataset* dataset);
    void exec();

private:
    struct PrintFormat
    {
        enum Type { Float, Int, Time };

        Type        m_type;
        std::string m_format;
        bool        m_local;
    };

private:
    cdb::Dataset*            m_dataset;
    std::string              m_text;
    std::vector<std::string> m_texts;
    std::vector<PrintFormat> m_formats;
    std::vector<uint16_t>    m_vals;

private:
    void splitCommaDelimitedString(std::vector<std::string>& tokens, const std::string& str);
    bool parseFormat(const std::string& format, PrintfStmt::PrintFormat& result);

};

#endif // CDB_CDBCON_PRINTF_STMT_H

