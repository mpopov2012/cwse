/**********************************************
   File:   console_handler.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN   
#include <windows.h>
#include <tchar.h>
#endif

#include <iostream>
#include <signal.h>

extern bool g_keepPrinting;

#ifdef WIN32

static BOOL WINAPI ConsoleHandler(DWORD CEvent)
{
    switch(CEvent)
    {
        case CTRL_C_EVENT:
        case CTRL_BREAK_EVENT:
        case CTRL_CLOSE_EVENT:
        case CTRL_LOGOFF_EVENT:
        case CTRL_SHUTDOWN_EVENT:
            g_keepPrinting = false;
            break;
    }
    return TRUE;
}

void startConsoleHandler()
{
    SetConsoleCtrlHandler((PHANDLER_ROUTINE)ConsoleHandler,TRUE);
}

void stopConsoleHandler()
{
    SetConsoleCtrlHandler((PHANDLER_ROUTINE)ConsoleHandler,FALSE);
}

#else

static void sighandler(int)
{
	g_keepPrinting = false;
}

void startConsoleHandler()
{
    signal(SIGINT, &sighandler);
}

void stopConsoleHandler()
{
    signal(SIGINT, SIG_DFL);
}

#endif
