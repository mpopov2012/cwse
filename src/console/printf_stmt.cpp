/**********************************************
   File:   printf_stmt.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifdef WIN32
// Turning off warning generated for localtime usage
#pragma warning( disable : 4996 )
#endif

#include "console/printf_stmt.h"
#include "clockwork_errors.h"
#include "utils/string_utils.h"
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <stdio.h>

using std::vector;
using std::string;
using std::cerr;
using std::cout;
using std::endl;

void PrintfStmt::splitCommaDelimitedString(vector<string>& tokens, const string& str)
{
    vector< string > t;
    cdb::StringUtils::split(str, ",", t);
    for (size_t i=0; i < t.size(); i++) {
        cdb::StringUtils::trim(t[i]);
        tokens.push_back(t[i]);
    }
}

bool PrintfStmt::parseFormat(const string& format, PrintfStmt::PrintFormat& result)
{
    result.m_type = PrintFormat::Float;
    result.m_format = "%.2lf";

    if (format.empty()) return true;

    if (format == "t") {
        result.m_type = PrintFormat::Time;
        result.m_format = "%H:%M:%S";
        result.m_local = true;
        return true;
    }

    if (format == "g") {
        result.m_type = PrintFormat::Time;
        result.m_format = "%H:%M:%S";
        result.m_local = false;
        return true;
    }

    size_t pos = format.find(':');
    if (pos == string::npos) {
        result.m_format = string("%") + format + "lf";
        return true;
    }

    if (pos != 1) {
        cerr << "Invalid printf command: corrupted format [" << format << "]" << endl;
        return false;
    }

    if (format[0] == 'd') {
        result.m_type = PrintFormat::Int;
        result.m_format = string("%") + format.substr(2) + "ld";
    }
    else if (format[0] == 'f') {
        result.m_type = PrintFormat::Float;
        result.m_format = string("%") + format.substr(2) + "lf";
    }
    else if (format[0] == 'e') {
        result.m_type = PrintFormat::Float;
        result.m_format = string("%") + format.substr(2) + "e";
    }
    else if (format[0] == 't') {
        result.m_type = PrintFormat::Time;
        result.m_format = format.substr(2);
        result.m_local = true;
    }
    else if (format[0] == 'g') {
        result.m_type = PrintFormat::Time;
        result.m_format = format.substr(2);
        result.m_local = false;
    }
    else {
        cerr << "Invalid printf command: corrupted format [ " << format << "]" << endl;
        return false;
    }

    return true;
}

bool PrintfStmt::init(const std::string& str, cdb::Dataset* dataset)
{
    m_dataset = dataset;

    size_t openQuote = str.find('"');
    if (openQuote == string::npos) {
        cerr << "Invalid printf command: missing format string" << endl;
        return false;
    }

    string s = str.substr(openQuote+1);
    size_t closeQuote = s.find('"');
    if (closeQuote == string::npos) {
        cerr << "Invalid printf command: bad format string" << endl;
        return false;
    }

    string format = s.substr(0, closeQuote);
    string values = s.substr(closeQuote);

    cdb::StringUtils::trim(values);
    if (values == "\"") {
        m_text = format;
        return true;
    }

    vector< string > tokens;
    splitCommaDelimitedString(tokens, values);
    if (tokens.size() < 2) {
        cerr << "Invalid printf command: missing values" << endl;
        return false;
    }
    if (tokens[0] != "\"") {
        cerr << "Invalid printf command: corrupted values" << endl;
        return false;
    }
    vector<string> temp;
    for (size_t i=1; i < tokens.size(); i++) temp.push_back(tokens[i]);
    tokens.swap(temp);

    size_t lastPos = 0;
    for (size_t i=0; i < tokens.size(); i++) {
        size_t openCurly = format.find('{', lastPos);
        if (openCurly == string::npos) {
            cerr << "Invalid printf command: no openning curles" << endl;
            return false;
        }

        size_t closeCurly = format.find('}', openCurly+1);
        if (closeCurly == string::npos) {
            cerr << "Invalid printf command: no closing curls" << endl;
            return false;
        }

        string itemFormat = format.substr(openCurly+1, closeCurly-openCurly-1);
        cdb::StringUtils::trim(itemFormat);
        PrintFormat printFormat;
        if (!parseFormat(itemFormat, printFormat)) {
            return false;
        }
        m_formats.push_back(printFormat);

        string s = format.substr(lastPos, openCurly-lastPos);

        m_texts.push_back(s);
        if (printFormat.m_type == PrintFormat::Time) {
            if (tokens[i] != "$now") {
                cerr << "Invalid printf command: expected $now" << endl;
                return false;
            }
            m_vals.push_back(0);
        }
        else {
            uint16_t id = m_dataset->findColumn(tokens[i].c_str());
            if (CWSE_INVALID_COLUMN_ID == id) {
                cerr << "Invalid printf command: column " << tokens[i] << " not found" << endl;
                return false;
            }
            m_vals.push_back(id);
        }

        lastPos = closeCurly + 1;
    }

    if (lastPos < format.length()) {
        m_texts.push_back(format.substr(lastPos));
    }

    return true;
}

void PrintfStmt::exec()
{
    cout.flush();

    if (!m_text.empty()) {
        cout << m_text << endl;
        return;
    }

    for (size_t i=0; i < m_vals.size(); i++) {
        printf("%s", m_texts[i].c_str());

        if (m_formats[i].m_type == PrintFormat::Time) {

            time_t t = time(NULL);
            char buffer[256];
            if (m_formats[i].m_local) {
                strftime(buffer, sizeof(buffer), m_formats[i].m_format.c_str(), localtime(&t));
            }
            else {
                strftime(buffer, sizeof(buffer), m_formats[i].m_format.c_str(), gmtime(&t));
            }
            printf("%s", buffer);
        }
        else {
            double d = m_dataset->get(m_vals[i]);
            if (m_formats[i].m_type == PrintFormat::Float) {
                printf(m_formats[i].m_format.c_str(), d);
            }
            else {
                int64_t temp = int64_t(d);
                printf(m_formats[i].m_format.c_str(), temp);
            }
        }
    }

    if (m_texts.size() > m_vals.size()) {
        printf("%s", m_texts.back().c_str());
    }

    printf("\n");
    fflush(stdout);
}

