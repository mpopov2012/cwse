/**********************************************
   File:   main.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "console/printf_stmt.h"
#include "clockwork_errors.h"
#include "clockwork.h"
#include "utils/config.h"
#include "utils/log.h"
#include "utils/timer.h"
#include "utils/string_utils.h"
#include <boost/lexical_cast.hpp>
#ifndef WIN32
#include <readline/readline.h>
#include <readline/history.h>
#endif
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <stdarg.h>
#include <stdio.h>

using namespace cdb;
using namespace std;

#define VERSION     "0.0.1"

bool g_keepPrinting;
void startConsoleHandler();
void stopConsoleHandler();

static cdb::Dataset* g_dataset = NULL;
static bool g_keepGoing = true;
static bool g_silent = false;

static void setLogLevel(cdb::Config& config);
static bool openDataset(cdb::Config& config);
static void execCommand(string str);
static void printValue(const string& str);
static void cmdAddColumn(vector<string>& tokens);
static void cmdCreateDataset(vector<string>& tokens);
static void cmdOpenDataset(vector<string>& tokens);
static void cmdSetColumn(vector<string>& tokens);
static void cmdIncColumn(vector<string>& tokens);
static void cmdListColumns(vector<string>& tokens);
static void cmdPrintColumns(vector<string>& tokens);
static void cmdPrintf(vector<string>& tokens, const string& str);
static void cmdShell(vector<string>& tokens);
static void cmdClose();
static void cmdHelp();
static void cmdVersion();
static void cmdForeach(vector<string>& tokens, const string& str);
static void parseWithClause(vector<string>& tokens, size_t from, Config& conf);

#define DATASET_CHECK \
    if (g_dataset == NULL) { \
        cerr << "Dataset is not opened" << endl; \
        return; \
    }

#ifdef WIN32

#define STRDUP _strdup

char* readline(const char* g_prompt)
{
    char buffer[512];
    printf("%s", g_prompt);
    fflush(stdout);
    char* s = fgets(buffer, sizeof(buffer), stdin);
    for (char* t = s; *t; ++t) {
        if ((*t == '\r') || (*t == '\n')) *t = '\0';
    }
    return _strdup(s);
}

void add_history(char* s) {}

#else

#define STRDUP strdup

#endif

int main(int argc, char* argv[])
{
    Config config;
    config.process((const char**) argv+1, argc-1);

    setLogLevel(config);
    g_silent = config.find("silent");
    if (!openDataset(config)) return -1;

    string execStr;
    if (config.find("exec", execStr)) {
        try {
            execCommand(execStr);
        }
        catch (CwException& e) {
            cerr << "FAILED: " << e.what() << " (" << e.file << ":" << e.line << ")" << endl;
            return -1;
        }
        return 0;
    }

    if (!g_silent) {
        cout << "Clockwork Storage Engine console ver0.1" << endl;
    }

    while (g_keepGoing) {
        char* s = NULL;
        try {
            if (g_silent) {
                if (!cin.good()) break;
                string str;
                getline(cin, str);
                s = STRDUP(str.c_str());
            }
            else {
                s = readline("=> ");
            }
            if (s == 0) break;

            if (*s == '\0') {
                free(s);
                continue;
            }

            add_history(s);
            execCommand(s);
            free(s);
        }
        catch(CwException& e) {
            free(s);
            cerr << "FAILED: " << e.what() << " (" << e.file << ":" << e.line << ")" << endl;
        }
    }
    return 0;
}

static void setLogLevel(cdb::Config& config)
{
    int level = CWSE_LOG_LEVEL_MUTE;
    string str;
    if (config.find("log", str)) {
        cdb::StringUtils::toUpper(str);
        if (str == "WARN") level = CWSE_LOG_LEVEL_WARN;
        else if (str == "ERROR") level = CWSE_LOG_LEVEL_WARN;
        else if (str == "TRACE") level = CWSE_LOG_LEVEL_TRACE;
        else cout << "Invalid log setting: " << str << endl;
    }
    Dataset::setLogging(level);
}

static void execCommand(string str)
{
    cdb::StringUtils::trim(str);
    if (str.length() == 0) return;
    if (str[0] == '#') return;

    if (str == "quit" || str == "exit") {
        g_keepGoing = false;
        return;
    }

    vector< string > tokens;
    cdb::StringUtils::split(str, " ", tokens);

    if (tokens.size() < 1) {
        cerr << "Invalid command" << endl;
        return;
    }

    if (tokens[0] == "create") {
        cmdCreateDataset(tokens);
    }
    else if (tokens[0] == "open") {
        cmdOpenDataset(tokens);
    }
    else if (tokens[0] == "add") {
        cmdAddColumn(tokens);
    }
    else if (tokens[0] == "set") {
        cmdSetColumn(tokens);
    }
    else if (tokens[0] == "list") {
        cmdListColumns(tokens);
    }
    else if (tokens[0] == "print") {
        cmdPrintColumns(tokens);
    }
    else if (tokens[0] == "help") {
        cmdHelp();
    }
    else if (tokens[0] == "version") {
        cmdVersion();
    }
    else if (tokens[0] == "close") {
        cmdClose();
    }
    else if (tokens[0] == "shell") {
        cmdShell(tokens);
    }
    else if (tokens[0] == "printf") {
        cmdPrintf(tokens, str);
    }
    else if (tokens[0] == "foreach") {
        cmdForeach(tokens, str);
    }
    else {
        cerr << "Unknown command: " << tokens[0] << endl;
        return;
    }
}

static void cmdVersion()
{
    cout << VERSION << endl;
}

static void cmdHelp()
{
    cout << "create <path> [with reserve=<number>]" << endl;
    cout << "open <path> [ with access=<readwrite|readonly>]" << endl;
    cout << "add <name> [with type=<double|long>]" << endl;
    cout << "list <pattern>" << endl;
    cout << "set <name>=<value>" << endl;
    cout << "set <name>+=<value>" << endl;
    cout << "print <pattern>*" << endl;
    cout << "printf \"{}\" <name>" << endl;
    cout << "close" << endl;
    cout << endl;
}

static bool openDataset(cdb::Config& config)
{
    string path;
    if (!config.find("path", path)) {
        return true;
    }

    int access;
    string accessStr = "readonly";
    config.find("access", accessStr);
    if (accessStr == "readonly") access = CWSE_DATA_ACCESS_READ_ONLY;
    else if (accessStr == "readwrite") access = CWSE_DATA_ACCESS_READ_WRITE;
    else {
        cerr << "Invalid access argument value" << endl;
        return false;
    }

    if (g_dataset != NULL) {
        delete g_dataset;
        g_dataset = NULL;
    }

    try {
        g_dataset = Dataset::open(path.c_str(), access);
    }
    catch (CwException& e) {
        cerr << "FAILED: " << e.what() << " (" << e.file << ":" << e.line << ")" << endl;
        return false;
    }

    return true;
}

// create xyz with reserve=128
static void cmdCreateDataset(vector<string>& tokens)
{
    if (tokens.size() < 2) {
        cerr << "Invalid command CREATE" << endl;
        return;
    }

    Config conf;
    parseWithClause(tokens, 2, conf);

    CwseDatasetConfig datasetConfig = Dataset::getDefaultDatasetConfig();

    string reserve;
    if (conf.find("reserve", reserve)) {
        datasetConfig.metricsMaxCount = boost::lexical_cast<uint64_t>(reserve);
    }

    Dataset::create(tokens[1].c_str(), &datasetConfig);

    cout << "Dataset created" << endl;
}

// open xyz with access=readwrite
static void cmdOpenDataset(vector<string>& tokens)
{
    if (tokens.size() < 2) {
        cerr << "Invalid command OPEN" << endl;
        return;
    }

    Config conf;
    parseWithClause(tokens, 2, conf);
    conf.add("path", tokens[1]);

    bool b = openDataset(conf);
    if (b) {
        cout << "Dataset opened" << endl;
    }
}

static void cmdClose()
{
    DATASET_CHECK;

    delete g_dataset;
    g_dataset = NULL;
    cout << "Dataset closed" << endl;
}

// add abc with type=long
static void cmdAddColumn(vector<string>& tokens)
{
    DATASET_CHECK;

    if (tokens.size() < 2) {
        cerr << "Invalid command ADD" << endl;
        return;
    }

    Config conf;
    parseWithClause(tokens, 2, conf);

    CwseColumnConfig columnConfig = Dataset::getDefaultColumnConfig();

    string typeStr;
    if (conf.find("type", typeStr)) {
        if (typeStr == "double") columnConfig.type = CWSE_TYPE_DOUBLE;
        else if (typeStr == "long") columnConfig.type = CWSE_TYPE_INT64;
        else {
            cerr << "Invalid CREATE COLUMN command: invalide type" << endl;
            return;
        }
    }

    g_dataset->addColumn(tokens[1].c_str(), &columnConfig, true);
    cout << "Column added" << endl;
}

static void parseWithClause(vector<string>& tokens, size_t from, Config& conf)
{
    if (tokens.size() > from) {
        if (tokens.size() < from + 2) {
            cerr << "Invalid command" << endl;
            return;
        }
        if (tokens[from] != "with") {
            cerr << "Invalid command" << endl;
            return;
        }
        for (size_t i=from+1; i < tokens.size(); i++) {
            conf.process(tokens[i]);
        }
    }
}

// set abc=123
static void cmdSetColumn(vector<string>& tokens)
{
    DATASET_CHECK;

    if (tokens.size() != 2) {
        cerr << "Invalid command SET: invalid number of tokens" << endl;
        return;
    }

    size_t pos = tokens[1].find("+=");
    if (pos != string::npos) {
        cmdIncColumn(tokens);
        return;
    }

    pos = tokens[1].find('=');
    if (pos == string::npos) {
        cerr << "Invalid command SET: symbol = not found" << endl;
        return;
    }

    string name = tokens[1].substr(0, pos);
    string value = tokens[1].substr(pos+1);

    uint16_t columnID = g_dataset->findColumn(name.c_str());
    if (columnID == CWSE_INVALID_COLUMN_ID) {
        cerr << "Column " << name << " is not found" << endl;
        return;
    }

    double d;
    try {
        d = boost::lexical_cast<double>(value);
    }
    catch (...) {
        cerr << "Faild to conver " << value << " to double" << endl;
        return;
    }

    g_dataset->set(d, columnID);
}

// inc abc+=2
static void cmdIncColumn(vector<string>& tokens)
{
    DATASET_CHECK;

    if (tokens.size() != 2) {
        cerr << "Invalid command SET: invalid number of tokens" << endl;
        return;
    }

    size_t pos = tokens[1].find("+=");
    if (pos == string::npos) {
        cerr << "Invalid command SET: symbol += not found" << endl;
        return;
    }

    string name = tokens[1].substr(0, pos);
    string value = tokens[1].substr(pos+2);

    uint16_t columnID = g_dataset->findColumn(name.c_str());
    if (columnID == CWSE_INVALID_COLUMN_ID) {
        cerr << "Column " << name << " is not found" << endl;
        return;
    }

    double d;
    try {
        d = boost::lexical_cast<double>(value);
    }
    catch (...) {
        cerr << "Faild to conver " << value << " to double" << endl;
        return;
    }

    g_dataset->inc(d, columnID);
}

// list [pattern]
static void cmdListColumns(vector<string>& tokens)
{
    DATASET_CHECK;

    if (tokens.size() != 1 && tokens.size() != 2) {
        cerr << "Invalid command LIST: invalid number of tokens" << endl;
        return;
    }

    Names* names = g_dataset->getNames(tokens.size() == 2 ? tokens[1].c_str() : NULL);
    try {
        string str;
        while (names->next(str)) {
            cout << str << endl;
        }
    }
    catch (CwException& ex) {
        delete names;
        cerr << ex.what() << endl;
    }
    delete names;
}

// print [pattern]
static void cmdPrintColumns(vector<string>& tokens)
{
    DATASET_CHECK;

    if (tokens.size() != 2) {
        cerr << "Invalid command PRINT: invalid number of tokens" << endl;
        return;
    }

    Names* names = g_dataset->getNames(tokens[1].c_str());
    try {
        string str;
        while (names->next(str)) {
            printValue(str);
        }
    }
    catch (CwException& ex) {
        delete names;
        cerr << ex.what() << endl;
    }
    delete names;
}

static void printValue(const string& str)
{
    DATASET_CHECK;

    uint16_t columnID = g_dataset->findColumn(str.c_str());
    if (columnID == CWSE_INVALID_COLUMN_ID) {
        cerr << "Column " << str << " is not found" << endl;
        return;
    }

    double d = g_dataset->get(columnID);

    CwseColumnConfig columnConfig = g_dataset->getColumnConfig(str.c_str());
    if (columnConfig.type == CWSE_TYPE_DOUBLE) {
        cout << str << "=" << setiosflags(ios::left) << setprecision(4) << d << endl;
    }
    else {
        cout << str << "=" << setiosflags(ios::left) << (int64_t) d << endl;
    }
}

static void cmdShell(vector<string>& tokens)
{
#ifdef WIN32

#else
    string str = "";
    for (size_t i=1; i < tokens.size(); i++) {
        str += tokens[i] + " ";
    }
    if (0 == system(str.c_str())) {} // eliminating warning about unused return value
#endif
}

// printf "format", arg1, arg2, ...
static void cmdPrintf(vector<string>& tokens, const string& str)
{
    DATASET_CHECK;

    if (tokens.size() < 2) {
        cerr << "Invalid printf command: missing format string" << endl;
        return;
    }

    if (tokens[1].length() < 3 || tokens[1][0] != '"') {
        cerr << "Invalid printf command: missing format string" << endl;
        return;
    }

    PrintfStmt stmt;
    if (!stmt.init(str, g_dataset)) {
        return;
    }

    stmt.exec();
}

// foreach N sec printf "format", arg1, arg2, ...
static void cmdForeach(vector<string>& tokens, const string& str)
{
    DATASET_CHECK;

    if (tokens.size() < 5) {
        cerr << "Invalid foreach command: missing tokens" << endl;
        return;
    }

    uint16_t n;
    try {
        n = boost::lexical_cast<uint16_t>(tokens[1]);
    }
    catch (...) {
        cerr << "Invalid foreach command: bad number" << endl;
        return;
    }
    if (n == 0) {
        cerr << "Invalid foreach command: number cannot be 0" << endl;
        return;
    }

    if (tokens[2] != "sec") {
        cerr << "Invalid foreach command: expected sec" << endl;
        return;
    }

    if (tokens[3] != "printf") {
        cerr << "Invalid foreach command: expected printf" << endl;
        return;
    }

    size_t pos = str.find("printf");
    if (pos == string::npos) {
        cerr << "Invalid foreach command: expected printf" << endl;
        return;
    }

    PrintfStmt stmt;
    if (!stmt.init(str.substr(pos), g_dataset)) {
        return;
    }

    g_keepPrinting = true;
    startConsoleHandler();
    while (g_keepPrinting) {
        stmt.exec();
        Timer::sleepSec(n);
    }
    stopConsoleHandler();
}

