/**********************************************
   File:   clockwork_cface.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "clockwork_cface.h"
#include "clockwork.h"
#include "clockwork_errors.h"
#include "string.h"

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include "Windows.h"
#define STRNCPY(dest,src,sz) strcpy_s(dest,sz,src)
#else
#define STRNCPY(dest,src,sz) strncpy(dest,src,sz)
#endif

#define CLEAR_ERROR(error) { if (error != NULL) { error->last_error = 0; error->msg[0] = '\0'; } }
#define CHECK_HANDLE(handle, error) \
    { \
    	if (handle == NULL) { \
    		if (error != NULL) { \
    			error->last_error = CWSE_ERROR_INVALID_ARG; \
    			STRNCPY(error->msg, "Invalid handle", sizeof(error->msg)); \
    		} \
    		return; \
    	} \
    }
#define CHECK_HANDLE_RET(handle, error) \
    { \
    	if (handle == NULL) { \
    		if (error != NULL) { \
    			error->last_error = CWSE_ERROR_INVALID_ARG; \
    			STRNCPY(error->msg, "Invalid handle", sizeof(error->msg)); \
    		} \
    		return result; \
    	} \
    }
#define SET_CWSE_EXCEPTION(ex, error) { \
	if (error != NULL) { \
		error->last_error = ex.errorID; \
	    STRNCPY(error->msg, ex.what(), sizeof(error->msg)); \
	} \
	return; \
}
#define SET_CWSE_EXCEPTION_RET(ex, error) { \
    if (error != NULL) { \
        error->last_error = ex.errorID; \
        STRNCPY(error->msg, ex.what(), sizeof(error->msg)); \
    } \
    return result; \
}
#define SET_STD_EXCEPTION(ex, error) { \
    if (error != NULL) { \
        error->last_error = CWSE_ERROR_UNKNOWN; \
        STRNCPY(error->msg, ex.what(), sizeof(error->msg)); \
    } \
    return; \
}
#define SET_STD_EXCEPTION_RET(ex, error) { \
    if (error != NULL) { \
        error->last_error = CWSE_ERROR_UNKNOWN; \
        STRNCPY(error->msg, ex.what(), sizeof(error->msg)); \
    } \
    return result; \
}
#define SET_UNDEFINED_EXCEPTION(ex, error) { \
    if (error != NULL) { \
        error->last_error = CWSE_ERROR_UNKNOWN; \
        STRNCPY(error->msg, "Undefined error", sizeof(error->msg)); \
    } \
    return; \
}
#define SET_UNDEFINED_EXCEPTION_RET(ex, error) { \
    if (error != NULL) { \
        error->last_error = CWSE_ERROR_UNKNOWN; \
        STRNCPY(error->msg, "Undefined error", sizeof(error->msg)); \
    } \
    return result; \
}

#define CATCH_EXCEPTIONS \
    catch (const cdb::CwException& ex) { \
        SET_CWSE_EXCEPTION(ex, error); \
    } \
    catch (const std::exception& ex) { \
        SET_STD_EXCEPTION(ex, error); \
    } \
    catch (...) { \
        SET_UNDEFINED_EXCEPTION(ex, error); \
    }

#define CATCH_EXCEPTIONS_RET \
    catch (const cdb::CwException& ex) { \
        SET_CWSE_EXCEPTION_RET(ex, error); \
    } \
    catch (const std::exception& ex) { \
        SET_STD_EXCEPTION_RET(ex, error); \
    } \
    catch (...) { \
        SET_UNDEFINED_EXCEPTION_RET(ex, error); \
    }

DllExport
struct CwseDatasetConfig cwse_get_default_dataset_config(struct CwseError* error)
{
    CwseDatasetConfig result;
    result.metricsMaxCount = 0;

    CLEAR_ERROR(error);

	try {
		result = cdb::Dataset::getDefaultDatasetConfig();
	}
	CATCH_EXCEPTIONS_RET;

	return result;
}

#if 0

DllExport
void cwse_set_logging(int level, CwseLogOutputFunc output, struct CwseError* error)
{
	CLEAR_ERROR(error);

    try {
        cdb::Dataset::setLogging(level, output);
    }
    CATCH_EXCEPTIONS;
}

DllExport
void cwse_create(const char* path, const struct CwseDatasetConfig* cfg, struct CwseError* error)
{
	CLEAR_ERROR(error);

    try {
        cdb::Dataset::create(path, cfg);
    }
    CATCH_EXCEPTIONS;
}

DllExport
void* cwse_open(const char* path, int dataAccess, struct CwseError* error)
{
	void* result = NULL;
	CLEAR_ERROR(error);

    try {
        result = cdb::Dataset::open(path, dataAccess);
    }
    CATCH_EXCEPTIONS_RET;

    return result;
}

DllExport 
void cwse_release_dataset(void* handle, struct CwseError* error)
{
	CHECK_HANDLE(handle, error);
	CLEAR_ERROR(error);
    cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    delete self;
}

DllExport
struct CwseDatasetConfig cwse_get_dataset_config(void* handle, struct CwseError* error)
{
	CwseDatasetConfig result = { 0 };
	CHECK_HANDLE_RET(handle, error);
	CLEAR_ERROR(error);

	cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        result = self->getDatasetConfig();
    }
    CATCH_EXCEPTIONS_RET;

	return result;
}

DllExport
void cwse_alter_dataset(void* handle, const struct CwseDatasetConfig* cfg, struct CwseError* error)
{
	CHECK_HANDLE(handle, error);
	CLEAR_ERROR(error);

    cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        self->alterDataset(cfg);
    }
    CATCH_EXCEPTIONS;
}

DllExport
CwseMetricID cwse_add_metric(void* handle, const char* name, struct CwseError* error)
{
	CwseMetricID result = CWSE_INVALID_METRIC_ID;
	CHECK_HANDLE_RET(handle, error);
	CLEAR_ERROR(error);

    cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        result = self->addMetric(name);
    }
    CATCH_EXCEPTIONS_RET;

	return result;
}

DllExport
void cwse_delete_metric(void* handle, const char* name, struct CwseError* error)
{
	CHECK_HANDLE(handle, error);
	CLEAR_ERROR(error);

    cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        self->deleteMetric(name);
    }
    CATCH_EXCEPTIONS;
}

DllExport
CwseMetricID cwse_get_metric(void* handle, const char* name, struct CwseError* error)
{
	CwseMetricID result = CWSE_INVALID_METRIC_ID;
	CHECK_HANDLE_RET(handle, error);
	CLEAR_ERROR(error);

    cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        result = self->getMetric(name);
    }
    CATCH_EXCEPTIONS_RET;

	return result;
}

DllExport
void* cwse_names_iterator(void* handle, struct CwseError* error)
{
	void* result = NULL;
	CHECK_HANDLE_RET(handle, error);
	CLEAR_ERROR(error);

    cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        result = self->getNames();
    }
    CATCH_EXCEPTIONS_RET;

	return result;
}

DllExport
CwseMetricID cwse_get_next_name(void* names_iterator, char* name, int size, struct CwseError* error)
{
    CwseMetricID result = CWSE_INVALID_METRIC_ID;
	CLEAR_ERROR(error);

	if (name == 0) {
        if (error != NULL) {
            error->last_error = CWSE_ERROR_INVALID_ARG;
            STRNCPY(error->msg, "Invalid name buffer", sizeof(error->msg));
        }
        return result;
	}

    cdb::Names* names = static_cast<cdb::Names*>(names_iterator);
    try {
        CwseMetricID id;
        std::string str;
        bool success = names->next(id, str);
        if (success) {
            result = id;
            STRNCPY(name, str.c_str(), size);
        }
    }
    CATCH_EXCEPTIONS_RET;

	return result;
}

DllExport
void cwse_release_names_iterator(void* names_iterator, struct CwseError* error)
{
	CLEAR_ERROR(error);

	cdb::Names* names = static_cast<cdb::Names*>(names_iterator);
    try {
        delete names;
    }
    CATCH_EXCEPTIONS;
}


DllExport
void cwse_set(void* handle, CwseTimestamp ts, const struct CwseMetricValue* values, CwseUint count, struct CwseError* error)
{
	CHECK_HANDLE(handle, error);
	CLEAR_ERROR(error);

    cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        self->set(ts, values, count);
    }
    CATCH_EXCEPTIONS;
}

DllExport
void cwse_inc(void* handle, CwseTimestamp ts, const struct CwseMetricValue* values, CwseUint count, struct CwseError* error)
{
	CHECK_HANDLE(handle, error);
	CLEAR_ERROR(error);

    cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        self->inc(ts, values, count);
    }
    CATCH_EXCEPTIONS;
}

DllExport
struct CwseTimedValue cwse_get(void* handle, CwseMetricID id, struct CwseError* error)
{
	CwseTimedValue result = { 0, 0.0 };
	CHECK_HANDLE_RET(handle, error);
	CLEAR_ERROR(error);

	cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        result = self->get(id);
    }
    CATCH_EXCEPTIONS_RET;

	return result;
}

DllExport
void cwse_get_many(void* handle, const CwseMetricID* ids, struct CwseTimedValue* values, CwseUint count, struct CwseError* error)
{
	CHECK_HANDLE(handle, error);
	CLEAR_ERROR(error);

    cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        self->get(ids, values, count);
    }
    CATCH_EXCEPTIONS;
}


DllExport
void cwse_export(void* handle, const char* path, struct CwseError* error)
{
	CHECK_HANDLE(handle, error);
	CLEAR_ERROR(error);

    cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        self->exportDataset(path);
    }
    CATCH_EXCEPTIONS;
}

DllExport
void cwse_import(void* handle, const char* path, struct CwseError* error)
{
	CHECK_HANDLE(handle, error);
	CLEAR_ERROR(error);

    cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        self->importDataset(path);
    }
    CATCH_EXCEPTIONS;
}

DllExport
void cwse_flush(void* handle, struct CwseError* error)
{
	CHECK_HANDLE(handle, error);
	CLEAR_ERROR(error);

    cdb::Dataset* self = static_cast<cdb::Dataset*>(handle);
    try {
        self->flush();
    }
    CATCH_EXCEPTIONS;
}

#endif
