/**********************************************
   File:   metrics_const.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DEMO_METRICS_CONST_H
#define CDB_DEMO_METRICS_CONST_H

#include "metrics_plus_plus.h"

namespace cdb_demo {

using metrics_plus_plus::Metric;
Metric& metric(const char* name);

#define UPDATE_METRIC(name,value)  { static Metric& m = metric(name); m.update(value); }

extern metrics_plus_plus::DynoConfig* g_dynoConfig;

} // namespace cdb_demo

#define DYNO_CONFIG                             "mxdemo.config"
#define METRICS                                 "mxdemo.metrics"

//============= Connections ============================================

#define CFG_CONNECTIONS_COUNT                   "connections.count"
#define DEFAULT_CONNECTIONS_COUNT               4

#define CFG_WORKING_THREADS_COUNT               "working_threads.count"
#define DEFAULT_WORKING_THREADS_COUNT           4

#define CFG_CONNECTIONS_MULTIPLIER              "connections.delay.multiplier"
#define DEFAULT_CONNECTIONS_MULTIPLIER          10

#define CFG_CONNECTIONS_ADD_BUCKET_DELAY        "connections.delay.bucket.add"
#define DEFAULT_CONNECTIONS_ADD_BUCKET_DELAY    4

#define CFG_CONNECTIONS_DELETE_BUCKET_DELAY     "connections.delay.bucket.delete"
#define DEFAULT_CONNECTIONS_DELETE_BUCKET_DELAY 4

#define CFG_CONNECTIONS_LIST_BUCKET_DELAY       "connections.delay.bucket.list"
#define DEFAULT_CONNECTIONS_LIST_BUCKET_DELAY   4

#define CFG_CONNECTIONS_CREATE_ITEM_DELAY       "connections.delay.item.create"
#define DEFAULT_CONNECTIONS_CREATE_ITEM_DELAY   4

#define CFG_CONNECTIONS_READ_ITEM_DELAY         "connections.delay.item.read"
#define DEFAULT_CONNECTIONS_READ_ITEM_DELAY     2

#define CFG_CONNECTIONS_UPDATE_ITEM_DELAY       "connections.delay.item.update"
#define DEFAULT_CONNECTIONS_UPDATE_ITEM_DELAY   4

#define CFG_CONNECTIONS_DELETE_ITEM_DELAY       "connections.delay.item.delete"
#define DEFAULT_CONNECTIONS_DELETE_ITEM_DELAY   4

//============= Load Buckets ============================================

#define CFG_LOAD_BUCKETS_MULTIPLIER             "load.buckets.multiplier"
#define DEFAULT_LOAD_BUCKETS_MULTIPLIER         1

#define CFG_LOAD_BUCKETS_SLEEP                  "load.buckets.sleep"
#define DEFAULT_LOAD_BUCKETS_SLEEP              100

#define CFG_LOAD_BUCKETS_ADD                    "load.buckets.add"
#define DEFAULT_LOAD_BUCKETS_ADD                5

#define CFG_LOAD_BUCKETS_DELETE                 "load.buckets.delete"
#define DEFAULT_LOAD_BUCKETS_DELETE             5

#define CFG_LOAD_BUCKETS_LIST                   "load.buckets.list"
#define DEFAULT_LOAD_BUCKETS_LIST               40

#define CFG_LOAD_BUCKETS_INVALID                "load.buckets.invalid"
#define DEFAULT_LOAD_BUCKETS_INVALID            10

#define CFG_LOAD_BUCKETS_RECONNECT              "load.buckets.reconnect"
#define DEFAULT_LOAD_BUCKETS_RECONNECT          5


//============= Load Items ============================================

#define CFG_LOAD_ITEMS_MULTIPLIER             "load.items.multiplier"
#define DEFAULT_LOAD_ITEMS_MULTIPLIER         1

#define CFG_LOAD_ITEMS_SLEEP                  "load.items.sleep"
#define DEFAULT_LOAD_ITEMS_SLEEP              100

#define CFG_LOAD_ITEMS_CREATE                 "load.items.add"
#define DEFAULT_LOAD_ITEMS_CREATE             5

#define CFG_LOAD_ITEMS_DELETE                 "load.items.delete"
#define DEFAULT_LOAD_ITEMS_DELETE             5

#define CFG_LOAD_ITEMS_READ                   "load.items.read"
#define DEFAULT_LOAD_ITEMS_READ               40

#define CFG_LOAD_ITEMS_UPDATE                 "load.items.update"
#define DEFAULT_LOAD_ITEMS_UPDATE             40

#define CFG_LOAD_ITEMS_INVALID                "load.items.invalid"
#define DEFAULT_LOAD_ITEMS_INVALID            10

#define CFG_LOAD_ITEMS_RECONNECT              "load.items.reconnect"
#define DEFAULT_LOAD_ITEMS_RECONNECT          5

//============= Metrics ===============================================

#define METRIC_TIMER_HEARTBEAT                "timer.heartbeat"
#define METRIC_CONNECTIONS_COUNT              "connections.count"
#define METRIC_CONNECTIONS_UNIQUE_COUNT       "connections.unique.count"
#define METRIC_WORKING_THREADS_COUNT          "working_threads.count"

#define METRIC_COMMAND                        "commands"
#define METRIC_COMMAND_BUCKET                 "commands.bucket"
#define METRIC_COMMAND_BUCKET_ADD             "commands.bucket.add"
#define METRIC_COMMAND_BUCKET_DROP            "commands.bucket.drop"
#define METRIC_COMMAND_BUCKET_LIST            "commands.bucket.list"
#define METRIC_COMMAND_ITEM                   "commands.item"
#define METRIC_COMMAND_ITEM_CREATE            "commands.item.create"
#define METRIC_COMMAND_ITEM_READ              "commands.item.read"
#define METRIC_COMMAND_ITEM_UPDATE            "commands.item.update"
#define METRIC_COMMAND_ITEM_DELETE            "commands.item.delete"
#define METRIC_COMMAND_INVALID                "commands.invalid"
#define METRIC_DATA_BUCKETS_COUNT             "data.buckets.count"
#define METRIC_DATA_ITEMS_COUNT               "data.items.count"
#define METRIC_DATA_ITEMS_SIZE                "data.items.size"
#define METRIC_HANDLER_BUSY                   "handler.busy.count"
#define METRIC_HANDLER_OK                     "handler.ok.count"
#define METRIC_HANDLER_FAILURE                "handler.failure.count"
#define METRIC_HANDLER_SEND_SIZE              "handler.send.size"
#define METRIC_HANDLER_RECV_SIZE              "handler.recv.size"
#define METRIC_HANDLER_RECV_COUNT             "handler.recv.count"
#define METRIC_HANDLER_RECV_FAILED            "handler.failure.recv.count"
#define METRIC_HANDLER_SEND_FAILED            "handler.failure.send.count"
#define METRIC_HANDLER_REQUEST_RATE_1_SEC     "handler.request.rate.1"
#define METRIC_HANDLER_RECV_THROUGHPUT_1_SEC  "handler.recv.throughput.1"
#define METRIC_HANDLER_SEND_THROUGHPUT_1_SEC  "handler.send.throughput.1"
#define METRIC_HANDLER_REQUEST_RATE_1_MIN     "handler.request.rate.60"
#define METRIC_HANDLER_RECV_THROUGHPUT_1_MIN  "handler.recv.throughput.60"
#define METRIC_HANDLER_SEND_THROUGHPUT_1_MIN  "handler.send.throughput.60"

#define METRIC_QUEUE_COUNT                    "queue.count"

#endif // CDB_DEMO_METRICS_CONST_H
