/**********************************************
   File:   container.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "demo/container.h"
#include "demo/exception.h"
#include "demo/metrics_const.h"

using namespace metrics_plus_plus;

namespace cdb_demo {

Container Container::m_self;

/*************************************************************
 *    Bucket
 */

int Bucket::create(const std::string& value)
{
    boost::lock_guard<boost::mutex> lock(m_mutex);
    m_values[m_nextID] = value;

    UPDATE_METRIC(METRIC_DATA_ITEMS_COUNT, 1);
    UPDATE_METRIC(METRIC_DATA_ITEMS_SIZE, value.length());

    return m_nextID++;
}

std::string Bucket::read(int id)
{
    boost::lock_guard<boost::mutex> lock(m_mutex);
    ValuesMap::const_iterator iter = m_values.find(id);
    if (iter == m_values.end()) {
        THROW("Failed to find item with id");
    }
    return iter->second;
}

void Bucket::update(int id, const std::string& value)
{
    boost::lock_guard<boost::mutex> lock(m_mutex);
    ValuesMap::const_iterator iter = m_values.find(id);
    if (iter == m_values.end()) {
        THROW("Failed to find item with id");
    }

    UPDATE_METRIC(METRIC_DATA_ITEMS_SIZE, (double)value.length() - (double)iter->second.length());

    m_values[id] = value;
}

void Bucket::del(int id)
{
    boost::lock_guard<boost::mutex> lock(m_mutex);
    ValuesMap::iterator iter = m_values.find(id);
    if (iter == m_values.end()) {
        THROW("Failed to find item with id");
    }

    UPDATE_METRIC(METRIC_DATA_ITEMS_COUNT, 1);
    UPDATE_METRIC(METRIC_DATA_ITEMS_COUNT, - (double)iter->second.length());

    m_values.erase(iter);
}

void Bucket::deleteAllItems()
{
    boost::lock_guard<boost::mutex> lock(m_mutex);

    ValuesMap::iterator iter = m_values.begin();
    for ( ; iter != m_values.end(); ++iter) {
        UPDATE_METRIC(METRIC_DATA_ITEMS_COUNT, -1);
        UPDATE_METRIC(METRIC_DATA_ITEMS_SIZE, - (double)iter->second.length());
    }
}

/*************************************************************
 *    Container
 */

void Container::create(const std::string& name)
{
    boost::lock_guard<boost::mutex> lock(m_mutex);
    BucketsMap::iterator iter = m_buckets.find(name);
    if (iter != m_buckets.end()) {
        THROW("Bucket with name exists already");
    }
    m_buckets[name] = BucketPtr(new Bucket);

    UPDATE_METRIC(METRIC_DATA_BUCKETS_COUNT, 1);
}

void Container::drop(const std::string& name)
{
    boost::lock_guard<boost::mutex> lock(m_mutex);
    BucketsMap::iterator iter = m_buckets.find(name);
    if (iter == m_buckets.end()) {
        THROW("Failed to find bucket with name");
    }

    iter->second->deleteAllItems();
    m_buckets.erase(iter);

    UPDATE_METRIC(METRIC_DATA_BUCKETS_COUNT, -1);
}

void Container::enumerate(std::list<std::string>& buckets)
{
    boost::lock_guard<boost::mutex> lock(m_mutex);
    BucketsMap::iterator iter = m_buckets.begin();
    for ( ; iter != m_buckets.end(); ++iter) {
        buckets.push_back(iter->first);
    }
}

BucketPtr Container::get(const std::string& name)
{
    boost::lock_guard<boost::mutex> lock(m_mutex);
    BucketsMap::iterator iter = m_buckets.find(name);
    if (iter == m_buckets.end()) {
        THROW("Failed to find bucket with name");
    }
    return iter->second;
}

} // namespace cdb_demo
