/**********************************************
   File:   controller.h

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_SYSTEM_CONTROLLER_H
#define CDB_SYSTEM_CONTROLLER_H

#include "demo/listener.h"
#include "demo/task.h"
#include "metrics_plus_plus.h"
#include <boost/thread.hpp>
#include <vector>
#include <map>

using std::vector;

namespace cdb_demo {

class WorkingThread
{
public:
    WorkingThread(int id=0) : m_finished(false), m_id(id) {}
    virtual ~WorkingThread() {}
    virtual void start() { m_thread = boost::thread(&WorkingThread::run, this); }
    virtual void stop()  { m_thread.join(); }
    virtual void run() = 0;

    bool finished() const { return m_finished; }

protected:
    bool m_finished;

private:
    int m_id;
    boost::thread m_thread;
};

class TaskProcessor : public WorkingThread
{
public:
    TaskProcessor(int id) : WorkingThread(id) {}
    virtual void run();
};

class Communicator : public WorkingThread
{
public:
    void init(short port, int tfd, TimerEventHandler* timerEventHandler);
    virtual void run();

private:
    Listener m_listener;

};

class Controller : public TimerEventHandler
{
private:
    Controller(const Controller&);
    Controller& operator=(const Controller&);

public:
    static Controller& instance();

public:
    Controller() : m_communicator(NULL), m_workingThreadID(0), m_targetCount(0) {}
    void init(short port);
    void start();
    void stop();
    void uninit();

    virtual void handleTimerEvent();

private:
    typedef map< int, TaskProcessor* >  TaskProcessors;
    TaskProcessors m_taskProcessors;
    Communicator* m_communicator;
    boost::mutex m_mutex;
    int m_workingThreadID;
    int m_targetCount;
    metrics_plus_plus::DynoConfigValue m_workingThreadCount;
};

} // namespace cdb_demo

#endif // CDB_SYSTEM_CONTROLLER_H

