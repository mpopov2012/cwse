/**********************************************
   File:   buffer.cpp

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "demo/buffer.h"
#include "demo/exception.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

namespace cdb_demo {

/******************************
 *  Buffer
 */
Buffer::Buffer()
  : m_buffer(NULL),
    m_size(0),
    m_length(0),
    m_offset(0),
    m_fixed(false)
{
}

Buffer::~Buffer()
{
    if (!m_fixed) {
        free(m_buffer);
    }
}

void Buffer::init(char* buffer, size_t size)
{
    assert(buffer != NULL);

    if (!m_fixed && m_buffer != NULL) {
        free(m_buffer);
    }

    m_buffer = buffer;
    m_size = size;
    m_fixed = true;
}

void Buffer::init(size_t size)
{
    if (!m_fixed && m_buffer != NULL) {
        free(m_buffer);
    }

    m_buffer = (char*) malloc(size);
    if (m_buffer == 0) THROW("Failed to allocate memory");
    m_size = size;
    clear();
}

void Buffer::extend(size_t increment)
{
    if (m_fixed) THROW("Cannot change size of the fixed buffer");
    if (increment < 1024) increment = 1024;
    m_buffer = (char*) ::realloc(m_buffer, m_size + increment);
    if (m_buffer == NULL) THROW("Failed to allocate memory");
    m_size += increment;
}

const char* Buffer::data()
{
    if (m_offset >= m_length) return NULL;
    return m_buffer + m_offset;
}

char* Buffer::buffer()
{
    if (m_buffer == NULL) return NULL;
    return m_buffer + m_length;
}

void Buffer::clear()
{
    memset(m_buffer, 0, m_size);
    m_length = m_offset = 0;
}

void Buffer::append(const string& content)
{
    size_t length = content.length();
    if (available() < length) extend(length-available());
    memcpy(buffer(), content.c_str(), length);
    commit(length);
}

void Buffer::append(const char* buf, size_t size)
{
    size_t length = size + 1;
    if (available() < length) extend(length-available());
    memcpy(buffer(), buf, size);
    buffer()[length] = '\0';
    commit(size);
}

void Buffer::commit(size_t length)
{
    if (m_length + length >= m_size) THROW("Committing too much length");
    m_length += length;
}

void Buffer::closeText()
{
    if (available() < 1) extend(1);
    memcpy(buffer(), "\0", 1);
    commit(1);
}

void Buffer::shiftContent()
{
    if (m_length > m_offset) {
        memmove(m_buffer, m_buffer+m_offset, m_length-m_offset);
        m_offset = 0;
        m_length -= m_offset;
    }
}


} // namespace cdb_demo


