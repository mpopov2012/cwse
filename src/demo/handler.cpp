/**********************************************
   File:   handler.cpp

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "demo/parser.h"
#include "demo/handler.h"
#include "demo/exception.h"
#include "demo/command_base.h"
#include "demo/metrics_const.h"
#include <iostream>
#include <string>
#include <string.h>

using std::map;
using std::string;
using std::cout;
using std::endl;
using std::ostringstream;

namespace cdb_demo {

/*******************************************
 *
 *   ParseFailure
 *
 */

void ParseFailure::exec(Buffer& response)
{
    UPDATE_METRIC(METRIC_COMMAND_INVALID, 1);
    response.append("Failed to parse command");
}

/*******************************************
 *
 *   HandlerTask
 *
 */

HandlerTask::HandlerTask()
  : m_parent(NULL),
    m_command(NULL)
{
    m_buffer.init(4096);
}

HandlerTask::~HandlerTask()
{

}

void HandlerTask::fillupResponse()
{
    UPDATE_METRIC(METRIC_HANDLER_BUSY, 1);

    m_buffer.append("busy");
    sendBuffer();
    m_parent->notifyListener();
}

void HandlerTask::exec()
{
    try {
        m_command->exec(m_buffer);
        delete m_command;
        UPDATE_METRIC(METRIC_HANDLER_OK, 1);
    }
    catch(std::runtime_error& e) {
        delete m_command;
        m_buffer.clear();
        m_buffer.append(e.what());
        UPDATE_METRIC(METRIC_HANDLER_FAILURE, 1);
    }
    catch(...) {
        delete m_command;
        m_buffer.clear();
        m_buffer.append("Failed to execute command");
        UPDATE_METRIC(METRIC_HANDLER_FAILURE, 1);
    }

    bool failure = !sendBuffer();
    m_parent->notifyListener(failure);
}

bool HandlerTask::sendBuffer()
{
    int ret;
    bool success = true;

    try {
        uint16_t length = (uint16_t) m_buffer.length();
        ret = m_parent->socket().send((char*) &length, 2);
        if (ret != 2) THROW("Failed to write buffer length to socket");

        ret = m_parent->socket().send(m_buffer.raw(), m_buffer.length());
        if (ret != (int)m_buffer.length()) THROW("Failed to write data to socket");

        UPDATE_METRIC(METRIC_HANDLER_SEND_SIZE, m_buffer.length() + 2);
    }
    catch (...) {
        success = false;
        UPDATE_METRIC(METRIC_HANDLER_SEND_FAILED, 1);
    }

    m_buffer.clear();

    return success;
}

/*******************************************
 *
 *   Handler
 *
 */

Handler::Handler(int fd, int pipeFD)
  : m_socket(fd),
    m_pipeFD(pipeFD)
{
    m_task.setParent(this);
    m_request.init(REQUEST_BUFFER_SIZE);
}

Handler::~Handler()
{

}

void Handler::notifyListener(bool withCloseConnectionFlag)
{
    int fdes = fd();
    if (withCloseConnectionFlag) fdes *= -1;
    // send notification to listener
    if (0 == ::write(m_pipeFD, &fdes, sizeof(int))) {} // eliminating warning about unused return value
}

int Handler::recvRequest()
{
    Parser parser;
    int res = 0;

    try {
        uint16_t length;

        int ret = m_socket.receive((char*) &length, 2);
        if (ret != 2) {
            THROW("Failed to read length from socket");
        }

        m_request.clear();
        if (length > m_request.size()) {
            m_request.extend(length - m_request.size());
        }

        ret = m_socket.receive(m_request.raw(), length);
        if (ret != (int) length) {
            THROW("Failed to read data from socket");
        }

        UPDATE_METRIC(METRIC_HANDLER_RECV_SIZE, length + 2);
        UPDATE_METRIC(METRIC_HANDLER_RECV_COUNT, 1);

        CommandBase* cmd;
        try {
            cmd = parser.parse(m_request.raw());
            if (cmd == NULL) {
                cmd = new ParseFailure;
            }
        }
        catch (...) {
            cmd = new ParseFailure;
        }

        m_task.setCommand(cmd);
        MainQueue::put(&m_task);

        UPDATE_METRIC(METRIC_QUEUE_COUNT, 1);
    }
    catch(...) {
        res = -1;
        UPDATE_METRIC(METRIC_HANDLER_RECV_FAILED, 1);
    }
    return res;
}


} // namespace cdb_demo
