/**********************************************
   File:   timer.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DEMO_TIMER_H
#define CDB_DEMO_TIMER_H

#include <time.h>
#include <stdlib.h>

namespace cdb_demo {

inline void sleepMs(int ms)
{
    timespec ts, ts2;
    ts.tv_sec = 0;
    ts.tv_nsec = ms * 1000000;
    nanosleep(&ts, &ts2);
}

inline void randomSleep(int prob, unsigned int randSeed)
{
    int r = rand_r(&randSeed);
    r = r % prob;
    sleepMs(r);
}

}

#endif // CDB_DEMO_TIMER_H

