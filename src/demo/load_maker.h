/**********************************************
   File:   load_maker.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DEMO_LOAD_MAKER_H
#define CDB_DEMO_LOAD_MAKER_H

#include "demo/sock.h"
#include "demo/buffer.h"
#include "demo/controller.h"
#include "metrics_plus_plus.h"
#include <boost/smart_ptr.hpp>
#include <boost/thread.hpp>
#include <list>
#include <map>
#include <vector>

namespace cdb_demo {

typedef std::list<int> ItemsList;
typedef boost::shared_ptr<ItemsList> ItemsListPtr;
typedef std::map< std::string, ItemsListPtr > BucketsMap;

struct LoadData
{
    BucketsMap m_buckets;
    int m_maxBucketId;
    boost::mutex m_mutex;
};

class BucketsMaker : public WorkingThread
{
public:
    BucketsMaker() : m_sock(NULL), m_buffer(NULL), m_randSeed(0), m_port(0) {}
    void init(Sock* sock, Buffer* buffer, short port);

    virtual void run();

private:
    Sock* m_sock;
    Buffer* m_buffer;
    unsigned int m_randSeed;
    short m_port;

    metrics_plus_plus::DynoConfigValue m_multipliyer;
    metrics_plus_plus::DynoConfigValue m_sleepMs;
    metrics_plus_plus::DynoConfigValue m_addProbability;
    metrics_plus_plus::DynoConfigValue m_deleteProbability;
    metrics_plus_plus::DynoConfigValue m_listProbability;
    metrics_plus_plus::DynoConfigValue m_invalidProbability;
    metrics_plus_plus::DynoConfigValue m_reconnectProbability;

private:
    void doWork();
};

class ItemsMaker : public WorkingThread
{
public:
    void init(short port);
    virtual void run();
private:
    Sock m_sock;
    Buffer m_buffer;
    unsigned int m_randSeed;
    short m_port;

    metrics_plus_plus::DynoConfigValue m_multipliyer;
    metrics_plus_plus::DynoConfigValue m_sleepMs;
    metrics_plus_plus::DynoConfigValue m_createProbability;
    metrics_plus_plus::DynoConfigValue m_readProbability;
    metrics_plus_plus::DynoConfigValue m_updateProbability;
    metrics_plus_plus::DynoConfigValue m_deleteProbability;
    metrics_plus_plus::DynoConfigValue m_invalidProbability;
    metrics_plus_plus::DynoConfigValue m_reconnectProbability;

private:
    void doWork();
};

class LoadMaker
{
public:
    void init(short port, int workThreadCount);
    void start();
    void stop();
    void uninit();

private:
    Sock m_sock;
    Buffer m_buffer;
    BucketsMaker m_bucketsMaker;
    std::vector<ItemsMaker*> m_itemMakers;

private:
    void buildInitialStock();

};

} // namespace cdb_demo

#endif // CDB_DEMO_LOAD_MAKER_H
