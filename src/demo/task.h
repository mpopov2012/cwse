/**********************************************
   File:   task.h

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_BASE_TASK_H
#define CDB_BASE_TASK_H

#include "demo/queue.h"

namespace cdb_demo {

class Task
{
public:
   virtual ~Task() {}
   virtual void fillupResponse() {}
   virtual void exec() { }
   virtual void finalize() {}
};

class NoReturnTask : public Task
{
public:
   virtual void finalize() { delete this; }
};

class MainQueue
{
private:
    MainQueue() {}

public:
    static void put(Task* task) { m_queue.put(task); }
    static Task* get() { return m_queue.get(); }
    static bool fillup() { return m_queue.fillup(); }

private:
    static Queue<Task> m_queue;
};


} // namespace cbd

#endif // CDB_BASE_TASK_H
