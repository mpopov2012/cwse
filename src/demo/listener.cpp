/**********************************************
   File:   listener.cpp

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "demo/listener.h"
#include "demo/exception.h"
#include "demo/metrics_const.h"
#include "metrics_plus_plus.h"
#include <string>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <assert.h>
#include <string.h>
#include <errno.h>

using std::string;
using std::cout;
using std::cerr;
using std::endl;

using namespace metrics_plus_plus;

namespace cdb_demo {

Listener::Listener()
  : m_listenFD(-1),
    m_timerFD(-1),
    m_timerEventHandler(NULL),
    m_epoll(-1),
    m_perfConnectionCount(NULL)
{
    m_pipe[0] = m_pipe[1] = -1;
}

Listener::~Listener()
{
    Handlers::iterator iter = m_handlers.begin();
    for ( ;  iter != m_handlers.end(); ++iter) {
        int fd = iter->second->fd();
        int ret = epoll_ctl(m_epoll, EPOLL_CTL_DEL , fd, NULL);
        if (ret < 0) {
            cerr << "Failed to remove a file handler " << fd << " from epoll - " << strerror(errno);
        }
        close(fd);
    }

    if (m_listenFD != -1) close(m_listenFD);
    if (m_epoll != -1)    close(m_epoll);
    if (m_pipe[0] != -1)  close(m_pipe[0]);
    if (m_pipe[1] != -1)  close(m_pipe[1]);
}

void Listener::init(short port, int tfd, TimerEventHandler* timerEventHandler)
{
    int ret;

    m_timerFD = tfd;
    m_timerEventHandler = timerEventHandler;

    ret = pipe(m_pipe);
    if (ret < 0) THROW(string("Failed to create pipe - ") + strerror(errno));

    m_events.resize(EXPECTED_CONNECTION_COUNT);
    m_epoll = epoll_create(EXPECTED_CONNECTION_COUNT);
    if (m_epoll < 0) THROW(string("Failed to create epoll handler - ") + strerror(errno));

    m_listenFD = socket(AF_INET, SOCK_STREAM, 0);
    if (m_listenFD < 0) THROW(string("Failed to create listening socket - ") + strerror(errno));

    int on = 1;
    ret = setsockopt(m_listenFD, SOL_SOCKET,  SO_REUSEADDR, (char *)&on, sizeof(on));
    if (ret < 0) THROW(string("Failed to set listening socket reusable - ") + strerror(errno));

    ret = ioctl(m_listenFD, FIONBIO, (char *)&on);
    if (ret < 0) THROW(string("Failed to set listening socket non-blocking - ") + strerror(errno));

    struct sockaddr_in serv;
    memset(&serv, 0, sizeof(sockaddr_in));
    serv.sin_family = AF_INET;
    serv.sin_addr.s_addr = htonl(INADDR_ANY);
    serv.sin_port = htons(port);
    ret = bind(m_listenFD, (struct sockaddr *) &serv, sizeof(sockaddr_in));
    if (ret < 0) THROW(string("Failed to bind listening socket - ") + strerror(errno));

    ret = listen(m_listenFD, 5);
    if (ret < 0) THROW(string("Failed to start listening socket - ") + strerror(errno));

    epoll_event ev;
    memset(&ev, 0, sizeof(epoll_event));
    ev.events = EPOLLIN;
    ret = epoll_ctl(m_epoll, EPOLL_CTL_ADD , m_listenFD, &ev);
    if (ret < 0) THROW(string("Failed to add listening socket to epoll - ") + strerror(errno));

    memset(&ev, 0, sizeof(epoll_event));
    ev.events = EPOLLIN;
    ev.data.fd = m_pipe[0];
    ret = epoll_ctl(m_epoll, EPOLL_CTL_ADD , m_pipe[0], &ev);
    if (ret < 0) THROW(string("Failed to add pipet to epoll - ") + strerror(errno));

    memset(&ev, 0, sizeof(epoll_event));
    ev.events = EPOLLIN;
    ev.data.fd = m_timerFD;
    ret = epoll_ctl(m_epoll, EPOLL_CTL_ADD , m_timerFD, &ev);
    if (ret < 0) THROW(string("Failed to add listening socket to epoll - ") + strerror(errno));

}

void Listener::handle()
{
    int ret = epoll_wait(m_epoll, &m_events[0], (int)m_events.size(), EPOLL_TIMEOUT);
    if (ret < 0) {
        if (errno == EINTR) return;
        THROW(string("Failed to wait on sockets - ") + strerror(errno));
    }

    for (int i=0; i < ret; i++) {
        if (m_events[i].data.fd == 0) {
            acceptConnection();
        }
        else if (m_events[i].data.fd == m_pipe[0]) {
            processNotification();
        }
        else if (m_events[i].data.fd == m_timerFD) {
            processTimerEvent();
        }
        else {
            Handler* hptr = static_cast<Handler*>(m_events[i].data.ptr);
            if (m_events[i].events & EPOLLIN) {
                int ret = hptr->recvRequest();
                if (ret != 0)
                    removeConnection(hptr->fd());
                else
                    blockConnection(hptr->fd());
            }
            else if (m_events[i].events & EPOLLERR) {
                removeConnection(hptr->fd());
            }
        }
    }
}

void Listener::acceptConnection()
{
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    socklen_t len = 0;
    int fd = accept(m_listenFD, (struct sockaddr *) &addr, &len);
    if (fd < 0) {
        cerr << "Failed to accept a new connection - " << strerror(errno);
        return;
    }

    Handler* p = new Handler(fd, m_pipe[1]);

    epoll_event ev;
    memset(&ev, 0, sizeof(epoll_event));
    ev.events = EPOLLIN;
    ev.data.ptr = p;
    int ret = epoll_ctl(m_epoll, EPOLL_CTL_ADD , fd, &ev);
    if (ret < 0) {
        delete p;
        close(fd);
        cerr << "Failed to add listening socket to epoll - " << strerror(errno);
        return;
    }

    HandlerPtr hptr(p);
    m_handlers[fd] = hptr;

    if (m_handlers.size() > m_events.size()) m_events.resize(m_events.size() + EXPECTED_CONNECTION_COUNT);

    UPDATE_METRIC(METRIC_CONNECTIONS_COUNT, m_handlers.size());
    UPDATE_METRIC(METRIC_CONNECTIONS_UNIQUE_COUNT, 1);
}

void Listener::blockConnection(int fd)
{
    int ret = epoll_ctl(m_epoll, EPOLL_CTL_DEL , fd, NULL);
    if (ret < 0) {
        cerr << "Failed to remove a file handler " << fd << " from epoll - " << strerror(errno);
    }
}

void Listener::removeConnection(int fd)
{
    Handlers::iterator iter = m_handlers.find(fd);
    if (iter == m_handlers.end()) {
        cerr << "Failed to find a handler for socket " << fd << " on error handling";
        close(fd);
        return;
    }

    m_handlers.erase(iter);

    UPDATE_METRIC(METRIC_CONNECTIONS_COUNT, m_handlers.size());
}

void Listener::processNotification()
{
    int fd;
    int ret = read(m_pipe[0], &fd, sizeof(int));
    if (ret != (int) sizeof(int)) {
        cerr << "Failed to read from pipe - " << strerror(errno);
        return;
    }

    Handlers::iterator iter = m_handlers.find(fd < 0 ? -fd : fd );
    if (iter == m_handlers.end()) {
        cerr << "Failed to find a handler for fd=" << fd;
        return;
    }

    if (fd < 0) {
        removeConnection(-fd);
    }
    else {
        epoll_event ev;
        memset(&ev, 0, sizeof(epoll_event));
        ev.events = EPOLLIN;
        ev.data.ptr = iter->second.get();
        int ret = epoll_ctl(m_epoll, EPOLL_CTL_ADD , fd, &ev);
        if (ret < 0) {
            m_handlers.erase(iter);
            cerr << "Failed to add listening socket to epoll - " << strerror(errno);
        }
    }
}

void Listener::processTimerEvent()
{
    uint64_t num;
    int ret = ::read(m_timerFD, &num, sizeof(num));
    if (ret < 0) {
        cerr << "Failed to read timerFD" << endl;
        return;
    }

    if (m_timerEventHandler != NULL) {
        m_timerEventHandler->handleTimerEvent();
    }

    UPDATE_METRIC(METRIC_TIMER_HEARTBEAT, 1);
}

} // namespace cdb_demo
