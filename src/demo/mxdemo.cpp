/**********************************************
   File:   mxdemo.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "metrics_plus_plus.h"
#include "demo/controller.h"
#include "demo/connections.h"
#include "demo/sock.h"
#include "demo/timer.h"
#include "demo/exception.h"
#include "demo/buffer.h"
#include "demo/config.h"
#include "demo/load_maker.h"
#include "demo/metrics_const.h"
#include "utils/string_utils.h"
#include "utils/log.h"
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <fstream>
#include <readline/readline.h>
#include <readline/history.h>
#include <string.h>

using namespace cdb;
using namespace metrics_plus_plus;
using namespace cdb_demo;
using namespace std;

static bool g_keepGoing = true;
static bool g_silent = false;
static Sock g_sock;
static Buffer g_buffer;
static short g_port = 1974;
static LoadMaker g_loadMaker;
static bool g_loadMakerStarted = false;
static MetricsFactory* g_metricsFactory;

static void workConsole();
static void execCommand(string& cmd);
static void loadTest(int threadCount);
static void prepareDynoConfig();
static void prepareMetrics(const Config& config);
static void deleteMetrics();

namespace cdb_demo {

metrics_plus_plus::DynoConfig* g_dynoConfig;

metrics_plus_plus::Metric& metric(const char* name)
{
    return g_metricsFactory->get(name);
}

} // namespace cdb_demo

int main(int argc, char** argv)
{
    Config config;
    config.process((const char**) argv+1, argc-1);

    g_silent = config.find("silent");
    if (!g_silent) {
        cout << "Metrics Demo ver 1.0" << endl;
    }

    prepareDynoConfig();
    prepareMetrics(config);

    string port;
    if (config.find("port", port)) {
        g_port = boost::lexical_cast<short>(port);
    }

    Controller::instance().init(g_port);
    Controller::instance().start();

    ConnectionPool::instance().init(2);

    sleepMs(20);

    workConsole();

    if (g_loadMakerStarted) {
        g_loadMaker.stop();
        g_loadMaker.uninit();
    }
    Controller::instance().stop();
    Controller::instance().uninit();

    deleteMetrics();

    return 0;
}

void loadTest(int threadCount)
{
    if (g_loadMakerStarted) {
        return;
    }
    if (!g_silent) {
        cout << "Starting load test..." << endl;
    }
    g_loadMakerStarted = true;
    g_loadMaker.init(g_port, threadCount);
    g_loadMaker.start();
}

void workConsole()
{
    g_sock.connect("localhost", g_port);
    g_buffer.init(1024);

    bool newCmd = true;
    string cmd = "";
    while (g_keepGoing) {
        char* s = NULL;
        try {
            if (g_silent) {
                if (!cin.good()) break;
                string str;
                getline(cin, str);
                s = strdup(str.c_str());
            }
            else {
                s = readline( newCmd ? "=> " : "...");
            }
            if (s == 0) break;

            size_t len = strlen(s);
            if (len == 0) {
                free(s);
                if (cmd.empty()) continue;
            }
            else if (s[len-1] == '\\') {
                newCmd = false;
                s[len-1] = '\0';
                cmd += s;
                free(s);
                continue;
            }
            else {
                cmd += s;
                free(s);
            }

            add_history(cmd.c_str());
            execCommand(cmd);
            cmd = "";
            newCmd = true;
        }
        catch(std::runtime_error& e) {
            cerr << "FAILED: " << e.what() << endl;
            cmd = "";
            newCmd = true;
        }
    }
}

void execCommand(string& str)
{
    cdb::StringUtils::trim(str);
    if (str.empty()) return;

    if (str == "exit" || str == "quit") {
        g_keepGoing = false;
        return;
    }

    vector< string > tokens;
    cdb::StringUtils::split(str, " ", tokens);
    if (tokens.size() == 2 && tokens[0] == "start") {
        int threadCount = boost::lexical_cast<int>(tokens[1]);
        loadTest(threadCount);
        return;
    }

    int ret;
    uint16_t length = str.length();
    ret = g_sock.send((const char*)& length, 2);
    if (ret != 2) THROW("Failed to send command's length");

    ret = g_sock.send(str.c_str(), length);
    if (ret != length) THROW("Failed to send command");

    ret = g_sock.receive((char*) &length, 2);
    if (ret != 2) THROW("Failed to receive response length");

    if (g_buffer.size() < length) {
        g_buffer.extend(length - g_buffer.size() + 1);
    }

    g_buffer.clear();
    ret = g_sock.receive(g_buffer.raw(), length);
    if (ret != length) THROW("Failed to receive response");

    g_buffer.commit(length);
    g_buffer.closeText();

    cout << g_buffer.raw() << endl;
}

static void createAndSetMetric(Dataset* dataset, const char* name, double value)
{
    if (dataset->findColumn(name) == CWSE_INVALID_COLUMN_ID) {
        CwseColumnConfig cfg = Dataset::getDefaultColumnConfig();
        dataset->addColumn(name, &cfg, false);
        uint16_t id = dataset->findColumn(name);
        dataset->set(value, id);
    }
}

static void prepareDynoConfig()
{
    const char* dynoConfigPath = DYNO_CONFIG;

    CwseDatasetConfig cfg = Dataset::getDefaultDatasetConfig();
    try {
        Dataset::create(dynoConfigPath, &cfg);
    }
    catch (...) {
    }

    Dataset* dataset = Dataset::open(dynoConfigPath, CWSE_DATA_ACCESS_READ_WRITE);

    createAndSetMetric(dataset, CFG_WORKING_THREADS_COUNT, DEFAULT_WORKING_THREADS_COUNT);

    createAndSetMetric(dataset, CFG_CONNECTIONS_COUNT, DEFAULT_CONNECTIONS_COUNT);
    createAndSetMetric(dataset, CFG_CONNECTIONS_MULTIPLIER, DEFAULT_CONNECTIONS_MULTIPLIER);
    createAndSetMetric(dataset, CFG_CONNECTIONS_ADD_BUCKET_DELAY, DEFAULT_CONNECTIONS_ADD_BUCKET_DELAY);
    createAndSetMetric(dataset, CFG_CONNECTIONS_DELETE_BUCKET_DELAY, DEFAULT_CONNECTIONS_DELETE_BUCKET_DELAY);
    createAndSetMetric(dataset, CFG_CONNECTIONS_LIST_BUCKET_DELAY, DEFAULT_CONNECTIONS_LIST_BUCKET_DELAY);
    createAndSetMetric(dataset, CFG_CONNECTIONS_CREATE_ITEM_DELAY, DEFAULT_CONNECTIONS_CREATE_ITEM_DELAY);
    createAndSetMetric(dataset, CFG_CONNECTIONS_READ_ITEM_DELAY, DEFAULT_CONNECTIONS_READ_ITEM_DELAY);
    createAndSetMetric(dataset, CFG_CONNECTIONS_UPDATE_ITEM_DELAY, DEFAULT_CONNECTIONS_UPDATE_ITEM_DELAY);
    createAndSetMetric(dataset, CFG_CONNECTIONS_DELETE_ITEM_DELAY, DEFAULT_CONNECTIONS_DELETE_ITEM_DELAY);

    createAndSetMetric(dataset, CFG_LOAD_BUCKETS_MULTIPLIER, DEFAULT_LOAD_BUCKETS_MULTIPLIER);
    createAndSetMetric(dataset, CFG_LOAD_BUCKETS_SLEEP, DEFAULT_LOAD_BUCKETS_SLEEP);
    createAndSetMetric(dataset, CFG_LOAD_BUCKETS_ADD, DEFAULT_LOAD_BUCKETS_ADD);
    createAndSetMetric(dataset, CFG_LOAD_BUCKETS_DELETE, DEFAULT_LOAD_BUCKETS_DELETE);
    createAndSetMetric(dataset, CFG_LOAD_BUCKETS_LIST, DEFAULT_LOAD_BUCKETS_LIST);
    createAndSetMetric(dataset, CFG_LOAD_BUCKETS_INVALID, DEFAULT_LOAD_BUCKETS_INVALID);
    createAndSetMetric(dataset, CFG_LOAD_BUCKETS_RECONNECT, DEFAULT_LOAD_BUCKETS_RECONNECT);

    createAndSetMetric(dataset, CFG_LOAD_ITEMS_MULTIPLIER, DEFAULT_LOAD_ITEMS_MULTIPLIER);
    createAndSetMetric(dataset, CFG_LOAD_ITEMS_SLEEP, DEFAULT_LOAD_ITEMS_SLEEP);
    createAndSetMetric(dataset, CFG_LOAD_ITEMS_CREATE, DEFAULT_LOAD_ITEMS_CREATE);
    createAndSetMetric(dataset, CFG_LOAD_ITEMS_DELETE, DEFAULT_LOAD_ITEMS_DELETE);
    createAndSetMetric(dataset, CFG_LOAD_ITEMS_READ, DEFAULT_LOAD_ITEMS_READ);
    createAndSetMetric(dataset, CFG_LOAD_ITEMS_UPDATE, DEFAULT_LOAD_ITEMS_UPDATE);
    createAndSetMetric(dataset, CFG_LOAD_ITEMS_INVALID, DEFAULT_LOAD_ITEMS_INVALID);
    createAndSetMetric(dataset, CFG_LOAD_ITEMS_RECONNECT, DEFAULT_LOAD_ITEMS_RECONNECT);

    delete dataset;

    g_dynoConfig = metrics_plus_plus::DynoConfig::create(dynoConfigPath);
}

static void prepareMetrics(const Config& config)
{
    LogLevel saveLogLevel = FILELog::Level();
    //FILELog::Level() = LL_DEBUG;

    LOG_DEBUG << "Enter prepareMetrics";

    g_metricsFactory = MetricsFactory::create(METRICS);

    string metricsPath;
    if (!config.find("metrics", metricsPath)) {
        LOG_DEBUG << "No metricsPath defined";
        return;
    }

    ifstream ifs;
    ifs.open(metricsPath.c_str());

    if (ifs.good()) {
        LOG_DEBUG << "Open file with metrics definitions: " << metricsPath;
    }
    else {
        LOG_DEBUG << "Failed to open file with metrics definitions: " << metricsPath;
    }

    int linesCount = 0;
    int commandsCount = 0;
    char buffer[256];
    while (ifs.good()) {
        ifs.getline(buffer, sizeof(buffer));
        if (ifs.fail()) {
            LOG_DEBUG << "Finished reading file. Number of lines: " << linesCount << "  Commands count: " << commandsCount;
            break;
        }
        linesCount++;

        char* line;
        for (line = buffer; *line; line++) {
            if (*line != ' ' || *line != '\t') {
                break;
            }
        }

        for (char* t = line; *t; t++) {
            if (*t == '#') {
                *t = '\0';
                break;
            }
        }

        if (*line == '\0') {
            continue;
        }

        char* saveptr = NULL;
        char* s = strtok_r(line, ";", &saveptr);
        while (s != NULL) {
            g_metricsFactory->process(s);
            s = strtok_r(NULL, ";", &saveptr);
            commandsCount++;
        }
    }

    FILELog::Level() = saveLogLevel;

    /*******************************
    g_metricsFactory->createCounter(METRIC_TIMER_HEARTBEAT);
    g_metricsFactory->createGauge(METRIC_WORKING_THREADS_COUNT);

    g_metricsFactory->createGauge(METRIC_CONNECTIONS_COUNT);
    g_metricsFactory->createCounter(METRIC_CONNECTIONS_UNIQUE_COUNT);

    g_metricsFactory->createCounter(METRIC_COMMAND);
    g_metricsFactory->createCounter(METRIC_COMMAND_BUCKET);      g_metricsFactory->linkMetrics(METRIC_COMMAND_BUCKET, METRIC_COMMAND);
    g_metricsFactory->createCounter(METRIC_COMMAND_BUCKET_ADD);  g_metricsFactory->linkMetrics(METRIC_COMMAND_BUCKET_ADD, METRIC_COMMAND_BUCKET);
    g_metricsFactory->createCounter(METRIC_COMMAND_BUCKET_DROP); g_metricsFactory->linkMetrics(METRIC_COMMAND_BUCKET_DROP, METRIC_COMMAND_BUCKET);
    g_metricsFactory->createCounter(METRIC_COMMAND_BUCKET_LIST); g_metricsFactory->linkMetrics(METRIC_COMMAND_BUCKET_LIST, METRIC_COMMAND_BUCKET);
    g_metricsFactory->createCounter(METRIC_COMMAND_ITEM);        g_metricsFactory->linkMetrics(METRIC_COMMAND_ITEM, METRIC_COMMAND);
    g_metricsFactory->createCounter(METRIC_COMMAND_ITEM_CREATE); g_metricsFactory->linkMetrics(METRIC_COMMAND_ITEM_CREATE, METRIC_COMMAND_ITEM);
    g_metricsFactory->createCounter(METRIC_COMMAND_ITEM_READ);   g_metricsFactory->linkMetrics(METRIC_COMMAND_ITEM_READ, METRIC_COMMAND_ITEM);
    g_metricsFactory->createCounter(METRIC_COMMAND_ITEM_UPDATE); g_metricsFactory->linkMetrics(METRIC_COMMAND_ITEM_UPDATE, METRIC_COMMAND_ITEM);
    g_metricsFactory->createCounter(METRIC_COMMAND_ITEM_DELETE); g_metricsFactory->linkMetrics(METRIC_COMMAND_ITEM_DELETE, METRIC_COMMAND_ITEM);
    g_metricsFactory->createCounter(METRIC_COMMAND_INVALID);     g_metricsFactory->linkMetrics(METRIC_COMMAND_INVALID, METRIC_COMMAND);

    g_metricsFactory->createCounter(METRIC_DATA_BUCKETS_COUNT);
    g_metricsFactory->createCounter(METRIC_DATA_ITEMS_COUNT);
    g_metricsFactory->createGauge(METRIC_DATA_ITEMS_SIZE);

    g_metricsFactory->createMeter(METRIC_HANDLER_REQUEST_RATE_1_SEC, 1, 1, 1);
    g_metricsFactory->createMeter(METRIC_HANDLER_RECV_THROUGHPUT_1_SEC, 1, 1, 1);
    g_metricsFactory->createMeter(METRIC_HANDLER_SEND_THROUGHPUT_1_SEC, 1, 1, 1);

    g_metricsFactory->createMeter(METRIC_HANDLER_REQUEST_RATE_1_MIN, 60, 1, 1);
    g_metricsFactory->createMeter(METRIC_HANDLER_RECV_THROUGHPUT_1_MIN, 60, 1, 1);
    g_metricsFactory->createMeter(METRIC_HANDLER_SEND_THROUGHPUT_1_MIN, 60, 1, 1);

    g_metricsFactory->createCounter(METRIC_HANDLER_BUSY);
    g_metricsFactory->createCounter(METRIC_HANDLER_OK);
    g_metricsFactory->createCounter(METRIC_HANDLER_FAILURE);
    g_metricsFactory->createCounter(METRIC_HANDLER_SEND_SIZE);   g_metricsFactory->linkMetrics(METRIC_HANDLER_SEND_SIZE, METRIC_HANDLER_SEND_THROUGHPUT_1_SEC);
                                                                 g_metricsFactory->linkMetrics(METRIC_HANDLER_SEND_SIZE, METRIC_HANDLER_SEND_THROUGHPUT_1_MIN);

    g_metricsFactory->createCounter(METRIC_HANDLER_RECV_SIZE);   g_metricsFactory->linkMetrics(METRIC_HANDLER_RECV_SIZE, METRIC_HANDLER_RECV_THROUGHPUT_1_SEC);
                                                                 g_metricsFactory->linkMetrics(METRIC_HANDLER_RECV_SIZE, METRIC_HANDLER_RECV_THROUGHPUT_1_MIN);

    g_metricsFactory->createCounter(METRIC_HANDLER_RECV_COUNT);  g_metricsFactory->linkMetrics(METRIC_HANDLER_RECV_COUNT, METRIC_HANDLER_REQUEST_RATE_1_SEC);
                                                                 g_metricsFactory->linkMetrics(METRIC_HANDLER_RECV_COUNT, METRIC_HANDLER_REQUEST_RATE_1_MIN);
    g_metricsFactory->createCounter(METRIC_HANDLER_RECV_FAILED);
    g_metricsFactory->createCounter(METRIC_HANDLER_SEND_FAILED);

    g_metricsFactory->createCounter(METRIC_QUEUE_COUNT);

********************************************************************/

    g_metricsFactory->start();
}

static void deleteMetrics()
{
    if (g_metricsFactory== NULL) return;
    g_metricsFactory->stop();
    g_metricsFactory->dumpMissingMetrics();
    delete g_metricsFactory;
    delete g_dynoConfig;
}
