/**********************************************
   File:   container.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DEMO_CONTAINER_H
#define CDB_DEMO_CONTAINER_H

#include <boost/thread/thread.hpp>
#include <boost/smart_ptr.hpp>
#include <map>
#include <string>
#include <list>

namespace cdb_demo {

class Bucket
{
public:
    Bucket() : m_nextID(0) {}

    int create(const std::string& value);
    std::string read(int id);
    void update(int id, const std::string& value);
    void del(int id);
    void deleteAllItems();

private:
    typedef std::map<int, std::string> ValuesMap;
    ValuesMap m_values;
    boost::mutex m_mutex;
    int m_nextID;
};
typedef boost::shared_ptr<Bucket> BucketPtr;

class Container
{
public:
    static Container& Instance() { return m_self; }

private:
    static Container m_self;

public:
    void create(const std::string& name);
    void drop(const std::string& name);
    void enumerate(std::list<std::string>& buckets);
    BucketPtr get(const std::string& name);

private:
    typedef std::map<std::string, BucketPtr> BucketsMap;
    BucketsMap m_buckets;
    boost::mutex m_mutex;
};

} // namespace cdb_demo

#endif // CDB_DEMO_CONTAINER_H
