/**********************************************
   File:   parser.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "demo/parser.h"
#include "demo/commands.h"
#include <boost/lexical_cast.hpp>
#include <stddef.h>
#include <string.h>
#include <iostream>

using namespace std;

namespace cdb_demo {

static const size_t MAX_COMMAND_LENGTH = 8;
static const size_t MAX_BUCKET_NAME_LENGTH = 32;

CommandBase* Parser::parse(const char* str)
{
    char command[MAX_COMMAND_LENGTH+1];

    size_t length = strlen(str);
    size_t i;
    for (i=0; i < length && i < MAX_COMMAND_LENGTH && str[i] != ' ' && str[i] != '\t'; i++) {
        command[i] = str[i];
    }
    command[i] = '\0';

    for (; i < length && (str[i] == ' ' || str[i] == '\t'); i++);

    if (strcmp(command, "add") == 0) {
        return parseNewCommand(str+i);
    }
    else if (strcmp(command, "drop") == 0) {
        return parseDropCommand(str+i);
    }
    else if (strcmp(command, "list") == 0) {
        return parseListCommand();
    }
    else if (strcmp(command, "create") == 0) {
        return parseCreateCommand(str+i);
    }
    else if (strcmp(command, "read") == 0) {
        return parseReadCommand(str+i);
    }
    else if (strcmp(command, "update") == 0) {
        return parseUpdateCommand(str+i);
    }
    else if (strcmp(command, "delete") == 0) {
        return parseDeleteCommand(str+i);
    }

    return new CmdUnknown;
}

CommandBase* Parser::parseNewCommand(const char* str)
{
    return new CmdNewBucket(str);
}

CommandBase* Parser::parseDropCommand(const char* str)
{
    return new CmdDropBucket(str);
}

CommandBase* Parser::parseListCommand()
{
    return new CmdListBucket();
}

CommandBase* Parser::parseCreateCommand(const char* str)
{
    char bucket[MAX_BUCKET_NAME_LENGTH+1];
    size_t offset = extractBucketName(str, bucket);
    return new CmdCreate(bucket, str+offset);
}

CommandBase* Parser::parseReadCommand(const char* str)
{
    char bucket[MAX_BUCKET_NAME_LENGTH+1];
    size_t offset = extractBucketName(str, bucket);
    int id;
    try {
        id = boost::lexical_cast<int>(str+offset);
    }
    catch(...) {
        return NULL;
    }
    return new CmdRead(bucket, id);
}

CommandBase* Parser::parseUpdateCommand(const char* str)
{
    char bucket[MAX_BUCKET_NAME_LENGTH+1];
    size_t offset = extractBucketName(str, bucket);

    char idstr[MAX_BUCKET_NAME_LENGTH+1];
    offset += extractBucketName(str+offset, idstr);

    int id;
    try {
        id = boost::lexical_cast<int>(idstr);
    }
    catch(...) {
        return NULL;
    }
    return new CmdUpdate(bucket, id, str+offset);
}

CommandBase* Parser::parseDeleteCommand(const char* str)
{
    char bucket[MAX_BUCKET_NAME_LENGTH+1];
    size_t offset = extractBucketName(str, bucket);
    int id;
    try {
        id = boost::lexical_cast<int>(str+offset);
    }
    catch(...) {
        return NULL;
    }
    return new CmdDelete(bucket, id);
}

size_t Parser::extractBucketName(const char* str, char* name)
{
    size_t length = strlen(str);
    size_t i;
    for (i=0; i < length && i < MAX_BUCKET_NAME_LENGTH && str[i] != ' ' && str[i] != '\t'; i++) {
        name[i] = str[i];
    }
    name[i] = '\0';

    for (; i < length && (str[i] == ' ' || str[i] == '\t'); i++);

    return i;
}

} // namespace cdb_demo

