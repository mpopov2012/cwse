/**********************************************
   File:   handler.h

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DEMO_HANDLER_H
#define CDB_DEMO_HANDLER_H

#include "demo/buffer.h"
#include "demo/sock.h"
#include "demo/task.h"
#include "demo/command_base.h"
#include <boost/smart_ptr.hpp>

namespace cdb_demo {

class Handler;

class ParseFailure : public CommandBase
{
public:
    ParseFailure() : CommandBase(CMD_PARSE_FAILURE) {}
    virtual void exec(Buffer& response);
};

class HandlerTask : public Task
{
public:
    HandlerTask();
    virtual ~HandlerTask();

    void setParent(Handler* parent) { m_parent = parent; }
    void setCommand(CommandBase* command) { m_command = command; }

    // override methods for Task
    virtual void fillupResponse();
    virtual void exec();
    virtual void finalize() {}

private:
    Handler* m_parent;
    Buffer   m_buffer;
    CommandBase* m_command;

private:
    bool sendBuffer();
};

class Handler
{
public:
    bool processCommand(CommandBase& command);

public:
    Handler(int fd, int pipeFD);
    ~Handler();

    int fd() { return m_socket.get(); }
    Sock& socket() { return m_socket; }
    int pipeFD() { return m_pipeFD; }

    int recvRequest();
    void notifyListener(bool withCloseConnectionFlag=false);

    HandlerTask& task() { return m_task; }

private:
    static const size_t REQUEST_BUFFER_SIZE = 1024 * 4;

private:

private:
    Sock m_socket;  // communication socket
    int m_pipeFD;
    HandlerTask m_task;
    Buffer m_request;
};
typedef boost::shared_ptr<Handler> HandlerPtr;

} // namespace cdb_demo

#endif // CDB_DEMO_HANDLER_H
