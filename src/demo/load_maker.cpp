/**********************************************
   File:   load_maker.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "demo/load_maker.h"
#include "demo/exception.h"
#include "demo/timer.h"
#include "demo/metrics_const.h"
#include <boost/lexical_cast.hpp>
#include <string>
#include <iostream>
#include <stdint.h>
#include <time.h>
#include <stdio.h>

using namespace std;

namespace cdb_demo {

static bool g_keepRunning = true;
static LoadData g_data;

static void execCommand(Sock& sock, string& str, Buffer& buffer)
{
    int ret;
    uint16_t length = str.length();
    ret = sock.send((const char*)& length, 2);
    if (ret != 2) THROW("Failed to send command's length");

    ret = sock.send(str.c_str(), length);
    if (ret != length) THROW("Failed to send command");

    ret = sock.receive((char*) &length, 2);
    if (ret != 2) THROW("Failed to receive response length");

    if (buffer.size() < length) {
        buffer.extend(length - buffer.size() + 1);
    }

    buffer.clear();
    ret = sock.receive(buffer.raw(), length);
    if (ret != length) THROW("Failed to receive response");

    buffer.commit(length);
    buffer.closeText();
}

static unsigned int makeSeed(void* p)
{
    unsigned int result;

    time_t t = time(NULL);
    memcpy(&result, &t, sizeof(result));

    unsigned int temp;
    memcpy(&temp, &p, sizeof(temp));

    result += temp;

    return result;
}

/*********************************************************
 *   BucketsMaker
 */

void BucketsMaker::init(Sock* sock, Buffer* buffer, short port)
{
    m_sock = sock;
    m_buffer = buffer;
    m_randSeed = makeSeed(this);
    m_port = port;

    m_multipliyer = g_dynoConfig->get(CFG_LOAD_BUCKETS_MULTIPLIER, DEFAULT_LOAD_BUCKETS_MULTIPLIER);
    m_sleepMs = g_dynoConfig->get(CFG_LOAD_BUCKETS_SLEEP, DEFAULT_LOAD_BUCKETS_SLEEP);
    m_addProbability = g_dynoConfig->get(CFG_LOAD_BUCKETS_ADD, DEFAULT_LOAD_BUCKETS_ADD);
    m_deleteProbability = g_dynoConfig->get(CFG_LOAD_BUCKETS_DELETE, DEFAULT_LOAD_BUCKETS_DELETE);
    m_listProbability = g_dynoConfig->get(CFG_LOAD_BUCKETS_LIST, DEFAULT_LOAD_BUCKETS_LIST);
    m_invalidProbability = g_dynoConfig->get(CFG_LOAD_BUCKETS_INVALID, DEFAULT_LOAD_BUCKETS_INVALID);
    m_reconnectProbability = g_dynoConfig->get(CFG_LOAD_BUCKETS_RECONNECT, DEFAULT_LOAD_BUCKETS_RECONNECT);
}

void BucketsMaker::run()
{
    while (g_keepRunning) {
        sleepMs(100);
        try {
            doWork();
        }
        catch (...) {
            cerr << "Crashed BucketMaster thread" << endl;
            return;
        }
    }
}

void BucketsMaker::doWork()
{
    int r = rand_r(&m_randSeed);
    r = r % (int)m_sleepMs();

    bool buckets_empty;
    string first_bucket;
    {
        boost::lock_guard<boost::mutex> lock(g_data.m_mutex);
        buckets_empty = g_data.m_buckets.empty();
        if (!buckets_empty) {
            first_bucket = g_data.m_buckets.begin()->first;
        }
    }

    if (r < m_multipliyer() * m_reconnectProbability()) {
        m_sock->disconnect();
        sleepMs(20);
        m_sock->connect("localhost", m_port);
    }

    int prob = m_multipliyer() * m_addProbability();
    if (r < prob || buckets_empty) {
        // Add bucket
        int nextId = g_data.m_maxBucketId + 1;
        string bucket = string("buck_") + boost::lexical_cast<string>(nextId);
        string cmd = string("add ") + bucket;
        execCommand(*m_sock, cmd, *m_buffer);

        if (strcmp(m_buffer->raw(), "ok") == 0) {
            boost::lock_guard<boost::mutex> lock(g_data.m_mutex);
            ItemsListPtr items(new list<int>);
            g_data.m_buckets[bucket] = items;
            g_data.m_maxBucketId = nextId;
        }
        else {
            cout << "Failed to add bucket: " << m_buffer->raw() << endl;
        }
        return;
    }

    prob += m_multipliyer() * m_deleteProbability();
    if (r < prob && !buckets_empty) {
        // Delete bucket
        string cmd = string("drop ") + first_bucket;
        execCommand(*m_sock, cmd, *m_buffer);

        if (strcmp(m_buffer->raw(), "ok") == 0) {
            boost::lock_guard<boost::mutex> lock(g_data.m_mutex);
            g_data.m_buckets.erase(g_data.m_buckets.begin());
        }
        else {
            cout << "Failed to delete bucket: " << m_buffer->raw() << endl;
        }
        return;
    }

    prob += m_multipliyer() * m_listProbability();
    if (r < prob) {
        // List buckets
        string cmd = "list";
        execCommand(*m_sock, cmd, *m_buffer);
        return;
    }

    prob += m_multipliyer() * m_invalidProbability();
    if (r < prob) {
        // Invalid command
        string cmd = "invalid command";
        execCommand(*m_sock, cmd, *m_buffer);
        return;
    }
}

/*********************************************************
 *   ItemsMaker
 */

void ItemsMaker::init(short port)
{
    m_port = port;
    m_sock.connect("localhost", port);
    m_randSeed = makeSeed(this);

    m_multipliyer = g_dynoConfig->get(CFG_LOAD_ITEMS_MULTIPLIER, DEFAULT_LOAD_ITEMS_MULTIPLIER);
    m_sleepMs = g_dynoConfig->get(CFG_LOAD_ITEMS_SLEEP, DEFAULT_LOAD_ITEMS_SLEEP);
    m_createProbability = g_dynoConfig->get(CFG_LOAD_ITEMS_CREATE, DEFAULT_LOAD_ITEMS_CREATE);
    m_deleteProbability = g_dynoConfig->get(CFG_LOAD_ITEMS_DELETE, DEFAULT_LOAD_ITEMS_DELETE);
    m_readProbability = g_dynoConfig->get(CFG_LOAD_ITEMS_READ, DEFAULT_LOAD_ITEMS_READ);
    m_updateProbability = g_dynoConfig->get(CFG_LOAD_ITEMS_UPDATE, DEFAULT_LOAD_ITEMS_UPDATE);
    m_invalidProbability = g_dynoConfig->get(CFG_LOAD_ITEMS_INVALID, DEFAULT_LOAD_ITEMS_INVALID);
    m_reconnectProbability = g_dynoConfig->get(CFG_LOAD_ITEMS_RECONNECT, DEFAULT_LOAD_ITEMS_RECONNECT);
}

void ItemsMaker::run()
{
    while (g_keepRunning) {
        sleepMs(100);
        try {
            doWork();
        }
        catch (...) {
            cerr << "Crashed ItemMaker thread" << endl;
            return;
        }
    }
}

void ItemsMaker::doWork()
{
    int r = rand_r(&m_randSeed);
    r = r % (int)m_sleepMs();

    int first = -1;
    string bucket;
    ItemsListPtr items;
    {
        boost::lock_guard<boost::mutex> lock(g_data.m_mutex);
        if (g_data.m_buckets.empty()) {
            return;
        }

        int n = r % g_data.m_buckets.size();
        BucketsMap::iterator iter = g_data.m_buckets.begin();

        for (int i = 0; iter != g_data.m_buckets.end(); i++, iter++) {
            if (i == n) {
                break;
            }
        }

        if (iter == g_data.m_buckets.end()) {
            cout << "ItemsMaker: Failed to find bucket" << endl;
            return;
        }

        bucket = iter->first;
        items = iter->second;
        if (!items->empty()) {
            first = *items->begin();
        }
    }

    if (r < m_multipliyer() * m_reconnectProbability()) {
        m_sock.disconnect();
        sleepMs(20);
        m_sock.connect("localhost", m_port);
    }

    int prob = m_multipliyer() * m_createProbability();
    if (r < prob) {
        // Add item
        string cmd = string("create ") + bucket + " 1111";
        execCommand(m_sock, cmd, m_buffer);

        int id;
        int n = sscanf(m_buffer.raw(), "%d", &id);
        if (n == 1) {
            boost::lock_guard<boost::mutex> lock(g_data.m_mutex);
            items->push_back(id);
        }
        else {
            //cout << "Failed to add item: " << m_buffer.raw() << endl;
        }
        return;
    }

    prob += m_multipliyer() * m_deleteProbability();
    if (r < prob && !items->empty()) {
        // Delete item
        string cmd = string("delete ") + bucket + " " + boost::lexical_cast<string>(first);
        execCommand(m_sock, cmd, m_buffer);

        if (strcmp(m_buffer.raw(), "ok") == 0) {
            boost::lock_guard<boost::mutex> lock(g_data.m_mutex);
            if (*items->begin() == first) {
                items->erase(items->begin());
            }
        }
        else {
            //cout << "Failed to delete item: " << m_buffer.raw() << endl;
        }
        return;
    }

    prob += m_multipliyer() * m_updateProbability();
    if (r < prob && !items->empty()) {
        // Update item
        string cmd = string("update ") + bucket + " " + boost::lexical_cast<string>(first) + " 2222";
        execCommand(m_sock, cmd, m_buffer);

        if (strcmp(m_buffer.raw(), "ok") != 0) {
            //cout << "Failed to update item: " << m_buffer.raw() << endl;
        }
        return;
    }

    prob += m_multipliyer() * m_readProbability();
    if (r < prob && !items->empty()) {
        // Read item
        string cmd = string("read ") + bucket + " " + boost::lexical_cast<string>(first);
        execCommand(m_sock, cmd, m_buffer);
    }

    prob += m_multipliyer() * m_invalidProbability();
    if (r < prob && !items->empty()) {
        // Invalid command
        string cmd = string("invalide command");
        execCommand(m_sock, cmd, m_buffer);
    }
}

/*********************************************************
 *   LoadMaker
 */

void LoadMaker::init(short port, int workThreadCount)
{
    m_sock.connect("localhost", port);
    buildInitialStock();
    m_bucketsMaker.init(&m_sock, &m_buffer, port);
    for (int i=0; i < workThreadCount; i++) {
        m_itemMakers.push_back(new ItemsMaker);
        m_itemMakers.back()->init(port);
    }
}

void LoadMaker::buildInitialStock()
{
    for (int i=0; i < 4; i++) {
        string bucket = string("buck_") + boost::lexical_cast<string>(i);
        string cmd = string("add ") + bucket;
        execCommand(m_sock, cmd, m_buffer);

        ItemsListPtr items(new list<int>);
        for (int j=0; j < 8; j++) {
            string item = string("item_") + boost::lexical_cast<string>(j);
            string cmd = string("create ") + bucket + " " + item;
            execCommand(m_sock, cmd, m_buffer);

            int id = boost::lexical_cast<int>(string(m_buffer.raw()));
            items->push_back(id);
        }

        g_data.m_buckets[bucket] = items;
        g_data.m_maxBucketId = i;
    }
}

void LoadMaker::start()
{
    m_bucketsMaker.start();
    for (size_t i=0; i < m_itemMakers.size(); i++) {
        m_itemMakers[i]->start();
    }
}

void LoadMaker::stop()
{
    g_keepRunning = false;
}

void LoadMaker::uninit()
{
    m_bucketsMaker.stop();
    for (size_t i=0; i < m_itemMakers.size(); i++) {
        m_itemMakers[i]->stop();
    }
}

}
