/**********************************************
   File:   config.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "demo/config.h"
#include "demo/exception.h"
#include "utils/string_utils.h"

using std::string;

namespace cdb_demo {

void Config::process(const char* argv[], int argc)
{
    string str;
    bool ready = false;

    for (int i=0; i < (argc != 0 ? argc : 1024) && argv[i] != NULL; i++) {
        string name = argv[i];
        if (name.length() > 2 && name[0] == '-' && name[1] == '-') {
            name = name.substr(2);
            if (ready) {
                m_items[str] = "";
                str = name;
            }
            else {
                str = name;
                ready = true;
            }
        }
        else {
            if (ready) {
                m_items[str] = name;
                ready = false;
            }
            else {
                THROW(string("Invalid command-line argument: ")+name);
            }
        }
    }

    if (ready) {
        m_items[str] = "";
    }
}

bool Config::find(const std::string& key, std::string& value) const
{
    Iterator iter = m_items.find(key);
    if (iter == m_items.end()) return false;
    value = iter->second;
    return true;
}

void Config::process(std::string str)
{
    cdb::StringUtils::trim(str);
    size_t pos = str.find('=');
    if (pos == std::string::npos) {
        THROW("Invalid configuration string");
    }

    std::string name = str.substr(0, pos);
    std::string value = str.substr(pos+1);

    m_items[name]=value;
}

}
