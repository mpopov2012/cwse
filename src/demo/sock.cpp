/**********************************************
   File:   sock.cpp

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "demo/sock.h"
#include "demo/exception.h"
#include <string.h>
#include <errno.h>
#include <iostream>
#include <string>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <unistd.h>

using std::string;
using std::cout;
using std::endl;

#define closesocket(x)   close(x)
static const int INVALID_SOCKET = -1;

namespace cdb_demo {

//static
void Sock::init()
{

}

Sock::Sock()
  : m_socket(INVALID_SOCKET),
    m_selectable(false)
{
}

Sock::~Sock()
{
    disconnect();
}

void Sock::connect(const char* host, short port)
{
    m_socket = ::socket(AF_INET, SOCK_STREAM, 0);
    if (m_socket == INVALID_SOCKET) THROW(string("Failed to create a socket - ") + strerror(errno));

    struct sockaddr_in   addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port   = htons(port);

    struct hostent* server = gethostbyname(host);
    memcpy(&addr.sin_addr.s_addr, server->h_addr, server->h_length);

    int ret = ::connect(m_socket, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));
    if (ret < 0) THROW(string("Failed to open connection to server - ")+strerror(errno));
}

void Sock::disconnect()
{
    if (m_socket != INVALID_SOCKET) {
        closesocket(m_socket);
        m_socket = INVALID_SOCKET;
    }
}

void Sock::listen(const char* /*addr*/, short port)
{
    int on = 1;
    int ret = setsockopt(m_socket, SOL_SOCKET,  SO_REUSEADDR, (char *)&on, sizeof(on));
    if (ret < 0) THROW(string("Failed to set listening socket reusable - ") + strerror(errno));

    struct sockaddr_in serv;
    memset(&serv, 0, sizeof(sockaddr_in));
    serv.sin_family = AF_INET;
    serv.sin_addr.s_addr = htonl(INADDR_ANY); // USE addr HERE!!!
    serv.sin_port = htons(port);
    ret = bind(m_socket, (struct sockaddr *) &serv, sizeof(sockaddr_in));
    if (ret < 0) THROW(string("Failed to bind listening socket - ") + strerror(errno));

    ret = ::listen(m_socket, 5);
    if (ret < 0) THROW(string("Failed to start listening socket - ") + strerror(errno));
}

Sock* Sock::accept()
{
    fd_set readFS, errorFS;
    FD_ZERO(&readFS);
    FD_SET(m_socket, &readFS);
    memcpy(&errorFS, &readFS, sizeof(fd_set));
    struct timeval tv = { 0, 1000000 };

    int ret = select(m_socket+1, &readFS, NULL, &errorFS, &tv);
    if (ret == 0) return NULL;

    if (FD_ISSET(m_socket, &errorFS)) {
        THROW(string("Failed to accept socket - ")+strerror(errno));
    }

    if (!FD_ISSET(m_socket, &readFS)) THROW("Invalid select result");

    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    socklen_t len = sizeof(sockaddr_in);
    int fd = ::accept(m_socket, (struct sockaddr *) &addr, &len);
    if (fd == INVALID_SOCKET) {
        THROW(string("Failed to accept a new connection - ") + strerror(errno));
    }

    return new Sock(fd);
}

int Sock::receive(char* buf, int length)
{
    int size = ::recv(m_socket, buf, length, 0);
    if (size < 0) THROW(string("Failed to read socket - ")+strerror(errno));
    return size;
}

int Sock::send(const char* buf, int length)
{
    int size = ::send(m_socket, buf, length, MSG_NOSIGNAL);
    if (size < 0) THROW(string("Failed to write socket - ")+strerror(errno));
    return size;
}

} // namespace cdb_demo
