/**********************************************
   File:   command_base.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DEMO_COMMAND_BASE_H
#define CDB_DEMO_COMMAND_BASE_H

#include "demo/buffer.h"

namespace cdb_demo {

enum AngenCommand
{
    CMD_UNKNOWN,

    CMD_PARSE_FAILURE,

    CMD_NEW,
    CMD_DROP,
    CMD_LIST,

    CMD_CREATE,
    CMD_READ,
    CMD_UPDATE,
    CMD_DELETE,
};

class CommandBase
{
public:
    CommandBase(AngenCommand cmdType)
      : m_cmdType(cmdType)
    {
    }
    virtual ~CommandBase() {}

    AngenCommand cmdType() { return m_cmdType; }

    virtual void exec(Buffer& /*response*/) {}

protected:
    AngenCommand  m_cmdType;
};

class CmdUnknown : public CommandBase
{
public:
    CmdUnknown() : CommandBase(CMD_UNKNOWN) {}
    virtual void exec(Buffer& response) { response.append("ERROR: invalid command"); }
};

} // cdb

#endif // CDB_DEMO_COMMAND_BASE_H
