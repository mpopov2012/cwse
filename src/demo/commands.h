/**********************************************
   File:   commands.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DEMO_COMMANDS_H
#define CDB_DEMO_COMMANDS_H

#include "demo/command_base.h"
#include <string>

namespace cdb_demo {

class CmdNewBucket : public CommandBase
{
public:
    CmdNewBucket(const char* name) : CommandBase(CMD_NEW), m_name(name) {}
    virtual void exec(Buffer& response);

private:
    std::string m_name;
};

class CmdDropBucket : public CommandBase
{
public:
    CmdDropBucket(const char* name) : CommandBase(CMD_DROP), m_name(name) {}
    virtual void exec(Buffer& response);

private:
    std::string m_name;
};

class CmdListBucket : public CommandBase
{
public:
    CmdListBucket() : CommandBase(CMD_LIST) {}
    virtual void exec(Buffer& response);
};

class CmdCreate : public CommandBase
{
public:
    CmdCreate(const char* bucket, const char* value) : CommandBase(CMD_CREATE), m_bucket(bucket), m_value(value) {}
    virtual void exec(Buffer& response);

private:
    std::string m_bucket;
    std::string m_value;
};

class CmdRead : public CommandBase
{
public:
    CmdRead(const char* bucket, int id) : CommandBase(CMD_READ), m_bucket(bucket), m_id(id) {}
    virtual void exec(Buffer& response);

private:
    std::string m_bucket;
    int m_id;
};

class CmdUpdate : public CommandBase
{
public:
    CmdUpdate(const char* bucket, int id, const char* value)
      : CommandBase(CMD_UPDATE), m_bucket(bucket), m_id(id), m_value(value) {}
    virtual void exec(Buffer& response);

private:
    std::string m_bucket;
    int m_id;
    std::string m_value;
};

class CmdDelete : public CommandBase
{
public:
    CmdDelete(const char* bucket, int id) : CommandBase(CMD_DELETE), m_bucket(bucket), m_id(id) {}
    virtual void exec(Buffer& response);

private:
    std::string m_bucket;
    int m_id;
};


}

#endif // CDB_DEMO_COMMANDS_BUCKET_H
