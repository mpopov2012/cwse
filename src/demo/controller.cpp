/**********************************************
   File:   controller.cpp

   Copyright 2012 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "demo/controller.h"
#include "demo/exception.h"
#include "demo/metrics_const.h"
#include "metrics_plus_plus.h"
#include <iostream>
#include <list>
#include <sys/timerfd.h>
#include <errno.h>
#include <string.h>

using namespace metrics_plus_plus;

namespace cdb_demo {

static bool RUNNING = true;

void TaskProcessor::run()
{
    while (RUNNING) {
        Task* task = MainQueue::get();
        if (task == NULL) break;

        UPDATE_METRIC(METRIC_QUEUE_COUNT, -1);

        if (MainQueue::fillup()) {
            task->fillupResponse();
        }
        else {
            task->exec();
        }
        task->finalize();
    }
    m_finished = true;
}

void Communicator::init(short port, int tfd, TimerEventHandler* timerEventHandler)
{
    m_listener.init(port, tfd, timerEventHandler);
}

void Communicator::run()
{
    while (RUNNING) {
        m_listener.handle();
    }
}

//static
Controller& Controller::instance()
{
    static Controller self;
    return self;
}

void Controller::init(short port)
{
    int fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
    if (fd < 0) {
        std::cerr << "Failed to create timerfd: " << strerror(errno) << std::endl;
        THROW("Failed to create timerfd");
    }

    itimerspec its;
    its.it_value.tv_sec = 1;
    its.it_value.tv_nsec = 0;
    its.it_interval.tv_sec = 1;
    its.it_interval.tv_nsec = 0;
    int ret = timerfd_settime(fd, 0, &its, NULL);
    if (ret < 0) {
        std::cerr << "Failed to start timerfd: " << strerror(errno) << std::endl;
        THROW("Failed to start timerfd");
    }

    m_workingThreadCount = g_dynoConfig->get(CFG_WORKING_THREADS_COUNT, DEFAULT_WORKING_THREADS_COUNT);

    boost::lock_guard<boost::mutex> lock(m_mutex);

    for (int i=0; i < m_workingThreadCount(); i++) {
        m_taskProcessors[m_workingThreadID] = new TaskProcessor(m_workingThreadID);
        m_workingThreadID++;
    }

    m_communicator = new Communicator;
    m_communicator->init(port, fd, this);
}

void Controller::start()
{
    RUNNING = true;

    m_communicator->start();

    boost::lock_guard<boost::mutex> lock(m_mutex);
    m_targetCount = 0;
    TaskProcessors::iterator iter = m_taskProcessors.begin();
    for ( ; iter != m_taskProcessors.end(); ++iter) {
        iter->second->start();
        m_targetCount++;
    }

    UPDATE_METRIC(METRIC_WORKING_THREADS_COUNT, m_taskProcessors.size());
}

void Controller::stop()
{
    RUNNING = false;

    m_communicator->stop();

    boost::lock_guard<boost::mutex> lock(m_mutex);

    TaskProcessors::iterator iter = m_taskProcessors.begin();
    for ( ; iter != m_taskProcessors.end(); ++iter) {
        MainQueue::put(NULL);
        m_targetCount--;
    }

    iter = m_taskProcessors.begin();
    for ( ; iter != m_taskProcessors.end(); ++iter) {
        iter->second->stop();
    }
}

void Controller::uninit()
{
    delete m_communicator;

    boost::lock_guard<boost::mutex> lock(m_mutex);

    TaskProcessors::iterator iter = m_taskProcessors.begin();
    for ( ; iter != m_taskProcessors.end(); ++iter) {
        delete iter->second;
    }
    m_taskProcessors.clear();

    UPDATE_METRIC(METRIC_WORKING_THREADS_COUNT, 0);
}

void Controller::handleTimerEvent()
{
    boost::lock_guard<boost::mutex> lock(m_mutex);

    std::list< TaskProcessors::iterator > eraser;

    TaskProcessors::iterator iter = m_taskProcessors.begin();
    for ( ; iter != m_taskProcessors.end(); ++iter) {
        if (iter->second->finished()) {
            eraser.push_back(iter);
        }
    }

    std::list< TaskProcessors::iterator >::iterator jter = eraser.begin();
    for ( ; jter != eraser.end(); ++jter) {
        iter = *jter;
        iter->second->stop();
        delete iter->second;
        m_taskProcessors.erase(iter);
    }

    if (!RUNNING) return;

    while (m_targetCount < m_workingThreadCount()) {
        TaskProcessor* proc = new TaskProcessor(m_workingThreadID);
        m_taskProcessors[m_workingThreadID] = proc;
        m_workingThreadID++;

        proc->start();
        m_targetCount++;
    }

    while (m_targetCount > m_workingThreadCount()) {
        MainQueue::put(NULL);
        m_targetCount--;
    }

    UPDATE_METRIC(METRIC_WORKING_THREADS_COUNT, m_taskProcessors.size());
}

} // namespace cdb_demo


