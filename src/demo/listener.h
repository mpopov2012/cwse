/**********************************************
   File:   listener.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_DEMO_LISTENER_H
#define CDB_DEMO_LISTENER_H

#include "demo/handler.h"
#include <boost/smart_ptr.hpp>
#include <map>
#include <vector>
#include <sys/epoll.h>

using std::map;
using std::vector;

namespace cdb_demo {

class TimerEventHandler
{
public:
    virtual ~TimerEventHandler() {}
    virtual void handleTimerEvent() = 0;
};

class Listener
{
public:
    Listener();
    ~Listener();
    void init(short port, int tfd, TimerEventHandler* timerEventHandler);
    void handle();

private:
    static const int EXPECTED_CONNECTION_COUNT = 128;
    static const int EPOLL_TIMEOUT = 50; // ms

private:
    int m_listenFD;
    int m_timerFD;
    TimerEventHandler* m_timerEventHandler;
    int m_epoll;
    int m_pipe[2];

    typedef map< int, HandlerPtr > Handlers;
    Handlers m_handlers;

    typedef vector< epoll_event > EpollEvents;
    EpollEvents m_events;

    long* m_perfConnectionCount;

private:
    void acceptConnection();
    void removeConnection(int fd);
    void processNotification();
    void blockConnection(int fd);
    void processTimerEvent();

};

} // namespace cdb_demo

#endif // CDB_DEMO_LISTENER_H
