/**********************************************
   File:   commands.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "metrics_plus_plus.h"
#include "demo/commands.h"
#include "demo/container.h"
#include "demo/connections.h"
#include "demo/metrics_const.h"
#include <boost/lexical_cast.hpp>

using namespace std;
using namespace metrics_plus_plus;

namespace cdb_demo {

void CmdNewBucket::exec(Buffer& response)
{
    Container::Instance().create(m_name);

    Connection* conn = ConnectionPool::instance().get();
    conn->addBucket(m_name);
    ConnectionPool::instance().put(conn);

    response.append("ok");

    UPDATE_METRIC(METRIC_COMMAND_BUCKET_ADD, 1);
}

void CmdDropBucket::exec(Buffer& response)
{
    Container::Instance().drop(m_name);

    Connection* conn = ConnectionPool::instance().get();
    conn->deleteBucket(m_name);
    ConnectionPool::instance().put(conn);

    response.append("ok");

    UPDATE_METRIC(METRIC_COMMAND_BUCKET_DROP, 1);
}

void CmdListBucket::exec(Buffer& response)
{
    list<string> lst;
    Container::Instance().enumerate(lst);

    string result = "";
    bool first = true;
    for (list<string>::iterator iter = lst.begin(); iter != lst.end(); ++iter) {
        if (first) {
            first = false;
        }
        else {
            result += '\n';
        }
        result += *iter;
    }

    Connection* conn = ConnectionPool::instance().get();
    conn->listBuckets(lst);
    ConnectionPool::instance().put(conn);

    if (result.empty()) result = "<empty>";

    response.append(result);

    UPDATE_METRIC(METRIC_COMMAND_BUCKET_LIST, 1);
}

void CmdCreate::exec(Buffer& response)
{
    BucketPtr bucket = Container::Instance().get(m_bucket);
    int id = bucket->create(m_value);
    response.append(boost::lexical_cast<string>(id));

    Connection* conn = ConnectionPool::instance().get();
    conn->createItem(m_bucket, id, m_value);
    ConnectionPool::instance().put(conn);

    UPDATE_METRIC(METRIC_COMMAND_ITEM_CREATE, 1);
}

void CmdRead::exec(Buffer& response)
{
    BucketPtr bucket = Container::Instance().get(m_bucket);
    response.append(bucket->read(m_id));

    Connection* conn = ConnectionPool::instance().get();
    conn->readItem(m_bucket, m_id);
    ConnectionPool::instance().put(conn);

    UPDATE_METRIC(METRIC_COMMAND_ITEM_READ, 1);
}

void CmdUpdate::exec(Buffer& response)
{
    BucketPtr bucket = Container::Instance().get(m_bucket);
    bucket->update(m_id, m_value);
    response.append("ok");

    Connection* conn = ConnectionPool::instance().get();
    conn->updateItem(m_bucket, m_id, m_value);
    ConnectionPool::instance().put(conn);

    UPDATE_METRIC(METRIC_COMMAND_ITEM_UPDATE, 1);
}

void CmdDelete::exec(Buffer& response)
{
    BucketPtr bucket = Container::Instance().get(m_bucket);
    bucket->del(m_id);
    response.append("ok");

    Connection* conn = ConnectionPool::instance().get();
    conn->deleteItem(m_bucket, m_id);
    ConnectionPool::instance().put(conn);

    UPDATE_METRIC(METRIC_COMMAND_ITEM_DELETE, 1);
}

} // namepspace cdb
