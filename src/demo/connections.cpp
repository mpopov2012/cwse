/**********************************************
   File:   connections.cpp

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#include "demo/connections.h"
#include "demo/timer.h"
#include "demo/metrics_const.h"
#include <string.h>
#include <stdlib.h>

namespace cdb_demo {

ConnectionPool ConnectionPool::m_self;

/*********************************************
 * Connection
 *********************************************/

Connection::Connection()
{
    time_t t = time(NULL);
    memcpy(&m_randSeed, &t, sizeof(m_randSeed));

    unsigned int temp;
    void* p = this;
    memcpy(&temp, &p, sizeof(temp));

    m_randSeed += temp;

    m_multiplier = g_dynoConfig->get(CFG_CONNECTIONS_MULTIPLIER, DEFAULT_CONNECTIONS_MULTIPLIER);
    m_addBucketDelay = g_dynoConfig->get(CFG_CONNECTIONS_ADD_BUCKET_DELAY, DEFAULT_CONNECTIONS_ADD_BUCKET_DELAY);
    m_deleteBucketDelay = g_dynoConfig->get(CFG_CONNECTIONS_DELETE_BUCKET_DELAY, DEFAULT_CONNECTIONS_DELETE_BUCKET_DELAY);
    m_listBucketDelay = g_dynoConfig->get(CFG_CONNECTIONS_LIST_BUCKET_DELAY, DEFAULT_CONNECTIONS_LIST_BUCKET_DELAY);
    m_createItemDelay = g_dynoConfig->get(CFG_CONNECTIONS_CREATE_ITEM_DELAY, DEFAULT_CONNECTIONS_CREATE_ITEM_DELAY);
    m_readItemDelay = g_dynoConfig->get(CFG_CONNECTIONS_READ_ITEM_DELAY, DEFAULT_CONNECTIONS_READ_ITEM_DELAY);
    m_updateItemDelay = g_dynoConfig->get(CFG_CONNECTIONS_UPDATE_ITEM_DELAY, DEFAULT_CONNECTIONS_UPDATE_ITEM_DELAY);
    m_deleteItemDelay = g_dynoConfig->get(CFG_CONNECTIONS_DELETE_ITEM_DELAY, DEFAULT_CONNECTIONS_DELETE_ITEM_DELAY);
}

void Connection::addBucket(const std::string&)
{
    randomSleep(m_multiplier() * m_addBucketDelay(), m_randSeed);
}

void Connection::deleteBucket(const std::string&)
{
    randomSleep(m_multiplier() * m_deleteBucketDelay(), m_randSeed);
}

void Connection::listBuckets(std::list<std::string>&)
{
    randomSleep(m_multiplier() * m_listBucketDelay(), m_randSeed);
}


void Connection::createItem(const std::string&, int, const std::string&)
{
    randomSleep(m_multiplier() * m_createItemDelay(), m_randSeed);
}

std::string Connection::readItem(const std::string&, int)
{
    randomSleep(m_multiplier() * m_readItemDelay(), m_randSeed);
    return "";
}

void Connection::updateItem(const std::string&, int, const std::string&)
{
    randomSleep(m_multiplier() * m_updateItemDelay(), m_randSeed);
}

void Connection::deleteItem(const std::string&, int)
{
    randomSleep(m_multiplier() * m_deleteItemDelay(), m_randSeed);
}

/*********************************************
 * ConnectionPool
 *********************************************/

void ConnectionPool::init(size_t count)
{
    m_reserved = g_dynoConfig->get(CFG_CONNECTIONS_COUNT, DEFAULT_CONNECTIONS_COUNT);

    for (size_t i=0; i < count; i++) {
        m_connections.put(new Connection);
        m_count++;
    }
}

Connection* ConnectionPool::get()
{
    if (m_connections.size() == 0 && (m_reserved() > m_count)) {
        m_count++;
        return new Connection;
    }
    return m_connections.get();
}

void ConnectionPool::put(Connection* conn)
{
    if (m_count > m_reserved()) {
        delete conn;
    }
    else {
        m_connections.put(conn);
    }
}


} // namespace cdb_demo

