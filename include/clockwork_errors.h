/**********************************************
   File:   clockwork_errors.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_CLOCKWORK_ERRORS_H
#define CDB_CLOCKWORK_ERRORS_H

#include <stdexcept>
#include <string>

namespace cdb {

#define THROW(cause, errID) throw CwException(cause, errID, __FILE__, __LINE__)

class CwException : public std::logic_error
{
public:
	explicit CwException(const std::string& arg, int errID, const char* fl, int ln)
	  : std::logic_error(arg), errorID(errID), file(fl), line(ln) {}
	~CwException() throw() {}

	const int   errorID;
	const char* file;
	const int   line;
};

}

#endif // CDB_CLOCKWORK_ERRORS_H
