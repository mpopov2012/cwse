/**********************************************
   File:   metrics_plus_plus.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_METRICS_PLUS_PLUS_H
#define CDB_METRICS_PLUS_PLUS_H

#include "clockwork.h"
#include "metrics_plus_plus.h"
#include <stdint.h>
#include <stddef.h>

namespace metrics_plus_plus {

class Metric
{
protected:
    Metric() {}
    Metric(const Metric&);
    Metric& operator=(const Metric&);
    virtual ~Metric() {}
public:
    virtual void update(double val) = 0;
};

class Stopwatch
{
public:
    Stopwatch() : m_high(0), m_low(0) {}
    Stopwatch(const Stopwatch& a) : m_high(a.m_high), m_low(a.m_low) {}
    Stopwatch& operator=(const Stopwatch& a) { if (&a != this) { m_high = a.m_high; m_low = a.m_low; } return *this; }

    void start();
    int64_t stop();

private:
    int64_t m_high;
    int64_t m_low;
};

struct BucketDescr
{
    double size;
    int count;
};

class MetricsFactory
{
public:
    static MetricsFactory* create(const char* path);

public:
    virtual ~MetricsFactory() {}

    virtual void start() = 0;
    virtual void stop() = 0;

    virtual void process(const char* str) = 0;

    virtual void createGauge(const char* name) = 0;
    virtual void createCounter(const char* name) = 0;
    virtual void createMeter(const char* name, int timeWindow, int collectPeriod, int ratePeriod) = 0;
    virtual void createStats(const char* name, int timeWindow, int collectPeriod) = 0;
    virtual void createPercentiles(const char* name, int timeWindow, int collectPeriod,
                                   const double* percentiles, int count,
                                   int subsetSize = 0) = 0;
    virtual void createHistogram(const char* name, int timeWindow, int collectPeriod,
                            const BucketDescr* buckets, int count) = 0;

    virtual Metric* find(const char* name) = 0;
    virtual Metric& get(const char* name) = 0;

    virtual void linkMetrics(const char* from, const char* to) = 0;

    virtual void dumpMissingMetrics(const char* path = NULL) = 0;
};


/*************************************************************
 *
 *   DynoConfig
 *
 */

class DynoConfigValue
{
public:
    DynoConfigValue() : m_dataset(NULL), m_id(CWSE_INVALID_COLUMN_ID), m_defaultValue(0.0) {}
    DynoConfigValue(cdb::Dataset* dataset, uint16_t id, double defaultValue);
    DynoConfigValue(const DynoConfigValue& another);
    DynoConfigValue& operator=(const DynoConfigValue& another);

    double operator()() const;

private:
    cdb::Dataset* m_dataset;
    uint16_t m_id;
    double m_defaultValue;
};

class DynoConfig
{
public:
    virtual ~DynoConfig() {}
public:
    static DynoConfig* create(const char* path);
public:
    virtual DynoConfigValue get(const char* name, double defaultValue) = 0;
};

} // namespace metrics_plus_plus

#endif // CDB_METRICS_PLUS_PLUS_H

