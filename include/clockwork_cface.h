/**********************************************
   File:   clockwork_flat.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_CLOCKWORK_FLAT_H
#define CDB_CLOCKWORK_FLAT_H

#include "clockwork_base.h"

#ifdef WIN32
#ifdef CLOCKWORK_SE_EXPORTS
#define DllExport  __declspec(dllexport)
#else
#define DllExport  __declspec(dllimport)
#endif
#else
#define DllExport
#endif

#ifdef __cplusplus
extern "C" {
#endif

struct CwseError
{
	int  last_error;
	char msg[256];
};

DllExport void          cwse_set_logging(int level, struct CwseError* error);

DllExport struct CwseDatasetConfig cwse_get_default_dataset_config(struct CwseError* error);
DllExport struct CwseColumnConfig  cwse_get_default_column_config(struct CwseError* error);

DllExport void          cwse_create(const char* path, const struct CwseDatasetConfig* cfg, struct CwseError* error);
DllExport void*         cwse_open(const char* path, int dataAccess, struct CwseError* error);
DllExport void          cwse_release_dataset(void* handle, struct CwseError* error);

DllExport struct CwseDatasetConfig cwse_get_dataset_config(void* handle, struct CwseError* error);
DllExport void          cwse_alter_dataset(void* handle, const struct CwseDatasetConfig* cfg, struct CwseError* error);

DllExport void          cwse_add_column(void* handle, const char* name, const CwseColumnConfig* cfg, struct CwseError* error);
DllExport struct CwseColumnConfig cwse_get_column_config(void* handle, const char* name, struct CwseError* error);
DllExport uint16_t      cwse_find_metric(void* handle, const char* name, struct CwseError* error);

DllExport void*         cwse_names_iterator(void* handle, const char* pattern, struct CwseError* error);
DllExport uint16_t      cwse_get_next_name(void* names_iterator, char* name, int size, struct CwseError* error);
DllExport void          cwse_release_names_iterator(void* names_iterator, struct CwseError* error);

DllExport void          cwse_set(void* handle, double value, uint16_t id, uint64_t* ts, struct CwseError* error);
DllExport void          cwse_inc_double(void* handle, double value, uint16_t id, uint64_t* ts, struct CwseError* error);
DllExport void          cwse_inc(void* handle, int64_t value, uint16_t id, uint64_t* ts, struct CwseError* error);
DllExport double        cwse_get(void* handle, uint16_t id, uint64_t* ts, struct CwseError* error);

#ifdef __cplusplus
}
#endif

#endif // CDB_CLOCKWORK_FLAT_H
