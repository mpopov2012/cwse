/**********************************************
   File:   clockwork_base.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_CLOCKWORK_BASE_H
#define CDB_CLOCKWORK_BASE_H

#include <stddef.h>
#include <stdint.h>
#include <float.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CWSE_DATA_ACCESS_READ_ONLY    0
#define CWSE_DATA_ACCESS_READ_WRITE   1

#define CWSE_LOG_LEVEL_MUTE           0
#define CWSE_LOG_LEVEL_ERROR          1
#define CWSE_LOG_LEVEL_WARN           2
#define CWSE_LOG_LEVEL_TRACE          3

#define CWSE_MAX_NAME_LENGTH          128

#define CWSE_TYPE_UNKNOWN             0
#define CWSE_TYPE_DOUBLE              1
#define CWSE_TYPE_INT64               2

#define CWSE_INVALID_COLUMN_ID        0xFFFF
#define CWSE_INVALID_TIMESTAMP        0xFFFFFFFFFFFFFFFF
#define CWSE_INVALID_VALUE            DBL_MAX

#define CWSE_ERROR_UNKNOWN                    1000
#define CWSE_ERROR_INVALID_ARG                1001
#define CWSE_ERROR_FAILED_FILE_OPERATION      1002
#define CWSE_ERROR_OBJECT_NOT_INITIALIZED     1003
#define CWSE_ERROR_NOT_IMPLEMENTED            1004
#define CWSE_ERROR_LOGICAL_ERROR              1005
#define CWSE_ERROR_INVALID_ACCESS_PERMISSIONS 1006
#define CWSE_ERROR_SYSTEM                     1007

typedef void (*CwseLogOutputFunc) (const char* msg);

struct CwseDatasetConfig
{
	uint64_t metricsMaxCount;
};

struct CwseColumnConfig
{
    uint64_t type;
};

#ifdef __cplusplus
}
#endif

#endif // CDB_CLOCKWORK_BASE_H

