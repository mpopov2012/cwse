/**********************************************
   File:   clockwork.h

   Copyright 2014 Michael Popov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **********************************************/

#ifndef CDB_CLOCKWORK_H
#define CDB_CLOCKWORK_H

#include "clockwork_names.h"
#include "clockwork_base.h"

namespace cdb {

class Dataset
{
protected:
	Dataset() {}
private:
    Dataset(const Dataset&);
    Dataset& operator=(const Dataset&);

public:
    static void setLogging(int level);
    static void releaseNamesIterator(void* handle);

    static CwseDatasetConfig getDefaultDatasetConfig();
    static CwseColumnConfig  getDefaultColumnConfig();

    static void create(const char* path, const CwseDatasetConfig* cfg);
    static Dataset* open(const char* path, int dataAccess);

public:
    virtual ~Dataset() {}

    // Dataset configuration
    virtual CwseDatasetConfig getDatasetConfig() = 0;
    virtual void alterDataset(const CwseDatasetConfig* cfg) = 0;

    // Columns management
    virtual void addColumn(const char* name, const CwseColumnConfig* cfg, bool ignoreIfExist) = 0;
    virtual CwseColumnConfig getColumnConfig(const char* name) = 0;
    virtual uint16_t findColumn(const char* name) = 0;

    // Data management: current values
    virtual void set(double value, uint16_t id, uint64_t* ts=NULL) = 0;
    virtual void inc(double value, uint16_t id, uint64_t* ts=NULL) = 0;
    virtual double get(uint16_t id, uint64_t* ts=NULL) const = 0;

    // Query names
    virtual Names* getNames(const char* pattern) = 0;
};

} // namespace cdb

#endif // CDB_CLOCKWORK_H

